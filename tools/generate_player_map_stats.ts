import Database from 'better-sqlite3'
import config from 'config'
import * as fs from 'fs'
import { parse } from 'csv-parse'
import { ScoreStatus } from '../src/bot/embeds/utils/constants';
import { LivePugScored } from 'src/db/dao/pug';


const db = new Database(config.get('dbConfig.dbName'), {});

interface MapStats {
    played: number
    preference: number
}

interface PlayerStats {
    id: string
    loved: number
    lovedPlayed: number
    liked: number
    likedPlayed: number
    sometimes: number
    sometimesPlayed: number
    hated: number
    hatedPlayed: number
    total: number
}

const generate = async (): Promise<void> => {
    const pugs = await db.prepare(`
        select p.*, s.*, m.name as map_name from pugs p join scores s 
            on p.id = s.pug_id join maps m
            on p.map_id = m.id 
            where p.started > '2023-04-02'`).all()

    const playerMaps: Map<string, Map<string, MapStats>> = new Map()

    for (const p of pugs) {
        const pugPlayers = p.players.split(',')
        pugPlayers.map((player: string) => {
            if (!playerMaps.has(player)) {
                playerMaps.set(player, new Map())
            }

            const playerMap = playerMaps.get(player)!

            if (!playerMap.has(p.map_name)) {
                playerMap.set(p.map_name, { played: 1, preference: -1 })
            } else {
                playerMap.set(p.map_name, { played: playerMap.get(p.map_name)!.played + 1, preference: -1 })
            }

        })
    }

    const preferences = await db.prepare(`select * from map_preferences `).all()

    for (const p of preferences) {
        if (!playerMaps.has(p.player_id)) {
            continue
        }

        const playerMap = playerMaps.get(p.player_id)!
        if (!playerMap.has(p.map_name)) {
            playerMap.set(p.map_name, { played: 0, preference: p.score })
        } else {
            playerMap.set(p.map_name, { played: playerMap.get(p.map_name)!.played, preference: p.score })

        }


    }

    const playerObjs = []

    for (const player of playerMaps) {
        const playerObj = new Object({
            id: player[0],
            loved: 0,
            lovedPlayed: 0,
            liked: 0,
            likedPlayed: 0,
            sometimes: 0,
            sometimesPlayed: 0,
            hated: 0,
            hatedPlayed: 0,
            total: 0,
        }) as PlayerStats

        for (const p of player[1]) {
            playerObj.total += 1

            if (p[1].preference === -10) {
                playerObj.hated += 1
                playerObj.hatedPlayed += p[1].played 
            }
            if (p[1].preference === 0) {
                playerObj.sometimes += 1
                playerObj.sometimesPlayed += p[1].played 
            }
            if (p[1].preference === 5) {
                playerObj.liked += 1
                playerObj.likedPlayed += p[1].played 
            }
            if (p[1].preference === 10) {
                playerObj.loved += 1
                playerObj.lovedPlayed += p[1].played 
            }
        }

        playerObjs.push(playerObj)

    }

    const total = new Object({
        id: 'total',
        loved: 0,
        lovedPlayed: 0,
        liked: 0,
        likedPlayed: 0,
        sometimes: 0,
        sometimesPlayed: 0,
        hated: 0,
        hatedPlayed: 0,
        total: 0,
    }) as PlayerStats

    for (const playerObj of playerObjs) {
        total.loved += playerObj.loved
        total.lovedPlayed += playerObj.lovedPlayed
        total.liked += playerObj.liked
        total.likedPlayed += playerObj.likedPlayed
        total.sometimes += playerObj.sometimes
        total.sometimesPlayed += playerObj.sometimesPlayed
        total.hated += playerObj.hated
        total.hatedPlayed += playerObj.hatedPlayed
        total.total += playerObj.total
    }

    console.log (total)
}

generate()

//4887
// 37.3%
// 26.3%
// 20.5%
// 15.8%
