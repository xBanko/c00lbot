import Database from 'better-sqlite3'
import config from 'config'

import { migrationsToRun } from '../src/db/migrations'

const db = new Database(config.get('dbConfig.dbName'), {});

(async () => {
    await db.exec(migrationsToRun)
})().catch(err => {
    console.error(err);
})
