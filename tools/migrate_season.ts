import Database from 'better-sqlite3'
import config from 'config'
import moment from 'moment'
import prompts from 'prompts'

const db = new Database(config.get('dbConfig.dbName'), {});

const format = "YYYY-MM-DD"
const today = new Date();
const defaultStartDate = moment(today).format(format);


const migrate = async (startDate: string, endDate: string) => {
  const lastId = await db.prepare('select id from seasons order by id desc limit 1').get()
  let id = 0
  if (!lastId) {
    id = 1
  } else {
    id = lastId.id + 1
  }

  const archivedId = id - 1

  const migrations = `
    BEGIN;

    CREATE TABLE IF NOT EXISTS season_${archivedId}_pugs (id integer primary key,
        players TEXT,
        team_red TEXT,
        team_blue TEXT,
        created DEFAULT CURRENT_TIMESTAMP,
        started TEXT,
        finished TEXT,
        str_red integer,
        str_blue integer,
        rating_change_red integer,
        rating_change_blue integer,
        mode_id integer,
        map_id integer,
        score_id integer,
        server_id integer
    );

    CREATE TABLE IF NOT EXISTS season_${archivedId}_players (
        id integer primary key,
        discord_id TEXT UNIQUE,
        nickname TEXT,
        created DEFAULT CURRENT_TIMESTAMP,
        rating integer,
        rating_change integer,
        pugs_played integer DEFAULT 0,
        fav_server text,
        fav_server_country text,
        starting_rating integer,
        coins integer
    );

    CREATE TABLE IF NOT EXISTS season_${archivedId}_scores (
        id integer primary key,
        pug_id integer,
        score_red integer,
        score_blue integer,
        created DEFAULT CURRENT_TIMESTAMP,
        created_by text,
        status text default 'PENDING',
        score_msg_id text
    );

    CREATE TABLE IF NOT EXISTS season_${archivedId}_bets (
      id integer,
      pug_id integer,
      player_id text,
      amount integer,
      bet_on text,
      net integer,
      odds integer,
      created DEFAULT CURRENT_TIMESTAMP,
      realised text
    );

    INSERT INTO season_${archivedId}_pugs SELECT * FROM pugs;
    INSERT INTO season_${archivedId}_players SELECT * FROM players;
    INSERT INTO season_${archivedId}_scores SELECT * FROM scores;
    INSERT INTO season_${archivedId}_bets SELECT * FROM bets;

    INSERT INTO seasons (start_date, end_date, name, archived) values ('${startDate}', '${endDate}', 'Season_${id}', 0);

    UPDATE seasons set archived = 1 where id = ${archivedId};
    UPDATE players set rating_change = null;
    UPDATE players set starting_rating = rating;
    UPDATE players set coins = 400;

    delete from scores;
    delete from pugs;
    delete from bets;
    delete from passwords;


    END;
    `

  await db.exec(migrations)
}


(async () => {
  const response = await prompts({
    name: 'yesno',
    message: 'You are about to migrate database to a new season, are you sure?',
    type: 'text'
  })

  if (response.yesno == 'yes' || response.yesno == 'y') {
    let startDate = ''
    let endDate = ''

    const startRes = await prompts({
      name: 'startdate',
      message: 'When does the season start? Enter date. Use YYYY-MM-DD format',
      initial: defaultStartDate,
      type: 'text'
    })

    const endRes = await prompts({
      name: 'enddate',
      message: 'When does the season end? Enter date. Use YYYY-MM-DD format',
      initial: moment(defaultStartDate).add(1, 'M').format(format),
      type: 'text'
    })

    startDate = startRes.startdate
    endDate = endRes.enddate

    if (!moment(startDate, format, true).isValid()) {
      console.log('Start date is not of right format!')
      return
    }

    if (!moment(endDate, format, true).isValid()) {
      console.log('End date is not of right format!')
      return
    }

    await migrate(startDate, endDate)
  }
})()
