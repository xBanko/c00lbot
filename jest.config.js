module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    setupFilesAfterEnv: ['./test/setup.ts'],
    globals: {
        'ts-jest': {
            diagnostics: false
        }
    }
};