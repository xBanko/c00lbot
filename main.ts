import Database from 'better-sqlite3'
import { migrationsToRun } from './src/db/migrations'
import config from 'config'
import { initBot } from './src/bot/bot';
import { startProcessing as processScores } from './src/pug/server/process_server_scores';
import { startProcessing as processUnbans } from './src/pug/process_unban_player';
import { startScheduler } from './src/bot/scheduler';
import { app } from './src/api/router'

const db = new Database(config.get('dbConfig.dbName'), {});

const start = async (): Promise<void> => {
    await db.exec(migrationsToRun)
    initBot() // bot!

    // scripts that have their own timers
    processScores() // server score fetcher
    processUnbans() // check for unbans
    startScheduler()

    app.listen(9999) // api
}

start()
