import { findOptimalServer } from '../../src/pug/server/find_optimal_server'
import { getNonBusyServers, getServerByShortname, Server } from '../../src/db/dao/server'
import { getPreferencesForPlayers, ServerPreference } from '../../src/db/dao/server_preference'

jest.mock('../../src/db/dao/server', () => ({
    getNonBusyServers: jest.fn(),
    getServerByShortname: jest.fn(),
}))

jest.mock('../../src/db/dao/server_preference', () => ({
    getPreferencesForPlayers: jest.fn()
}))

describe('findOptimalServer', () => {
    let servers: Server[]
    let preferences: ServerPreference[]

    beforeEach(() => {
        servers = [
            { id: '1', shortname: 'server1', country: 'US', ip: '1.1.1.1', port: '123', modes: '', busy: false, password: '' },
            { id: '2', shortname: 'server2', country: 'CA', ip: '2.2.2.2', port: '123', modes: '', busy: false, password: '' },
            { id: '3', shortname: 'server3', country: 'CA', ip: '3.3.3.3', port: '123', modes: '', busy: false, password: '' }
        ]
        preferences = [
            { player_id: 'player1', server_shortname: 'server1', score: 0 },
            { player_id: 'player2', server_shortname: 'server2', score: 1 },
            { player_id: 'player3', server_shortname: 'server3', score: -1 },
        ];

        (getNonBusyServers as jest.Mock).mockClear(),
        (getPreferencesForPlayers as jest.Mock).mockClear(),
        (getServerByShortname as jest.Mock).mockClear()
    })

    it('should return the server with the highest number of votes', async () => {
        preferences = [
            { player_id: 'player1', server_shortname: 'server1', score: 0 },
            { player_id: 'player2', server_shortname: 'server2', score: 1 },
            { player_id: 'player3', server_shortname: 'server3', score: -1 },
        ];
        (getNonBusyServers as jest.Mock).mockResolvedValueOnce(servers),
        (getPreferencesForPlayers as jest.Mock).mockResolvedValueOnce(preferences),
        (getServerByShortname as jest.Mock).mockResolvedValueOnce(servers[1])

        const result = await findOptimalServer('player1,player2,player3')

        expect(getNonBusyServers).lastCalledWith()
        expect(getPreferencesForPlayers).lastCalledWith('player1,player2,player3')
        expect(getServerByShortname).lastCalledWith('server2')
        expect(result).toEqual({ id: '2', shortname: 'server2', country: 'CA', ip: '2.2.2.2', port: '123', modes: '', busy: false, password: '' })
    })

    it('should return the first non-busy server if there is no preference', async () => {
        preferences = [];
        (getNonBusyServers as jest.Mock).mockResolvedValueOnce(servers),
        (getPreferencesForPlayers as jest.Mock).mockResolvedValueOnce(preferences),
        (getServerByShortname as jest.Mock).mockResolvedValueOnce(servers[0])

        const result = await findOptimalServer('player1,player2,player3')

        expect(getNonBusyServers).lastCalledWith()
        expect(getPreferencesForPlayers).lastCalledWith('player1,player2,player3')
        expect(getServerByShortname).toHaveBeenCalled()
        expect(result).toEqual({ id: '1', shortname: 'server1', country: 'US', ip: '1.1.1.1', port: '123', modes: '', busy: false, password: '' })
    })

    it('should return one of the servers with highest votes', async () => {
        preferences = [
            { player_id: 'player1', server_shortname: 'server1', score: 0 },
            { player_id: 'player2', server_shortname: 'server2', score: 1 },
            { player_id: 'player3', server_shortname: 'server3', score: 1 },
        ];
        (getNonBusyServers as jest.Mock).mockResolvedValueOnce(servers),
        (getPreferencesForPlayers as jest.Mock).mockResolvedValueOnce(preferences),
        (getServerByShortname as jest.Mock).mockResolvedValueOnce(servers[1])

        const result = await findOptimalServer('player1,player2,player3')

        expect(getNonBusyServers).lastCalledWith()
        expect(getPreferencesForPlayers).lastCalledWith('player1,player2,player3')
        expect(getServerByShortname).toHaveBeenCalled()
        expect(result).not.toEqual({ id: '1', shortname: 'server1', country: 'US', ip: '1.1.1.1', port: '123', modes: '', busy: false, password: '' })
    })

    it('should return null if there are no servers', async () => {
        (getNonBusyServers as jest.Mock).mockResolvedValueOnce([]),
        (getPreferencesForPlayers as jest.Mock).mockResolvedValueOnce(preferences),
        (getServerByShortname as jest.Mock).mockResolvedValueOnce(servers[0])

        const result = await findOptimalServer('player1,player2,player3')

        expect(getNonBusyServers).lastCalledWith()
        expect(getPreferencesForPlayers).not.toBeCalled()
        expect(getServerByShortname).not.toBeCalled()
        expect(result).toBeNull()
    })

})
