import Database from 'better-sqlite3'
import { migrationsToRun } from '../src/db/migrations'
import config from 'config'
import { insertScore } from '../src/db/dao/score';

const db = new Database(config.get('dbConfig.dbName'), {});

(async () => {
    await db.exec(migrationsToRun)
    await db.exec(`delete from pugs`)
    await db.exec(`delete from modes`)
})().catch(err => {
    console.error(err);
})


export async function insertModeAndEmptyPug(): Promise<number> {
    const modeId = (await db.prepare(`INSERT INTO modes (name, team_size) VALUES (?, ?) returning id`).run('testMode', '2')).lastInsertRowid
    return <number><unknown>db.prepare(`INSERT INTO pugs (mode_id, players, team_red, team_blue) VALUES (?, ?, ?, ?) RETURNING *`).run(modeId, '1,2,3,4', '1,2', '3,4').lastInsertRowid
}

export async function startPug(pugId: number) {
    await db.prepare(`UPDATE pugs set started = date('now') where id = ? `).run(pugId)
}

export async function finishPug(pugId: number, score1: number, score2: number, dateFinished?: string) {
    const scoreId = await insertScore(score1, score2, pugId, 'alfonzo')
    await db.prepare(`UPDATE pugs set finished = ?, score_id = ? where id = ? `).run(dateFinished != null ? dateFinished : "date('now')", scoreId, pugId)
}