import { Player } from '../src/db/dao/player'
import { makeTeams } from '../src/pug/make_teams'

describe('makeTeams', () => {
    it('should create two teams with equal ratings when possible', () => {
        const players: Player[] = [
            { discord_id: '123', nickname: 'Anže', rating: 500 },
            { discord_id: '456', nickname: 'Anžej', rating: 500 },
            { discord_id: '789', nickname: 'Anžek', rating: 500 },
            { discord_id: '321', nickname: 'Anžel', rating: 500 },
            { discord_id: '654', nickname: 'Anžem', rating: 500 },
            { discord_id: '987', nickname: 'Anžen', rating: 500 },
        ]

        const teamSize = 3

        const suggestedTeams = makeTeams(players, teamSize)

        expect(suggestedTeams.red.rating).toEqual(suggestedTeams.blue.rating)
        expect(suggestedTeams.red.players.length).toEqual(teamSize)
        expect(suggestedTeams.blue.players.length).toEqual(teamSize)
    })

    it('should create teams with as close to equal ratings as possible', () => {
        const players: Player[] = [
            { discord_id: '123', nickname: 'The Catcher in the Rye', rating: 1000 },
            { discord_id: '456', nickname: 'To Kill a Mockingbird', rating: 500 },
            { discord_id: '789', nickname: 'The Great Gatsby', rating: 500 },
            { discord_id: '321', nickname: 'Pride and Prejudice', rating: 500 },
            { discord_id: '654', nickname: 'Moby Dick', rating: 100 },
            { discord_id: '987', nickname: 'Wuthering Heights', rating: 100 },
        ]

        const teamSize = 3

        const suggestedTeams = makeTeams(players, teamSize)

        expect(Math.abs(suggestedTeams.red.rating - suggestedTeams.blue.rating)).toEqual(300)
        expect(suggestedTeams.red.players.length).toEqual(teamSize)
        expect(suggestedTeams.blue.players.length).toEqual(teamSize)
    })

    it('should create teams with as close to equal ratings as possible when there is a large difference in ratings between players', () => {
        const players: Player[] = [
            { discord_id: '123', nickname: 'Lion King', rating: 10000 },
            { discord_id: '456', nickname: 'Beauty and the Beast', rating: 9000 },
            { discord_id: '789', nickname: 'Aladdin', rating: 8000 },
            { discord_id: '987', nickname: 'Cinderella', rating: 400 },
            { discord_id: '321', nickname: 'The Little Mermaid', rating: 7000 },
            { discord_id: '654', nickname: 'Snow White', rating: 500 },
        ]

        const teamSize = 3

        const suggestedTeams = makeTeams(players, teamSize)

        expect(Math.abs(suggestedTeams.red.rating - suggestedTeams.blue.rating)).toEqual(100)
        expect(suggestedTeams.red.players.length).toEqual(teamSize)
        expect(suggestedTeams.blue.players.length).toEqual(teamSize)
    })

    it('should create teams with as close to equal ratings as possible when all players have different ratings', () => {
        const players: Player[] = [
            { discord_id: '123', nickname: 'Luka', rating: 1000 },
            { discord_id: '456', nickname: 'Maja', rating: 900 },
            { discord_id: '789', nickname: 'Ana', rating: 800 },
            { discord_id: '321', nickname: 'Matej', rating: 700 },
            { discord_id: '654', nickname: 'Tjaša', rating: 600 },
            { discord_id: '987', nickname: 'Lana', rating: 500 },
        ]

        const teamSize = 3

        const suggestedTeams = makeTeams(players, teamSize)

        expect(Math.abs(suggestedTeams.red.rating - suggestedTeams.blue.rating)).toEqual(100)
        expect(suggestedTeams.red.players.length).toEqual(teamSize)
        expect(suggestedTeams.blue.players.length).toEqual(teamSize)
    })

    it('should create different teams each time it is called with the same input', () => {
        const players: Player[] = [
            { discord_id: '258', nickname: 'Sirène', rating: 479 },
            { discord_id: '679', nickname: 'Sirena', rating: 998 },
            { discord_id: '921', nickname: 'Serrure', rating: 756 },
            { discord_id: '362', nickname: 'Mermaid', rating: 688 },
            { discord_id: '514', nickname: 'Lockpick', rating: 227 },
            { discord_id: '986', nickname: 'Verrou', rating: 100 },
        ]

        const teamSize = 3

        jest.spyOn(Math, 'random').mockReturnValue(0).mockReturnValueOnce(0.5)

        const suggestedTeams1 = makeTeams(players, teamSize)
        const suggestedTeams2 = makeTeams(players, teamSize)

        expect(suggestedTeams1.red.players).not.toEqual(suggestedTeams2.red.players)
        expect(suggestedTeams1.blue.players).not.toEqual(suggestedTeams2.blue.players)
    })

    it('should randomly select a combination of players from the bestCombinations array', () => {
        const players: Player[] = [
            { discord_id: '123', nickname: 'Lockprick', rating: 800 },
            { discord_id: '456', nickname: 'Lockprickster', rating: 700 },
            { discord_id: '789', nickname: 'Lockprickly', rating: 500 },
            { discord_id: '321', nickname: 'Lockprickled', rating: 300 },
            { discord_id: '654', nickname: 'Lockprickish', rating: 100 },
            { discord_id: '987', nickname: 'Lockick', rating: 200 },
        ]

        const teamSize = 3

        const bestCombinations = [[
            { discord_id: '456', nickname: 'Lockprickster', rating: 700 },
            { discord_id: '789', nickname: 'Lockprickly', rating: 500 },
            { discord_id: '654', nickname: 'Lockprickish', rating: 100 }
        ], [
            { discord_id: '123', nickname: 'Lockprick', rating: 800 },
            { discord_id: '321', nickname: 'Lockprickled', rating: 300 },
            { discord_id: '987', nickname: 'Lockick', rating: 200 }
        ], [
            { discord_id: '789', nickname: 'Lockprickly', rating: 500 },
            { discord_id: '987', nickname: 'Lockick', rating: 200 },
            { discord_id: '321', nickname: 'Lockprickled', rating: 300 }
        ]]

        jest.spyOn(Math, 'random').mockReturnValueOnce(0).mockReturnValueOnce(0.5).mockReturnValueOnce(0.9)

        const suggestedTeams1 = makeTeams(players, teamSize)
        const suggestedTeams2 = makeTeams(players, teamSize)
        const suggestedTeams3 = makeTeams(players, teamSize)

        expect(bestCombinations).toContainEqual(suggestedTeams1.red.players)
        expect(bestCombinations).toContainEqual(suggestedTeams1.blue.players)
        expect(bestCombinations).toContainEqual(suggestedTeams2.red.players)
        expect(bestCombinations).toContainEqual(suggestedTeams2.blue.players)
        expect(bestCombinations).toContainEqual(suggestedTeams3.red.players)
        expect(bestCombinations).toContainEqual(suggestedTeams3.blue.players)
    })
})
