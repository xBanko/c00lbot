import { blueDot, redDot } from '../../src/bot/embeds/utils/constants'
import { getBoldScore, getScoreColor } from '../../src/bot/embeds/utils/highlight_score'

jest.mock('discord.js', () => {
    return {
        Colors: {
            DarkRed: 'darkred',
            DarkBlue: 'darkblue',
            DarkGrey: 'darkgrey',
        }
    }
})

describe('getBoldScore', () => {
    it('should return a string with the red score in bold if it is greater than the blue score', () => {
        const pug = { score_red: 10, score_blue: 5 }
        expect(getBoldScore(pug)).toBe(`${redDot} **10** - 5 ${blueDot}`)
    })

    it(`should return a string with the blue score in bold if it is greater than the red score`, () => {
        const pug = { score_red: 5, score_blue: 10 }
        expect(getBoldScore(pug)).toBe(`${redDot} 5 - **10** ${blueDot}`)
    })

    it(`should return a string with both scores if they are equal`, () => {
        const pug = { score_red: 5, score_blue: 5 }
        expect(getBoldScore(pug)).toBe(`${redDot} 5 - 5 ${blueDot}`)
    })

    it(`should return a string with both scores set to 0 if they are not defined or are null`, () => {
        const pug: { score_red?: number | null, score_blue?: number | null } = {}
        expect(getBoldScore(pug)).toBe(`${redDot} 0 - 0 ${blueDot}`)
    })
})

describe('getScoreColor', () => {
    it('should return Colors.DarkRed if redScore is greater than blueScore', () => {
        expect(getScoreColor(10, 5)).toBe('darkred')
    })

    it('should return Colors.DarkBlue if blueScore is greater than redScore', () => {
        expect(getScoreColor(5, 10)).toBe('darkblue')
    })

    it('should return Colors.DarkGrey if redScore and blueScore are equal', () => {
        expect(getScoreColor(5, 5)).toBe('darkgrey')
    })
})
