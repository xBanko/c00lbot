import moment from 'moment'

import { timeAgo } from '../../src/bot/embeds/utils/time_ago'

describe('timeAgo', () => {
    test('returns "Invalid date" for invalid input', () => {
        const invalidInput = 'invalid input'
        const result = timeAgo(invalidInput)
        expect(result).toBe('Invalid date')
    })

    test('returns time remaining for future date input', () => {
        const currentTime = moment.utc()
        const futureMilliseconds = currentTime.add(2, 'hours').valueOf()
        const futureDate = moment.utc(futureMilliseconds).format()
        const result = timeAgo(futureDate)
        expect(result).toMatch(/in 2 hours/)
    })

    test('returns time remaining for positive number input', () => {
        const futureSeconds = 1000000000
        const result = timeAgo(futureSeconds)
        expect(result).toMatch(/in 32 years/)
    })

    test('returns time remaining for future string object input', () => {
        const currentDate = new Date()
        const futureDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 5)
        const result = timeAgo(futureDate.toISOString())
        expect(result).toMatch(/in (4|5) days/)
    })

    test('returns time ago for past date input', () => {
        const currentTime = moment.utc()
        const futureMilliseconds = currentTime.subtract(3, 'weeks').valueOf()
        const futureDate = moment.utc(futureMilliseconds).format()
        const result = timeAgo(futureDate)
        expect(result).toMatch(/21 days ago/)
    })

    test('returns time ago for negative number input', () => {
        const pastSeconds = -45
        const result = timeAgo(pastSeconds)
        expect(result).toMatch(/a minute ago/)
    })

    test('returns time ago for past string input', () => {
        const currentDate = new Date()
        const pastDate = new Date(currentDate.getFullYear(), currentDate.getMonth() - 2, currentDate.getDate(), currentDate.getHours(), currentDate.getMinutes())
        const result = timeAgo(pastDate.toISOString())
        expect(result).toMatch(/2 months ago/)
    })

    test('returns time ago for date string with missing leading zeros', () => {
        const currentTime = moment.utc()
        const futureMilliseconds = currentTime.add(5, 'days').valueOf()
        const futureDateString = moment.utc(futureMilliseconds).format('ddd MMM D YYYY HH:mm:ss')
        const result = timeAgo(futureDateString)
        expect(result).toMatch(/in (4|5) days/)
    })

    test('returns time ago for date string in different timezone', () => {
        const currentTime = moment.utc()
        const futureMilliseconds = currentTime.add(4, 'hours').valueOf()
        const futureDateString = moment.utc(futureMilliseconds).format('ddd MMM DD YYYY HH:mm:ss') + ' GMT+0100 (Central European Time)'
        const result = timeAgo(futureDateString)
        expect(result).toMatch(/in (3|4) hours/)
    })

    test('returns time remaining for current date and time', () => {
        const currentDate = new Date()
        const result = timeAgo(currentDate)
        expect(result).toMatch(/a few seconds ago/)
    })

    test('returns time ago for past date more than a year old', () => {
        const currentTime = moment.utc()
        const pastMilliseconds = currentTime.subtract(2, 'years').valueOf()
        const pastDate = moment.utc(pastMilliseconds).toDate()
        const result = timeAgo(pastDate)
        expect(result).toMatch(/2 years ago/)
    })
})
