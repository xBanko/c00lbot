import { Player } from '../../src/db/dao/player'
import { getPlayersInPug } from '../../src/bot/embeds/utils/players_in_pug'

describe('getPlayersInPug', () => {
    it('returns "No one joined yet" when given an empty array', () => {
        const players: Player[] = []
        expect(getPlayersInPug(players)).toBe('No one joined yet')
    })

    it('returns a list of players sorted by rating', () => {
        const players: Player[] = [
            { discord_id: '1234567890', nickname: 'player1', rating: 851 },
            { discord_id: '1234567891', nickname: 'player2', rating: 2000 },
            { discord_id: '1234567892', nickname: 'player3', rating: 1500 },
        ]
        expect(getPlayersInPug(players)).toBe(
            ':small_orange_diamond: **player2** (2k) :small_orange_diamond: **player3** (1.5k) :small_orange_diamond: **player1** (851)'
        )
    })
})
