/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { commandTimeouts } from '../commands'

export class CommandRestrictor {
    lastCommandsTime: Map<string, number> = new Map()

    runSpamFilter = async (playerId: string, commandName: string) => {
        const commandTimeout = commandTimeouts.get(commandName)
        if (commandTimeout === undefined) {
            console.log(`${commandName} command has no timeout filter, not checking for timeout!`)
            return true
        }

        const now = new Date().getTime() / 1000
        let allow = true

        if (this.lastCommandsTime.get(playerId + commandName)) {
            if (this.lastCommandsTime.get(playerId + commandName)! > (now - commandTimeout)) allow = false
        }

        if (allow) this.lastCommandsTime.set(playerId + commandName, now)

        return allow
    }
}
