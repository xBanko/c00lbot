import { ResolvedBet } from "../../pug/bet/bet"
import { sendEmbed } from "../bot"
import { getBetResultEmbed } from "../embeds/user/bets/bet_result_embed"

export const sendBetResultMsg = async (pugId: number, bets: ResolvedBet[]): Promise<void> => {
    const embed = getBetResultEmbed(pugId, bets)
    if (embed) {
        sendEmbed(embed)
    }
}