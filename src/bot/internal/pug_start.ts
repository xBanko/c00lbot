/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { CommandInteraction } from 'discord.js'
import { insertPassword } from '../../db/dao/password'

import { disc_bet_role_id } from '../../config'
import { getAllEnabledMaps } from '../../db/dao/map'
import { getModeById } from '../../db/dao/mode'
import { checkIfPlayerRegistered, getPlayersByDiscordId, getPlayersStr } from '../../db/dao/player'
import { getCurrentPug, updatePugPlayers, updatePugToStart } from '../../db/dao/pug'
import { insertEmptyScore } from '../../db/dao/score'
import { setServerBusy } from '../../db/dao/server'
import { setBettingOdds } from '../../pug/bet/bet'
import { makeTeams } from '../../pug/make_teams'
import { pickMap } from '../../pug/pick_map'
import { findOptimalServer } from '../../pug/server/find_optimal_server'
import { generateMultiplePasswords } from '../../pug/server/generate_password'
import { setupServer } from '../../pug/server/setup_server'
import { sendEmbedToUser, sendLogMessage, sendMessage } from '../bot'
import { getBetStartEmbed } from '../embeds/user/bets/bet_start_embed'
import { getDMUserEmbed } from '../embeds/user/dm_user_embed'
import { getJoinEmbed } from '../embeds/user/join_embed'
import { getPugFilledEmbed } from '../embeds/user/pug_filled_embed'
import { pugMode } from '../embeds/utils/constants'
import { checkIfDudeHasMapPreferenceSet, checkIfDudeHasServerPreferenceSet } from '../../pug/settings_checker'

export const addPlayer = async (interaction: CommandInteraction, userId: string) => {
    if (!await checkIfPlayerRegistered(userId)) {
        await interaction.editReply('Not registered, contact an admin')
        return
    }

    let livePug = await getCurrentPug(pugMode)
    const pugId = livePug.id!

    if (livePug.players.includes(userId)) {
        await interaction.editReply('Already in the pug')
        return
    }

    const mode = await getModeById(livePug.mode_id)
    if (livePug.players.length == mode.team_size * 2) {
        await interaction.editReply('Pug is already full')
        return
    }

    livePug.players.push(userId)
    const newPlayers = livePug.players.toString()

    await updatePugPlayers(newPlayers, pugId)

    livePug = await getCurrentPug(pugMode)

    if (livePug.players.length == mode.team_size * 2) {
        const scoreId = await insertEmptyScore(livePug.id!)

        const server = await findOptimalServer(newPlayers)
        const teams = makeTeams(await getPlayersByDiscordId(livePug.players), mode.team_size)
        const redTeam = teams.red.players.map(p => p.discord_id).join(',')
        const blueTeam = teams.blue.players.map(p => p.discord_id).join(',')
        const redStr = await getPlayersStr(teams.red.players.map(p => p.discord_id))
        const blueStr = await getPlayersStr(teams.blue.players.map(p => p.discord_id))
        const maps = await getAllEnabledMaps()
        const map = await pickMap(maps, livePug.players)
        const timePlus5 = new Date(new Date().getTime() + 5 * 60 * 1000).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })

        await updatePugToStart(redTeam, blueTeam, redStr, blueStr, server ? server.id : '', livePug.id!, map.id, scoreId)

        const odds = await setBettingOdds(livePug.id!)
        // update odds here

        if (!server) {
            await interaction.channel?.send('All servers seem busy. Please join a server of your choosing and the scoring will be done manually by admins.')
        } else {
            const passwords = generateMultiplePasswords(mode.team_size * 2 + 1)
            const serverPassword = passwords.join(';')

            // we dont need to await this, leave it unblocking
            setupServer(server.ip, server.port, map.name, serverPassword, server.shortname, livePug.id!, mode.name)

            await setServerBusy(server.shortname, livePug.id!, map.id, serverPassword)

            teams.red.players.sort((a, b) => Number(b.rating) - Number(a.rating))
            teams.blue.players.sort((a, b) => Number(b.rating) - Number(a.rating))

            for (let i = 0; i < mode.team_size; i++) {
                const player = teams.red.players[i]

                await insertPassword({ pug_id: livePug.id!, password: passwords[i], player_id: player.discord_id, team: 'RED' })
                await sendEmbedToUser(await getDMUserEmbed(player.discord_id, map.name, teams.red.players, server, passwords[i], pugMode, livePug.id!, 'red', timePlus5), player.discord_id)
            }

            for (let i = mode.team_size; i < mode.team_size*2; i++) {
                const player = teams.blue.players[i-mode.team_size]
                await insertPassword({ pug_id: livePug.id!, password: passwords[i], player_id: player.discord_id, team: 'BLUE' })
                await sendEmbedToUser(await getDMUserEmbed(player.discord_id, map.name, teams.blue.players, server, passwords[i], pugMode, livePug.id!, 'blue', timePlus5), player.discord_id)
            }

            await sendLogMessage(`**${mode.name}** [#${livePug.id}] has filled and all players need to click-in before __${timePlus5}__ on server **${server.shortname}** (${map.name})`)
        }

        const filledPugEmbed = await getPugFilledEmbed(livePug, teams, map)
        await interaction.editReply({ embeds: [filledPugEmbed] })

        const startBetEmbed = await getBetStartEmbed(odds, timePlus5)
        await interaction.followUp({ embeds: [startBetEmbed] })
        await sendMessage(`<@&${disc_bet_role_id}> bet with \`/bet\``)

    } else {
        const joinEmbed = await getJoinEmbed(userId)
        await Promise.all([checkIfDudeHasMapPreferenceSet(userId), checkIfDudeHasServerPreferenceSet(userId)])
        await interaction.editReply({ embeds: [joinEmbed] })
    }
}
