import { getPugById, LivePugScoredDto } from '../../db/dao/pug'
import { Score, updatePugScoreMsgId } from '../../db/dao/score'
import { fetchChannel, sendEmbed } from '../bot'
import { getPugConcludedEmbed } from '../embeds/user/pug_concluded_embed'
import { getPugFinishedEmbed } from '../embeds/user/pug_finished_embed'

export const sendScoreMsg = async (pugId: number, score_id: number, score_red: number, score_blue: number, user = 'bot'): Promise<string | undefined> => {
    const pug = await getPugById(pugId)
    const sentMessage = await sendEmbed(await getPugFinishedEmbed({
        ...pug,
        players: pug.players.split(','),
        team_red: pug.team_red.split(','),
        team_blue: pug.team_blue.split(','),
        score_blue,
        score_red,
        map_id: pug.map_id
    }))
    if (sentMessage) {
        await updatePugScoreMsgId(sentMessage.id, score_id)

        return sentMessage.id
    }
}

export const concludeScoreMsg = async (pug: LivePugScoredDto, score: Score) => {
    const channel = await fetchChannel()
    const msg = await channel!.messages.fetch(score.score_msg_id)
    const newEmbed = await getPugConcludedEmbed({
        ...pug,
        players: pug.players.split(','),
        team_red: pug.team_red.split(','),
        team_blue: pug.team_blue.split(','),
        score_blue: score.score_blue,
        score_red: score.score_red,
        map_id: pug.map_id
    })

    await msg.edit({ embeds: [newEmbed] })
}
