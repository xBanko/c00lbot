export class Voter {
    private votes: Map<string, Map<string, number>> = new Map()

    createVote = (voteId: string) => {
        this.votes.set(voteId, new Map<string, number>())
    }

    deleteVote = (voteId: string) => {
        this.votes.delete(voteId)
    }

    vote = (voteId: string, voteOption: string) => {
        const voteMap = this.votes.get(voteId)
        if (voteMap === undefined) {
            console.log(`cannot vote for an undefined Vote ${voteId}`)
            return
        }

        const currentVotes = voteMap.get(voteOption)
        if (currentVotes === undefined) {
            voteMap.set(voteOption, 1)
        } else voteMap.set(voteOption, currentVotes + 1)
    }

    getVotes = (voteId: string) => {
        return this.votes.get(voteId)
    }
}