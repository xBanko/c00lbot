/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { getModeById } from '../../db/dao/mode'
import { checkIfPlayerRegistered } from '../../db/dao/player'
import { getCurrentPug, updatePugPlayers } from '../../db/dao/pug'
import { sendMessage } from '../bot'
import { pugMode } from '../embeds/utils/constants'

export const removeDudeFromPugIfSigned = async (userId: string, status: string) => {
    if (!await checkIfPlayerRegistered(userId)) {
        return
    }

    const livePug = await getCurrentPug(pugMode)
    const mode = await getModeById(pugMode)

    if (livePug.players.includes(userId)) {
        const newPlayers = livePug.players.filter(p => p !== userId)
        await updatePugPlayers(newPlayers.join(','), livePug.id!)
        await sendMessage(`<@${userId}> was removed from **${mode.name}** [#${livePug.id}] because user went ${status}`)
    }
}
