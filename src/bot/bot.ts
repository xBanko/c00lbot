import { Routes } from 'discord-api-types/v9'
import { CacheType, Client, EmbedBuilder, GatewayIntentBits, GuildMember, Interaction, Message, MessageEditOptions, MessageCreateOptions, MessagePayload, Partials, PresenceUpdateStatus, TextChannel, User } from 'discord.js'

import { REST } from '@discordjs/rest'

import { disc_ban_channel_id, disc_bot_id, disc_channel_id, disc_log_channel_id, disc_server_id, disc_stats_channel_id, token } from '../config'
import { DeadPugger } from '../pug/dead_pug_manager'
import { adminCommandHandlers, adminCommands, buttonCommandHandlers, commandTimeouts, initAsyncCommands, interactionHandlers, userCommandHandlers, userCommands } from './commands'
import { CommandRestrictor } from './internal/command_timeout'
import { removeDudeFromPugIfSigned } from './internal/remove_person_from_pug'

export class Bot {
  public client!: Client
  public initialized = false
  public deadpugger!: DeadPugger
  private commandRestrictor!: CommandRestrictor

  init = async () => {

    this.client = new Client({
      intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.MessageContent,
        GatewayIntentBits.GuildMembers,
        GatewayIntentBits.DirectMessages,
        GatewayIntentBits.DirectMessageTyping,
        GatewayIntentBits.GuildPresences,
      ],
      partials: [Partials.Message, Partials.Channel, Partials.Reaction, Partials.User]
    })

    this.commandRestrictor = new CommandRestrictor()
    this.deadpugger = new DeadPugger()

    this.client.on('ready', () => {
      console.log(`Logged in as drek!`)
    })

    this.client.on('interactionCreate', async (interaction: Interaction<CacheType>) => {
      if (!interaction.isCommand() && !interaction.isAutocomplete() && !interaction.isButton()) return
      if (interaction.isCommand()) {
        if (adminCommands.some(cmd => cmd.name === interaction.commandName)) {
          await adminCommandHandlers.find(cmd => cmd.name === interaction.commandName)?.handle(interaction)
        }
  
        if (userCommands.some(cmd => cmd.name === interaction.commandName)) {
            const allow = await this.commandRestrictor.runSpamFilter(interaction.user.id, interaction.commandName)
            
            if (!allow) {
              const commandTimeout = commandTimeouts.get(interaction.commandName)
              await interaction.reply({ content: `Command triggered timeout, wait ${commandTimeout} seconds`, ephemeral: true })
              return
            }
    
            await userCommandHandlers.find(cmd => cmd.name === interaction.commandName)?.handle(interaction)
        }
      } else if (interaction.isButton()) {
        const actionId = interaction.customId.substr(0, interaction.customId.indexOf('-'))
        await buttonCommandHandlers.find(cmd => cmd.name === actionId)?.handle(interaction)
      } else {
        interactionHandlers.find(cmd => cmd.name === interaction.commandName)?.handleInteraction(interaction)
      }
    })

    this.client.on('presenceUpdate', (oldPresence, newPresence) => {
      if (!newPresence.member || !newPresence.member.id) return

      if (newPresence.status === PresenceUpdateStatus.Offline ||
        ((newPresence.clientStatus?.desktop && newPresence.clientStatus?.desktop === PresenceUpdateStatus.Idle) &&
          (!newPresence.clientStatus?.mobile || newPresence.clientStatus?.mobile === PresenceUpdateStatus.Idle))) {
        removeDudeFromPugIfSigned(newPresence.member.id, newPresence.status!)
      }
    })

    this.client.login(token)

    this.initialized = true

    
    await initAsyncCommands()

    loadCommands()
  }
}

export let bot: Bot
export const initBot = async () => {
  bot = new Bot()
  bot.init()
}

export const loadCommands = () => {
  try {
    console.log('Started refreshing application (/) commands.')
    const rest = new REST({ version: '10' }).setToken(token)

    rest.put(
      Routes.applicationGuildCommands(disc_bot_id, disc_server_id),
      { body: adminCommands.concat(userCommands) },
    )

    console.log('Successfully reloaded application (/) commands.')
  } catch (error) {
    console.error(error)
  }
}

export const sendMessage = async (msg: string) => {
  if (!bot || !bot.initialized) return
  try {
    return (bot.client.channels.cache.get(disc_channel_id) as TextChannel).send(msg)
  } catch (error) {
    console.error('error at sendMessage', error)
  }
}

export const sendLogMessage = async (msg: string) => {
  if (!bot || !bot.initialized) return
  try {
    console.log(`[${new Date().toISOString()}] ${msg}`)
    return (bot.client.channels.cache.get(disc_log_channel_id) as TextChannel).send(msg)
  } catch (error) {
    console.error('error at sendLogMessage', error)
  }
}

export const sendBanMessage = async (msg: string) => {
  if (!bot || !bot.initialized) return
  try {
    return (bot.client.channels.cache.get(disc_ban_channel_id) as TextChannel).send(msg)
  } catch (error) {
    console.error('error at sendBanMessage', error)
  }
}

export const sendStatsMessage = async (msg: string | MessagePayload | MessageEditOptions, msgId?: string): Promise<string | undefined> => {
  if (!bot || !bot.initialized) return
  try {
    const channel = await bot.client.channels.fetch(disc_stats_channel_id) as TextChannel

    if (msgId) {
      const statsMsg = await channel.messages.fetch(msgId)
      await statsMsg.edit(msg)

      return msgId
    }

    return (await channel.send(<MessagePayload>msg)).id

  } catch (error) {
    console.error('error at sendStatsMessage', error)
  }
}

export const sendMessageToUser = async (msg: string | MessagePayload | MessageCreateOptions, discordId: string) => {
  if (!bot || !bot.initialized) return
  try {
    const user = await bot.client.users.fetch(discordId.toString(), { cache: false })
    await user.send(msg)
  } catch (error) {
    console.error('error at sendMessageToUser', error)
  }
}

export const sendEmbedToUser = async (msg: EmbedBuilder, discordId: string): Promise<Message<boolean> | undefined> => {
  if (!bot || !bot.initialized) return
  try {
    const user = await bot.client.users.fetch(discordId.toString(), { cache: false })
    await user.send({ embeds: [msg] })
  } catch (error) {
    console.error('error at sendEmbedToUser', error)
  }
}



export const fetchChannel = async (): Promise<TextChannel | undefined> => {
  if (!bot || !bot.initialized) return
  try {
    return bot.client.channels.cache.get(disc_channel_id) as TextChannel
  } catch (error) {
    console.error('error at fetchChannel', error)
  }
}

export const sendEmbed = async (msg: EmbedBuilder): Promise<Message<boolean> | undefined> => {
  if (!bot || !bot.initialized) return
  try {
    return (bot.client.channels.cache.get(disc_channel_id) as TextChannel).send({ embeds: [msg] })
  } catch (error) {
    console.error('error at sendEmbed', error)
  }
}

export const fetchDiscordUser = async (discordId: string): Promise<User | undefined> => {
  if (!bot || !bot.initialized) return
  try {
    const user = await bot.client.users.fetch(discordId.toString(), { cache: false })
    return user
  } catch (error) {
    console.error('error at fetchDiscordUser', error)
  }
}

export const userToMember = async (user: User): Promise<GuildMember> => {
  const guild = await bot.client.guilds.fetch(disc_server_id)

  const member = await guild.members.fetch(user)

  return member
}
