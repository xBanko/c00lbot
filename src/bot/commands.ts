import { SlashCommandBuilder } from '@discordjs/builders'
import { AutocompleteInteraction, ButtonInteraction, CacheType, CommandInteraction } from 'discord.js'

import { adminMapCommand, adminMapCommandHandler } from './commands/admin/map'
import { adminModeCommand, adminModeCommandHandler } from './commands/admin/mode'
import { adminPlayerCommand, adminPlayerCommandHandler } from './commands/admin/player'
import { adminPugCommand, adminPugCommandHandler } from './commands/admin/pug'
import { adminRefreshCommand, adminRefreshCommandHandler } from './commands/admin/refresh'
import { adminServerCommand, adminServerCommandHandler } from './commands/admin/server'
import { banCommand, banCommandHandler } from './commands/user/bans'
import { betCommand, betCommandHandler } from './commands/user/bet'
import { betsCommand, betsCommandHandler } from './commands/user/bets'
import { coinsCommand, coinsCommandHandler, coinsInteractionHandler } from './commands/user/coins'
import { deadPugCommand, deadPugCommandHandler } from './commands/user/deadpug'
import { joinCommand, joinCommandHandler } from './commands/user/join'
import { lastCommand, lastCommandHandler } from './commands/user/last'
import { leaveCommand, leaveCommandHandler } from './commands/user/leave'
import { liastCommand, liastCommandHandler } from './commands/user/liast'
import { listCommand, listCommandHandler } from './commands/user/list'
import { mapCommand, mapCommandHandler } from './commands/user/map'
import { generateMapPreferenceButtonHandler, generateMapPreferenceCommand, generateMapPreferenceCommandHandler } from './commands/user/map_preference'
import { promoteCommand, promoteCommandHandler } from './commands/user/promote'
import { serverCommand, serverCommandHandler } from './commands/user/server'
import { generateServerPreferenceButtonHandler, generateServerPreferenceCommand, generateServerPreferenceCommandHandler } from './commands/user/server_preference'
import { spectateCommand, spectateCommandHandler } from './commands/user/spectate'
import { statsCommand, statsCommandHandler } from './commands/user/stats'

export interface CommandHandler {
    name: string
    description: string
    handle: (interaction: CommandInteraction<CacheType>) => unknown
}

export interface InteractionHandler {
    name: string
    handleInteraction: (interaction: AutocompleteInteraction<CacheType>) => unknown
}

export interface CommandHandler {
    name: string
    description: string
    handle: (interaction: CommandInteraction<CacheType>) => unknown
}

export interface ButtonHandler {
    name: string
    description: string
    handle: (interaction: ButtonInteraction<CacheType>) => unknown
}

export const userCommands: Omit<SlashCommandBuilder, 'addSubcommand' | 'addSubcommandGroup'>[] = []
export const adminCommands: Omit<SlashCommandBuilder, 'addSubcommand' | 'addSubcommandGroup'>[] = []

export const userCommandHandlers: CommandHandler[] = []
export const adminCommandHandlers: CommandHandler[] = []
export const buttonCommandHandlers: ButtonHandler[] = []

// use this block to generate async commands
let serverPreferenceCommand: SlashCommandBuilder
let serverPreferenceCommandHandler: CommandHandler
let serverPreferenceButtonCommandHandler: ButtonHandler
let mapPreferenceCommand: SlashCommandBuilder
let mapPreferenceCommandHandler: CommandHandler
let mapPreferenceButtonCommandHandler: ButtonHandler


export const initAsyncCommands = async () => {
    serverPreferenceCommand = await generateServerPreferenceCommand()
    serverPreferenceCommandHandler = generateServerPreferenceCommandHandler(serverPreferenceCommand)
    serverPreferenceButtonCommandHandler = generateServerPreferenceButtonHandler(serverPreferenceCommand)

    userCommands.push(serverPreferenceCommand)
    userCommandHandlers.push(serverPreferenceCommandHandler)
    buttonCommandHandlers.push(serverPreferenceButtonCommandHandler)

    mapPreferenceCommand = await generateMapPreferenceCommand()
    mapPreferenceCommandHandler = generateMapPreferenceCommandHandler(mapPreferenceCommand)
    mapPreferenceButtonCommandHandler = generateMapPreferenceButtonHandler(mapPreferenceCommand)

    userCommands.push(mapPreferenceCommand)
    userCommandHandlers.push(mapPreferenceCommandHandler)
    buttonCommandHandlers.push(mapPreferenceButtonCommandHandler)
}

adminCommands.push(
    adminMapCommand,
    adminModeCommand,
    adminPlayerCommand,
    adminPugCommand,
    adminServerCommand,
    adminRefreshCommand,
)

adminCommandHandlers.push(
    adminMapCommandHandler,
    adminModeCommandHandler,
    adminPlayerCommandHandler,
    adminPugCommandHandler,
    adminServerCommandHandler,
    adminRefreshCommandHandler,
)

userCommands.push(
    banCommand,
    betCommand,
    betsCommand,
    deadPugCommand,
    joinCommand,
    lastCommand,
    leaveCommand,
    liastCommand,
    listCommand,
    mapCommand,
    promoteCommand,
    serverCommand,
    spectateCommand,
    statsCommand,
    coinsCommand,
)

userCommandHandlers.push(
    banCommandHandler,
    betCommandHandler,
    betsCommandHandler,
    deadPugCommandHandler,
    joinCommandHandler,
    lastCommandHandler,
    leaveCommandHandler,
    liastCommandHandler,
    listCommandHandler,
    mapCommandHandler,
    promoteCommandHandler,
    serverCommandHandler,
    spectateCommandHandler,
    statsCommandHandler,
    coinsCommandHandler,
)


export const interactionHandlers: InteractionHandler[] = [
    coinsInteractionHandler,
]

export const commandTimeouts: Map<string, number> = new Map()
commandTimeouts.set('bans', 30)
commandTimeouts.set('coins', 30)
commandTimeouts.set('bet', 5)
commandTimeouts.set('bets', 10)
commandTimeouts.set('deadpug', 10)
commandTimeouts.set('join', 10)
commandTimeouts.set('last', 10)
commandTimeouts.set('leave', 10)
commandTimeouts.set('liast', 10)
commandTimeouts.set('list', 10)
commandTimeouts.set('map', 60)
commandTimeouts.set('promote', 120)
commandTimeouts.set('server', 30)
commandTimeouts.set('serverpreference', 5)
commandTimeouts.set('spectate', 5)
commandTimeouts.set('stats', 10)
