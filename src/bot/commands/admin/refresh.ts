import { CommandInteraction } from 'discord.js'

/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { SlashCommandBuilder } from '@discordjs/builders'

import { CommandHandler } from '../../commands'
import { generateSeasonalStats } from '../../../pug/stats/daily'
import { generateLeaderboard } from '../../../pug/stats/leaderboard'

export const adminRefreshCommand = new SlashCommandBuilder()
    .setName('adminrefresh')
    .setDescription('Refreshes the leaderboard and seasonal stats')

export const adminRefreshCommandHandler: CommandHandler = {
    ...adminRefreshCommand,
    handle: async (interaction: CommandInteraction) => {
        try {
            if (!interaction.isChatInputCommand()) return

            const seasonalTime = await generateSeasonalStats()
            await generateLeaderboard()

            await interaction.reply({ ephemeral: true, content: `Seasonal stats generated in ${seasonalTime} ms`})
        } catch (err) {
            console.log(err)
        }
    }
}
