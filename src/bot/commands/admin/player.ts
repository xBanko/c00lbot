import { CommandInteraction, GuildMember, User } from 'discord.js'

/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { SlashCommandBuilder } from '@discordjs/builders'

import { disc_banned_role_id } from '../../../config'
import { getAllCurrentBansForUser, insertBan, updateEndDate } from '../../../db/dao/ban'
import { addCoins, getPlayerName, insertPlayer, setPlayer } from '../../../db/dao/player'
import { getCurrentPug, updatePugPlayers } from '../../../db/dao/pug'
import { sendBanMessage } from '../../bot'
import { getBanEmbed } from '../../embeds/admin/ban_embed'
import { getLeaveEmbed } from '../../embeds/user/leave_embed'
import { pugMode } from '../../embeds/utils/constants'
import { addPlayer } from '../../internal/pug_start'
import { CommandHandler } from '../../commands'

export const adminPlayerCommand = new SlashCommandBuilder()
    .setName('adminplayer')
    .setDescription('Admin commands for player')

adminPlayerCommand
    .addSubcommand(subcommand =>
        subcommand
            .setName('add')
            .setDescription('Adds player to the pug')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('remove')
            .setDescription('Removes player from the pug')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('ban')
            .setDescription('Bans a player from the pug')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true))
            .addStringOption(option => option.setName('reason').setDescription('Reason').setRequired(true))
            .addIntegerOption(option => option.setName('duration').setDescription('Duration in hours').setRequired(true))
            .addStringOption(option => option.setName('where').setDescription('Where do you want to ban?').setRequired(true).addChoices(
                { name: 'All', value: 'all' },
                { name: 'Ranked pugs', value: 'ranked' },
                { name: 'Chill pugs', value: 'chill' },
            )))
    .addSubcommand(subcommand =>
        subcommand
            .setName('unban')
            .setDescription('Unbans a player from the pug')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true))
            .addStringOption(option => option.setName('where').setDescription('Where do you want to unban?').setRequired(true).addChoices(
                { name: 'All', value: 'all' },
                { name: 'Ranked pugs', value: 'ranked' },
                { name: 'Chill pugs', value: 'chill' },
            )))
    .addSubcommand(subcommand =>
        subcommand
            .setName('register')
            .setDescription('Adds a player to DB')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true))
            .addIntegerOption(option => option.setName('rating').setDescription('Set rating').setRequired(true)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('update')
            .setDescription('Updates the name and rating of a player')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true))
            .addStringOption(option => option.setName('nickname').setDescription('New nickname').setRequired(true))
            .addIntegerOption(option => option.setName('rating').setDescription('Set rating').setRequired(true)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('givecoins')
            .setDescription('Adds x amount of coins to user')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true))
            .addIntegerOption(option => option.setName('coins').setDescription('How many coins to add').setRequired(true)))

export const adminPlayerCommandHandler: CommandHandler = {
    ...adminPlayerCommand,
    handle: async (interaction: CommandInteraction) => {
        try {
            if (!interaction.isChatInputCommand()) return

            if (interaction.options.getSubcommand() === 'add') {
                await interaction.deferReply()
                const player: GuildMember = <GuildMember>interaction.options.getMentionable('player')!
                const userId = player.id
                await addPlayer(interaction, userId)
            }

            else if (interaction.options.getSubcommand() === 'remove') {
                const player: GuildMember = <GuildMember>interaction.options.getMentionable('player')!
                const livePug = await getCurrentPug(pugMode)

                if (!livePug.players.includes(player.id)) {
                    const playerName = (await getPlayerName(player.id)).nickname
                    await interaction.reply({ ephemeral: true, content: `${playerName} not in the pug??` })
                    return
                }

                const newPlayers = livePug.players.filter(p => p !== player.id)
                await updatePugPlayers(newPlayers.join(','), livePug.id!)
                const leaveEmbed = await getLeaveEmbed(player.id)
                await interaction.reply({ embeds: [leaveEmbed] })
            }

            else if (interaction.options.getSubcommand() === 'ban') {
                const admin = interaction.user
                const player: GuildMember = <GuildMember>interaction.options.getMentionable('player')!
                const reason = interaction.options.getString('reason')!
                const duration = interaction.options.getInteger('duration')!
                const whereToBan = interaction.options.getString('where')!
                const bannedTill = new Date()
                bannedTill.setHours(bannedTill.getHours() + duration)
                const endTime = bannedTill.toString()
                const playerName = (await getPlayerName(player.id)).nickname
                const adminName = (await getPlayerName(admin.id)).nickname


                if (whereToBan === 'all' || whereToBan === 'ranked') {
                    player.roles.add(disc_banned_role_id)
                    await insertBan({ discord_id: player.id, reason, duration, end_time: endTime, admin_id: admin.id, admin_name: adminName })
                }

                // Ban command specific for UTCTFPUG
                if (whereToBan === 'all' || whereToBan === 'chill') {
                    await sendBanMessage(`.ban discord_id:${player.id} reason:${reason} duration:${duration}`)
                }

                const banEmbed = await getBanEmbed(playerName, reason, duration, adminName, endTime, whereToBan)
                await interaction.reply({ embeds: [banEmbed] })
            }

            else if (interaction.options.getSubcommand() === 'unban') {
                const player: GuildMember = <GuildMember>interaction.options.getMentionable('player')!
                const whereToBan = interaction.options.getString('where')!
                const unbanNow = new Date().toString()
                const bansToUnban = await getAllCurrentBansForUser(player.id)
                const playerName = (await getPlayerName(player.id)).nickname
                let unbanStatus = false

                if (whereToBan === 'all' || whereToBan === 'ranked') {
                    await Promise.all(bansToUnban.map(async ban => {
                        unbanStatus = true
                        await updateEndDate(ban.id!, unbanNow)
                    }))
                }

                // Unban command specific for UTCTFPUG
                if (whereToBan === 'all' || whereToBan === 'chill') {
                    unbanStatus = true
                    await sendBanMessage(`.delban discord_id:${player.id}`)
                }

                if (!unbanStatus) {
                    await interaction.reply({ ephemeral: true, content: `${playerName} not banned currently` })
                } else {
                    await interaction.reply({ ephemeral: true, content: `${playerName} will be unbanned from ${whereToBan} pugs shortly` })
                }
            }

            else if (interaction.options.getSubcommand() === 'register') {
                try {
                    const player: User = (<GuildMember>interaction.options.getMentionable('player')).user
                    const rating: number = <number>interaction.options.getInteger('rating')!
                    await insertPlayer({ discord_id: player.id, nickname: player.username, rating, starting_rating: rating })
                    await interaction.reply({ ephemeral: true, content: `Registered player ${player.username} with rating ${rating}` })
                } catch (err) {
                    await interaction.reply({ ephemeral: true, content: `Player already registered!` })
                }
            }

            else if (interaction.options.getSubcommand() === 'update') {
                const player: GuildMember = <GuildMember>interaction.options.getMentionable('player')!
                const nickname = interaction.options.getString('nickname')!
                const rating = interaction.options.getInteger('rating')!
                const playerName = await setPlayer(player.id, rating, nickname)
                if (playerName.found === true) {
                    await interaction.reply({ ephemeral: true, content: `**${nickname}** updated with rating ${rating}` })
                } else {
                    await interaction.reply({ ephemeral: true, content: `**${playerName.nickname}** does not exist` })
                }
            }

            else if (interaction.options.getSubcommand() === 'givecoins') {
                const player: GuildMember = <GuildMember>interaction.options.getMentionable('player')!
                const coins = interaction.options.getInteger('coins')!
                const playerFound = await addCoins(player.id, coins)
                const playerName = (await getPlayerName(player.id)).nickname

                if (playerFound.found === true) {
                    await interaction.reply({ content: `**${playerName}** given ${coins} coins!` })
                } else {
                    await interaction.reply({ ephemeral: true, content: `**${playerName}** does not exist` })
                }
            }
        } catch (err) {
            console.log(err)
        }
    }
}
