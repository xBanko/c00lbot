/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { SlashCommandBuilder } from '@discordjs/builders'
import { BetDeadOption, resolveBets } from '../../../pug/bet/bet'

import { getPlayerByDiscordId } from '../../../db/dao/player'
import { getLivePugById, getPugById, updateRatings } from '../../../db/dao/pug'
import { insertScore, updateScore, updateScoreStatus } from '../../../db/dao/score'
import { setServerFreeId } from '../../../db/dao/server'
import { calculateElo } from '../../../pug/elo_calculation'
import { generateLeaderboard } from '../../../pug/stats/leaderboard'
import { CommandHandler } from '../../commands'
import { getUnconcludedPugsEmbed } from '../../embeds/admin/unconcluded_pugs_embed'
import { getPugConcludedEmbed } from '../../embeds/user/pug_concluded_embed'
import { ScoreStatus } from '../../embeds/utils/constants'
import { whoWon } from '../../../pug/who_won'
import { sendBetResultMsg } from '../../../bot/internal/bet_result'
import { addCoinsToPlayer } from '../../../db/dao/bet'
import { isPugDoubleElo, resolveBoostedDeadPug } from '../../../db/dao/pug_boosts'

export const adminPugCommand = new SlashCommandBuilder()
    .setName('adminpug')
    .setDescription('Commands for dealing with specific pugs')
adminPugCommand
    .addSubcommand(subcommand =>
        subcommand
            .setName('kill')
            .setDescription('Kills a pug. Basically a deadpug for admins')
            .addIntegerOption(option => option.setName('pugid').setDescription('Pug ID').setRequired(true)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('score')
            .setDescription('Manually scores a pug')
            .addIntegerOption(option => option.setName('pugid').setDescription('Pug ID').setRequired(true))
            .addIntegerOption(option => option.setName('scorered').setDescription('RED team score').setRequired(true))
            .addIntegerOption(option => option.setName('scoreblue').setDescription('BLUE team score').setRequired(true)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('listunconcluded')
            .setDescription('Returns a list of all unconcluded pugs'))

export const adminPugCommandHandler: CommandHandler = {
    ...adminPugCommand,
    handle: async (interaction) => {
        try {
            if (!interaction.isChatInputCommand()) return

            if (interaction.options.getSubcommand() === 'kill') {

                await interaction.deferReply()

                const userId: string = interaction.user.id
                const pugId: number = interaction.options.getInteger('pugid')!

                const pugToScore = await getLivePugById(pugId)
                if (pugToScore === undefined) {
                    await interaction.editReply({ content: `[#${pugId}] is either concluded or non-existent` })
                    return
                }

                await setServerFreeId(pugToScore.server_id)
                await updateScore(0, 0, pugToScore.score_id, ScoreStatus.Rejected)
                await resolveBets(BetDeadOption.Dead, pugId.toString())
                await resolveBoostedDeadPug(pugId)

                console.log(`[${new Date().toISOString()}] #${pugId} pug has been killed by ${(await getPlayerByDiscordId(userId)).nickname}`)

                await interaction.editReply(`[#${pugId}] has been killed! :headstone:`)
            }

            else if (interaction.options.getSubcommand() === 'score') {

                await interaction.deferReply()

                const userId: string = interaction.user.id
                const pugId: number = interaction.options.getInteger('pugid')!
                const scoreRed: number = interaction.options.getInteger('scorered')!
                const scoreBlue: number = interaction.options.getInteger('scoreblue')!

                const pugToScore = await getPugById(pugId)
                if (pugToScore === undefined) {
                    await interaction.editReply({ content: 'No such pug found' })
                    return
                }

                if (pugToScore.rating_change_blue != undefined && pugToScore.rating_change_red != undefined) {
                    await interaction.editReply({ content: 'The pug is concluded, cannot force score' })
                    return
                }

                await setServerFreeId(pugToScore.server_id)

                const doubleElo = await isPugDoubleElo(parseInt(pugToScore.id))
                const newElos = await calculateElo(pugToScore.str_red, pugToScore.str_blue, scoreRed, scoreBlue, doubleElo)
                await updateScore(0, 0, pugToScore.score_id, ScoreStatus.Rejected)
                const scoreId = await insertScore(scoreRed, scoreBlue, pugId, userId)
                await updateRatings(pugToScore, newElos.teamRed.delta, newElos.teamBlue.delta)
                await updateScoreStatus(scoreId.toString(), ScoreStatus.Manual)

                const embed = await getPugConcludedEmbed({
                    ...pugToScore,
                    players: pugToScore.players.split(','),
                    team_red: pugToScore.team_red.split(','),
                    team_blue: pugToScore.team_blue.split(','),
                    score_blue: scoreBlue,
                    score_red: scoreRed,
                    rating_change_blue: newElos.teamBlue.delta,
                    rating_change_red: newElos.teamRed.delta,
                    finished: new Date(),
                })

                generateLeaderboard()

                await Promise.all(pugToScore.players.split(',').map(async p => await addCoinsToPlayer(p, 25)))
                const resolvedBets = await resolveBets(whoWon(scoreRed, scoreBlue), pugId.toString())
                await sendBetResultMsg(pugId, resolvedBets)

                await interaction.editReply({ embeds: [embed] })
            }

            else if (interaction.options.getSubcommand() === 'listunconcluded') {
                const unconcludedPugsEmbed = await getUnconcludedPugsEmbed()
                await interaction.reply({ ephemeral: true, embeds: [unconcludedPugsEmbed] })
            }

        } catch (err) {
            console.log(err)
        }
    }
}
