import { CommandInteraction } from 'discord.js'

/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { SlashCommandBuilder } from '@discordjs/builders'

import { insertMode } from '../../../db/dao/mode'
import { CommandHandler } from '../../commands'

export const adminModeCommand = new SlashCommandBuilder()
    .setName('adminmode')
    .setDescription('Admin commands for modes')

adminModeCommand
    .addSubcommand(subcommand =>
        subcommand
            .setName('add')
            .setDescription('Adds a mode to pugs')
            .addStringOption(option => option.setName('name').setDescription('Mode name (like nwctf)').setRequired(true))
            .addIntegerOption(option => option.setName('team_size').setDescription('How many people per team?').setRequired(true)))

export const adminModeCommandHandler: CommandHandler = {
    ...adminModeCommand,
    handle: async (interaction: CommandInteraction) => {
        try {
            if (!interaction.isChatInputCommand()) return;

            if (interaction.options.getSubcommand() === 'add') {
                const name = interaction.options.getString('name')!
                const teamSize = interaction.options.getInteger('team_size')!
                await insertMode({ name, team_size: teamSize })
                await interaction.reply({ ephemeral: true, content: `Mode **${name}** added with team size **${teamSize}**` })
            }
        } catch (err) {
            console.log(err)
        }
    }
}
