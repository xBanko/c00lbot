import { CommandInteraction } from 'discord.js'

/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { SlashCommandBuilder } from '@discordjs/builders'

import { addServer, removeServer, setServer, setServerStatus } from '../../../db/dao/server'
import { CommandHandler } from '../../commands'

export const adminServerCommand = new SlashCommandBuilder()
    .setName('adminserver')
    .setDescription('Server commands')

adminServerCommand.addSubcommand(subcommand =>
    subcommand
        .setName('add')
        .setDescription('Adds a server')
        .addStringOption(option => option.setName('ip').setDescription('Ip WITHOUT unreal:// (example: ranked2.utctfpug.com)').setRequired(true))
        .addStringOption(option => option.setName('port').setDescription('Port (example 7777)').setRequired(true))
        .addStringOption(option => option.setName('shortname').setDescription('Descriptive short name. example: ranked2').setRequired(true))
        .addStringOption(option => option.setName('modes').setDescription('Comma separated mode ids that this server can be used for. example: 1,2').setRequired(true))
        .addIntegerOption(option => option.setName('status').setDescription('Status you want to set').setRequired(true).addChoices(
            { name: 'Enabled', value: 1 },
            { name: 'Disabled', value: 0 }
        ))
        .addStringOption(option => option.setName('country').setDescription('Country (DE, UK, NL, ...)').setRequired(true)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('changestatus')
            .setDescription('Enables or Disables a server')
            .addStringOption(option => option.setName('shortname').setDescription('Descriptive short name. example: ranked2').setRequired(true))
            .addIntegerOption(option => option.setName('status').setDescription('Status you want to set').setRequired(true).addChoices(
                { name: 'Enabled', value: 1 },
                { name: 'Disabled', value: 0 }
            )))
    .addSubcommand(subcommand =>
        subcommand
            .setName('remove')
            .setDescription('Removes a server')
            .addStringOption(option => option.setName('shortname').setDescription('Descriptive short name. example: ranked2').setRequired(true)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('update')
            .setDescription('Updates a server')
            .addStringOption(option => option.setName('shortname').setDescription('Descriptive short name. example: ranked2').setRequired(true))
            .addStringOption(option => option.setName('ip').setDescription('IP WITHOUT unreal:// (example: ranked2.utctfpug.com)').setRequired(true))
            .addStringOption(option => option.setName('port').setDescription('Port (example 7777)').setRequired(true))
            .addStringOption(option => option.setName('modes').setDescription('Comma separated mode ids that this server can be used for. example: 1,2').setRequired(true)).addIntegerOption(option => option.setName('status').setDescription('Status you want to set').setRequired(true).addChoices(
                { name: 'Enabled', value: 1 },
                { name: 'Disabled', value: 0 }
            ))
            .addStringOption(option => option.setName('country').setDescription('Country (DE, UK, NL, ...)').setRequired(true)))

export const adminServerCommandHandler: CommandHandler = {
    ...adminServerCommand,
    handle: async (interaction: CommandInteraction) => {
        try {
            if (!interaction.isChatInputCommand()) return

            if (interaction.options.getSubcommand() === 'add') {
                const serverName = interaction.options.getString('shortname')!
                await addServer(serverName,
                    interaction.options.getString('ip')!,
                    interaction.options.getString('port')!,
                    interaction.options.getString('modes')!,
                    interaction.options.getInteger('status')!,
                    interaction.options.getString('country')!.toUpperCase()
                )
                await interaction.reply({ ephemeral: true, content: `**${serverName}** added` })
            }

            else if (interaction.options.getSubcommand() === 'changestatus') {
                const serverName = interaction.options.getString('shortname')!
                const server = await setServerStatus(serverName, interaction.options.getInteger('status')!)
                if (server.found === true) {
                    await interaction.reply({ ephemeral: true, content: `**${serverName}** status updated` })
                } else {
                    await interaction.reply({ ephemeral: true, content: `**${serverName}** does not exist` })
                }
            }

            else if (interaction.options.getSubcommand() === 'remove') {
                const serverName = interaction.options.getString('shortname')!
                const server = await removeServer(serverName)
                if (server.found === false) {
                    await interaction.reply({ ephemeral: true, content: `**${serverName}** does not exist, check spelling and version` })
                } else if (server.deleted === false) {
                    await interaction.reply({ ephemeral: true, content: `**${serverName}** could not be removed, it has already been used! Change status instead.` })
                } else {
                    await interaction.reply({ ephemeral: true, content: `**${serverName}** removed` })
                }
            }

            else if (interaction.options.getSubcommand() === 'update') {
                const serverName = interaction.options.getString('shortname')!
                const server = await setServer(serverName,
                    interaction.options.getString('ip')!,
                    interaction.options.getString('port')!,
                    interaction.options.getString('modes')!,
                    interaction.options.getInteger('status')!,
                    interaction.options.getString('country')!.toUpperCase())
                if (server.found === true) {
                    await interaction.reply({ ephemeral: true, content: `**${serverName}** updated` })
                } else {
                    await interaction.reply({ ephemeral: true, content: `**${serverName}** does not exist` })
                }
            }
        } catch (err) {
            console.log(err)
        }
    }
}
