/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { SlashCommandBuilder } from '@discordjs/builders'

import { sendMessage } from '../../../bot/bot'
import { getModeById } from '../../../db/dao/mode'
import { getCurrentPug } from '../../../db/dao/pug'
import { CommandHandler } from '../../commands'
import { getPromoteEmbed } from '../../embeds/user/promote.embed'
import { pugMode } from '../../embeds/utils/constants'

export const promoteCommand = new SlashCommandBuilder()
    .setName('promote')
    .setDescription('Promotes the pug with a @here mention')

export const promoteCommandHandler: CommandHandler = {
    ...promoteCommand,
    handle: async (interaction) => {
        try {
            const mode = await (await getModeById(pugMode))
            const livePug = await getCurrentPug(pugMode)
            if (livePug.players.length > 0) {
                await sendMessage(`@here`)
                const promoteEmbed = await getPromoteEmbed(livePug, mode)
                await interaction.reply({ embeds: [promoteEmbed] })
            } else {
                await interaction.reply({ ephemeral: true, content: `Can't promote empty pug` })
            }
        } catch (err) {
            console.log(err)
        }
    }
}
