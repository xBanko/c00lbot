/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { GuildMember } from 'discord.js'

import { SlashCommandBuilder } from '@discordjs/builders'

import { getPlayerMapsEmbed } from '../../../bot/embeds/user/stats/player_maps_embed'
import { SeasonChoice } from '../../../bot/embeds/utils/constants'
import { getEnemyWinrateAll, getEnemyWinrateSeason } from '../../../db/dao/pug'
import { generateProgressImageForUser } from '../../../pug/stats/progress'
import { calculateWinrate } from '../../../pug/stats/winrate'
import { CommandHandler } from '../../commands'
import { getBestPugsEmbed } from '../../embeds/user/stats/best_pugs_embed'
import { getLeaderboardEmbed } from '../../embeds/user/stats/leaderboard_embed'
import { getMapStatsEmbed } from '../../embeds/user/stats/map_stats_embed'
import { getCombinedPerfEmbed, getPerfEmbed } from '../../embeds/user/stats/perf_embed'
import { getPugStatsEmbed } from '../../embeds/user/stats/pug_stats_embed'
import { getRatingTopEmbed } from '../../embeds/user/stats/rating_top_embed'
import { getWorstPugsEmbed } from '../../embeds/user/stats/worst_pugs_embed'
import { generatePugsCountGraph } from '../../../pug/stats/pugs_count'

export const statsCommand = new SlashCommandBuilder()
    .setName('stats')
    .setDescription('Shows various stats')

statsCommand
    .addSubcommand(subcommand =>
        subcommand
            .setName('compareprogress')
            .setDescription('Compares progress')
            .addMentionableOption(option => option.setName('player1').setDescription('Player to compare to').setRequired(true))
            .addMentionableOption(option => option.setName('player2').setDescription('Player to compare to'))
            .addMentionableOption(option => option.setName('player3').setDescription('Player to compare to'))
            .addMentionableOption(option => option.setName('player4').setDescription('Player to compare to'))
            .addIntegerOption(option => option.setName('history').setDescription('How many pugs back? Default: 20  Max: 40'))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('myprogress')
            .setDescription('Your progress')
            .addIntegerOption(option => option.setName('history').setDescription('How many pugs back? Default: 20, Max: 40')))
    
    .addSubcommand(subcommand =>
        subcommand
            .setName('pugcount')
            .setDescription('Graph of pugs played'))
        
    .addSubcommand(subcommand =>
        subcommand
            .setName('leaderboard')
            .setDescription('Displays leaderboard for span of players')
            .addIntegerOption(option => option.setName('quantity').setDescription('Limit').setRequired(true).addChoices(
                { name: '1-10', value: 0 },
                { name: '11-20', value: 10 },
                { name: '21-30', value: 20 },
                { name: '31-40', value: 30 },
                { name: '41-50', value: 40 },
                { name: '51-60', value: 50 },
                { name: '61-70', value: 60 },
                { name: '71-80', value: 70 },
                { name: '81-90', value: 80 },
                { name: '91-100', value: 90 }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('bestpugs')
            .setDescription('Displays strongest pugs')
            .addStringOption(option => option.setName('season').setDescription('When? Default: This season').addChoices(
                { name: 'All time', value: `${SeasonChoice.All}` },
                { name: 'This season', value: `${SeasonChoice.Current}` }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('performance')
            .setDescription('Player performance stats')
            .addMentionableOption(option => option.setName('player1').setDescription('Player'))
            .addStringOption(option => option.setName('season').setDescription('When? Default: This season').addChoices(
                { name: 'All time', value: `${SeasonChoice.All}` },
                { name: 'This season', value: `${SeasonChoice.Current}` }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('performancecombined')
            .setDescription('Performance of 2 players stats')
            .addMentionableOption(option => option.setName('player1').setDescription('Player').setRequired(true))
            .addMentionableOption(option => option.setName('player2').setDescription('Player').setRequired(true))
            .addStringOption(option => option.setName('season').setDescription('When? Default: This season').addChoices(
                { name: 'All time', value: `${SeasonChoice.All}` },
                { name: 'This season', value: `${SeasonChoice.Current}` }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('ratingtop')
            .setDescription('Shows the most spectacular rating changes')
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('pugs')
            .setDescription('Shows stats for the pug')
            .addStringOption(option => option.setName('when').setDescription('Todays pugs or all time stats').setRequired(true).addChoices(
                { name: 'Today', value: 'today' },
                { name: 'This Season', value: `${SeasonChoice.Current}` },
                { name: 'All time', value: `${SeasonChoice.All}` }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('maps')
            .setDescription('Shows the map stats within a time span with avg caps and avg cap difference')
            .addStringOption(option => option.setName('time').setDescription('When?').setRequired(true).addChoices(
                { name: 'Today', value: 'today' },
                { name: 'Yesterday', value: 'yesterday' },
                { name: 'Three days', value: 'three' },
                { name: 'This week', value: 'week' },
                { name: 'This month', value: 'month' },
                { name: 'This year', value: 'year' },
                { name: 'This season', value: `${SeasonChoice.Current}` },
                { name: 'All time', value: `${SeasonChoice.All}` }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('worstpugs')
            .setDescription('Like best pugs but worse')
            .addStringOption(option => option.setName('season').setDescription('When? Default: This season').addChoices(
                { name: 'All time', value: `${SeasonChoice.All}` },
                { name: 'This season', value: `${SeasonChoice.Current}` }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('playermaps')
            .setDescription('Map stats for specific player')
            .addMentionableOption(option => option.setName('player').setDescription('Player'))
            .addStringOption(option => option.setName('time').setDescription('When? Default: This season').addChoices(
                { name: 'This season', value: `${SeasonChoice.Current}` },
                { name: 'All time', value: `${SeasonChoice.All}` }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

export const statsCommandHandler: CommandHandler = {
    ...statsCommand,
    handle: async (interaction) => {
        try {
            if (!interaction.isChatInputCommand()) return
            const season = <SeasonChoice>interaction.options.getString('season') ?? SeasonChoice.Current
            const hidden = interaction.options.getBoolean('hidden') ?? false

            if (interaction.options.getSubcommand() === 'compareprogress') {
                await interaction.deferReply({ ephemeral: hidden })

                const ids = []

                const player1: GuildMember = <GuildMember>interaction.options.getMentionable('player1')
                const player2: GuildMember = <GuildMember>interaction.options.getMentionable('player2')
                const player3: GuildMember = <GuildMember>interaction.options.getMentionable('player3')
                const player4: GuildMember = <GuildMember>interaction.options.getMentionable('player4')

                if (player1) ids.push(player1.id)
                if (player2) ids.push(player2.id)
                if (player3) ids.push(player3.id)
                if (player4) ids.push(player4.id)

                const limit = interaction.options.getInteger('history')

                const image = await generateProgressImageForUser(ids, limit ?? undefined)
                await interaction.editReply({ files: [{ attachment: image }] })
            }

            else if (interaction.options.getSubcommand() === 'myprogress') {
                await interaction.deferReply({ ephemeral: hidden })

                const limit = interaction.options.getInteger('history')
                const image = await generateProgressImageForUser([interaction.user.id], limit ?? undefined)

                await interaction.editReply({ files: [{ attachment: image }] })
            }

            else if (interaction.options.getSubcommand() === 'pugcount') {
                await interaction.deferReply({ ephemeral: hidden })

                const image = await generatePugsCountGraph()

                await interaction.editReply({ files: [{ attachment: image }] })
            }

            else if (interaction.options.getSubcommand() === 'leaderboard') {
                const leaderboardEmbed = await getLeaderboardEmbed(interaction.options.getInteger('quantity')!)
                await interaction.reply({ ephemeral: hidden, embeds: [leaderboardEmbed] })
            }

            else if (interaction.options.getSubcommand() === 'bestpugs') {
                const bestPugsEmbed = await getBestPugsEmbed(season)
                await interaction.reply({ ephemeral: hidden, embeds: [bestPugsEmbed] })
            }

            else if (interaction.options.getSubcommand() === 'performance') {
                const player1: GuildMember = <GuildMember>interaction.options.getMentionable('player1')

                const winrate = await calculateWinrate(season, player1 ? player1.id : interaction.user.id)
                const winrateEmbed = await getPerfEmbed(winrate, player1 ? player1.id : interaction.user.id)
                await interaction.reply({ ephemeral: hidden, embeds: [winrateEmbed] })
                return
            }

            else if (interaction.options.getSubcommand() === 'performancecombined') {
                const player1: GuildMember = <GuildMember>interaction.options.getMentionable('player1')!
                const player2: GuildMember = <GuildMember>interaction.options.getMentionable('player2')!

                if (player1 === player2) {
                    await interaction.reply({ ephemeral: hidden, content: 'Wow you found a bug, honestly, you did' })
                    return
                }

                if (player1 && player2) {
                    let enemyWinrate
                    const winrate = await calculateWinrate(season, player1.id, player2.id)
                    if (season === SeasonChoice.Current) {
                        enemyWinrate = await getEnemyWinrateSeason(player1.id, player2.id)
                    } else {
                        enemyWinrate = await getEnemyWinrateAll(player1.id, player2.id)
                    }
                    const combinedPerfEmbed = await getCombinedPerfEmbed(winrate, enemyWinrate, player1, player2)
                    await interaction.reply({ ephemeral: hidden, embeds: [combinedPerfEmbed] })
                    return
                }
            }

            else if (interaction.options.getSubcommand() === 'ratingtop') {
                const ratingTopEmbed = await getRatingTopEmbed()
                await interaction.reply({ ephemeral: hidden, embeds: [ratingTopEmbed] })
            }

            else if (interaction.options.getSubcommand() === 'pugs') {
                const pugStatsEmbed = await getPugStatsEmbed(<SeasonChoice>interaction.options.getString('when')!)
                await interaction.reply({ ephemeral: hidden, embeds: [pugStatsEmbed] })
            }

            else if (interaction.options.getSubcommand() === 'maps') {
                const mapStatsEmbed = await getMapStatsEmbed(interaction.options.getString('time')!)
                await interaction.reply({ ephemeral: hidden, embeds: [mapStatsEmbed] })
            }
            else if (interaction.options.getSubcommand() === 'worstpugs') {
                const worstPugsEmbed = await getWorstPugsEmbed(season)
                await interaction.reply({ ephemeral: hidden, embeds: [worstPugsEmbed] })
            }
            else if (interaction.options.getSubcommand() === 'playermaps') {
                const player: GuildMember = <GuildMember>interaction.options.getMentionable('player') ?? <GuildMember>interaction.member
                const playerMapsEmbed = await getPlayerMapsEmbed(interaction.options.getString('time')! as SeasonChoice, player.id)
                await interaction.reply({ ephemeral: hidden, embeds: [playerMapsEmbed] })
            }
        } catch (err) {
            console.log(err)
        }
    }
}
