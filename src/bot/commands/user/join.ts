import config from 'config'

import { SlashCommandBuilder } from '@discordjs/builders'

import { getLivePugForUser } from '../../../db/dao/pug'
import { addPlayer } from '../../internal/pug_start'
import { CommandHandler } from '../../commands'
import { getJoinTimeoutEmbed } from '../../embeds/user/join_timeout_embed'

export const joinCommand = new SlashCommandBuilder()
    .setName('join')
    .setDescription('Lets you join the pug')

export const joinCommandHandler: CommandHandler = {
    ...joinCommand,
    handle: async (interaction) => {
        try {
            await interaction.deferReply()
            const userId = interaction.user.id
            const pug = await getLivePugForUser(interaction.user.id)
            if (pug) {
                const timeLimit = new Date(pug.started)

                timeLimit.setMinutes(timeLimit.getMinutes() + <number>config.get('scoring.timeoutMinutes'))

                if (timeLimit > new Date()) {
                    await interaction.editReply({ embeds: [await getJoinTimeoutEmbed(pug.id, Math.round((timeLimit.getTime() - new Date().getTime()) / 60000).toString())] })
                    return
                }
            }
            await addPlayer(interaction, userId)
        } catch (err) {
            console.log(err)
        }
    }
}
