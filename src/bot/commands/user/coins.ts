import { SlashCommandBuilder } from '@discordjs/builders'

import { CommandHandler, InteractionHandler } from '../../commands'
import { doesPlayerHaveMoneyz, removeCoinsFromPlayer } from '../../../db/dao/bet'
import { bumpMapWeight, getAllEnabledMaps } from '../../../db/dao/map'
import { AutocompleteInteraction } from 'discord.js'
import { getMapBoostEmbed } from '../../../bot/embeds/user/map_boost_embed'
import { getActiveEloBoost, updatePugForBoost, updateValueForBoost, updateValueForBoostFinish } from '../../../db/dao/pug_boosts'
import { getEloBoostEmbed, getEloBoostFullEmbed } from '../../../bot/embeds/user/elo_boost_embed'
import { pugMode } from '../../embeds/utils/constants'
import { getCurrentPug } from '../../../db/dao/pug'

export const coinsCommand = new SlashCommandBuilder()
    .setName('coins')
    .setDescription('Options to use your coins')

coinsCommand.addSubcommand(subcommand =>
    subcommand
        .setName('boostmap')
        .setDescription('Boost a map')
        .addStringOption(option => option.setName('map').setDescription('Map name').setRequired(true).setAutocomplete(true))
        .addIntegerOption(option => option.setName('coins').setDescription('How many coins? (500 coins = 1 weight increase, 50 = min)').setRequired(true).setMinValue(50)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('boostelo')
            .setDescription('Boost elo for next pug')
            .addIntegerOption(option => option.setName('coins').setDescription('How many coins?').setRequired(true).setMinValue(50)))

export const coinsInteractionHandler: InteractionHandler = {
    ...coinsCommand,
    handleInteraction: async (interaction) => {
        if (interaction.isAutocomplete()) {
            const command = (<AutocompleteInteraction>interaction).options.getSubcommand()
    
            if (command === 'boostmap') {
                const focusedValue = interaction.options.getFocused()
                const choices = await (await getAllEnabledMaps()).map(map => map.name.slice(4))
                const filtered = choices.filter(choice => choice.startsWith(focusedValue) || choice.toLowerCase().startsWith(focusedValue))

                await interaction.respond(
                    filtered.map(choice => ({ name: choice, value: choice })),
                )
            }
        }
    }
}

export const coinsCommandHandler: CommandHandler = {
    ...coinsCommand,

    handle: async (interaction) => {
        try {
            if (!interaction.isChatInputCommand()) return

            const playerId = interaction.user.id
            const amount = Math.round(interaction.options.getInteger('coins')!)

            if (!await doesPlayerHaveMoneyz(playerId, amount)) {
                await interaction.reply({ ephemeral: true, content: `You simply don't have the coins` })
                return
            }

            if (interaction.options.getSubcommand() === 'boostmap') { 
                const map = interaction.options.getString('map')!

                if (!((await getAllEnabledMaps()).map(m => m.name)).includes('CTF-' + map)) {
                    await interaction.reply({ ephemeral: true, content: `There is no such map enabled` })
                    return
                }

                const weightBump = Math.round(amount/250 * 100) / 100
                await bumpMapWeight(weightBump, 'CTF-' + map)

                const embed = await getMapBoostEmbed(map, weightBump, interaction.user.username)

                await removeCoinsFromPlayer(playerId, amount)
                await interaction.reply({ embeds: [embed]})

            } else if (interaction.options.getSubcommand() === 'boostelo') {
                const currentBoost = await getActiveEloBoost()
                const valueTillBoost = 10000 - currentBoost.value

                if (amount >= valueTillBoost) {
                    // full boost
                    const livePug = await getCurrentPug(pugMode)

                    await updateValueForBoostFinish(currentBoost.id)
                    await updatePugForBoost(parseInt(livePug.id!), currentBoost.id)

                    if (amount > valueTillBoost) {
                        await removeCoinsFromPlayer(playerId, valueTillBoost)
                    } else {
                        await removeCoinsFromPlayer(playerId, amount)
                    }

                    const boostEmbed = await getEloBoostFullEmbed(amount, currentBoost.value + amount, interaction.user.username)
                    await interaction.reply({ embeds: [boostEmbed]})
                } else {
                    await updateValueForBoost(amount, currentBoost.id)
                    await removeCoinsFromPlayer(playerId, amount)
                    
                    const boostEmbed = await getEloBoostEmbed(amount, currentBoost.value + amount, interaction.user.username)
                    await interaction.reply({ embeds: [boostEmbed]})
                }
            }
        } catch (err) {
            console.log(err)
        }
    }
}

