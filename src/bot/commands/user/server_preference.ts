/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { SlashCommandBuilder } from '@discordjs/builders'
import { ActionRowBuilder, ButtonBuilder, ButtonInteraction, ButtonStyle, CommandInteraction } from 'discord.js'
import { getPreferencesForPlayers, insertServerPreference } from '../../../db/dao/server_preference'
import { flagToEmoji } from '../../../bot/embeds/utils/flags'

import { getAllServers } from '../../../db/dao/server'


export const generateServerPreferenceCommand = async () => {
    const serverPreferenceCommand = new SlashCommandBuilder()
    .setName('serverpreferences')
    .setDescription(`Sets your preferential server, such as 'ranked1'`)

    serverPreferenceCommand.addSubcommand(subcommand =>
        subcommand
            .setName('set')
            .setDescription('Sets your server preference')

            )
    .addSubcommand(subcommand =>
        subcommand
            .setName('get')
            .setDescription('Gets your server preference'))

    return serverPreferenceCommand
}

export const generateServerPreferenceButtonHandler = (command: SlashCommandBuilder) => {
    return {
        ...command,
        name: 'servpref',
        handle: async (interaction: ButtonInteraction) => {
            try {
                if (!interaction.isButton()) return
    
                    const shortname = interaction.customId.slice(9, -2)
                    const value = parseInt(interaction.customId.slice(-1)) -1

                    await insertServerPreference(interaction.user.id, shortname, value)

                    const servers = await getAllServers()
                    const serverNames = servers.map(s => s.shortname)
                    const nextServerIndex = serverNames.indexOf(shortname) + 1
                    
                    const currentPreferences = await getPreferencesForPlayers(interaction.user.id)

                    if (nextServerIndex + 1 > servers.length) {
                        let embedValue = ''
                        for (let i = 0; i<nextServerIndex; i++) {
                            embedValue += `${currentPreferences[i].server_shortname}: ${currentPreferences[i].score}  \n`
                        }
                        const previousRatings = {
                            name: `Your current list`, 
                            value: embedValue
                        }

                        await interaction.reply({
                            embeds: [{
                                fields: [previousRatings, {
                                    name: `Done`, 
                                    value: `Your results have been saved! :saluting_face:`
                                }]
                            }],
                            ephemeral: true
                        })
                    } else {

                        const embeds = []

                        if (currentPreferences && currentPreferences.length !== 0) {
                            let embedValue = ''
                            for (let i = 0; i<nextServerIndex; i++) {
                                embedValue += `${currentPreferences[i].server_shortname}: ${currentPreferences[i].score}  \n`
                            }
                            const previousRatings = {
                                name: `Your current list`, 
                                value: embedValue
                            }

                            embeds.push(previousRatings)
                        }

                        embeds.push({
                            name: `Currently rating: ${servers[nextServerIndex].shortname} ${flagToEmoji(servers[nextServerIndex].country)}`, 
                            value: ` \n unreal://${servers[nextServerIndex].ip}:${servers[nextServerIndex].port}`})

                        const buttons = new ActionRowBuilder<ButtonBuilder>()
                        .addComponents(
                            new ButtonBuilder()
                                .setCustomId('servpref-' + servers[nextServerIndex].shortname + '_2')
                                .setLabel('+1')
                                .setStyle(ButtonStyle.Success),
                            new ButtonBuilder()
                                .setCustomId('servpref-' + servers[nextServerIndex].shortname + '_1')
                                .setLabel('0')
                                .setStyle(ButtonStyle.Secondary),
                            new ButtonBuilder()
                                .setCustomId('servpref-' + servers[nextServerIndex].shortname + '_0')
                                .setLabel('-1')
                                .setStyle(ButtonStyle.Danger))
                    
                        await interaction.reply({ 
                            embeds: [{
                                fields: embeds,
                            }],
                            components: [buttons],
                            ephemeral: true
                        })
                    }
            } catch (err) {
                console.log(err)
            }
        }
    }
}

export const generateServerPreferenceCommandHandler = (command: SlashCommandBuilder) => {
    return {
        ...command,
        handle: async (interaction: CommandInteraction) => {
            try {
                if (!interaction.isChatInputCommand()) return
    
                if (interaction.options.getSubcommand() === 'set') {
                    const servers = await getAllServers()

                    const buttons = new ActionRowBuilder<ButtonBuilder>()
                        .addComponents(
                            
                            new ButtonBuilder()
                                .setCustomId('servpref-' + servers[0].shortname + '_2')
                                .setLabel('+1')
                                .setStyle(ButtonStyle.Success),
                            new ButtonBuilder()
                                .setCustomId('servpref-' + servers[0].shortname + '_1')
                                .setLabel('0')
                                .setStyle(ButtonStyle.Secondary),
                            new ButtonBuilder()
                                .setCustomId('servpref-' + servers[0].shortname + '_0')
                                .setLabel('-1')
                                .setStyle(ButtonStyle.Danger))
                    

                    await interaction.reply({
                        embeds: [{
                            fields: [{
                                name: `Currently rating: ${servers[0].shortname} ${flagToEmoji(servers[0].country)}`, 
                                value: ` \n unreal://${servers[0].ip}:${servers[0].port}`}]}],
                        components: [buttons],
                        ephemeral: true
                    })
                }

    
                else if (interaction.options.getSubcommand() === 'get') {
                    const currentPreferences = await getPreferencesForPlayers(interaction.user.id)

                    if (currentPreferences.length === 0) {
                        await interaction.reply({
                            embeds: [{
                                fields: [{
                                    name: `No server preference set :pensive:`, 
                                    value: `Use /serverpreferences set to fix that!`
                                }]
                            }],
                            ephemeral: true
                        })

                        return
                    }

                    let embedValue = ''
                    for (let i = 0; i<currentPreferences.length; i++) {
                        embedValue += `${currentPreferences[i].server_shortname}: ${currentPreferences[i].score}  \n`
                    }
                    const previousRatings = {
                        name: `Your current list`, 
                        value: embedValue
                    }

                    await interaction.reply({
                        embeds: [{
                            fields: [previousRatings]
                        }],
                        ephemeral: true
                    })
                }
            } catch (err) {
                console.log(err)
            }
        }
    }
}

