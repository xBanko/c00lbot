import { GuildMember } from 'discord.js'

import { SlashCommandBuilder } from '@discordjs/builders'

import { getBanListEmbed } from '../../embeds/user/bans/ban_list_embed'
import { getPlayerBanEmbed } from '../../embeds/user/bans/player_ban_embed'
import { CommandHandler } from '../../commands'

export const banCommand = new SlashCommandBuilder()
    .setName('bans')
    .setDescription(`Shows information about bans`)

banCommand
    .addSubcommand(subcommand =>
        subcommand
            .setName('list')
            .setDescription('Shows the current list of banned people')
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('player')
            .setDescription('Checks ban stats for a player')
            .addMentionableOption(option => option.setName('player').setDescription('Player to check').setRequired(true))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

export const banCommandHandler: CommandHandler = {
    ...banCommand,
    handle: async (interaction) => {
        try {
            if (!interaction.isChatInputCommand()) return
            const hidden = interaction.options.getBoolean('hidden') ?? false

            if (interaction.options.getSubcommand() === 'list') {
                const listBanEmbed = await getBanListEmbed()
                await interaction.reply({ ephemeral: hidden, embeds: [listBanEmbed] })
            }

            else if (interaction.options.getSubcommand() === 'player') {
                const player: GuildMember = <GuildMember>interaction.options.getMentionable('player')

                const playerBanEmbed = await getPlayerBanEmbed(player.id)
                await interaction.reply({ ephemeral: hidden, embeds: [playerBanEmbed] })
            }
        } catch (err) {
            console.log(err)
        }
    }
}
