import { SlashCommandBuilder } from '@discordjs/builders'

import { getMapListEmbed } from '../../embeds/user/map/map_list_embed'
import { getNextMapEmbed } from '../../embeds/user/map/map_next_embed'
import { CommandHandler } from '../../commands'

export const mapCommand = new SlashCommandBuilder()
    .setName('map')
    .setDescription(`Shows the maps in the pug and the odds for the top 3 maps`)

mapCommand
    .addSubcommand(subcommand =>
        subcommand
            .setName('list')
            .setDescription('Shows the current map list')
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    // .addSubcommand(subcommand =>
    //     subcommand
    //         .setName('next')
    //         .setDescription('Checks which map is likely to be picked for next pug')
    //         .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))


export const mapCommandHandler: CommandHandler = {
    ...mapCommand,
    handle: async (interaction) => {
        try {
            if (!interaction.isChatInputCommand()) return
            const hidden = interaction.options.getBoolean('hidden') ?? false

            if (interaction.options.getSubcommand() === 'list') {
                const listMapEmbed = await getMapListEmbed()
                await interaction.reply({ ephemeral: hidden, embeds: [listMapEmbed] })
            }

            else if (interaction.options.getSubcommand() === 'next') {
                const nextMapEmbed = await getNextMapEmbed()
                await interaction.reply({ ephemeral: hidden, embeds: [nextMapEmbed] })
            }
        } catch (err) {
            console.log(err)
        }
    }
}
