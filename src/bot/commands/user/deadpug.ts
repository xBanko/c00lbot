import { CommandInteraction } from 'discord.js'

/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { SlashCommandBuilder } from '@discordjs/builders'

import { getDeadpugEmbed } from '../../../bot/embeds/user/deadpug_embed'
import { getModeById } from '../../../db/dao/mode'
import { getPlayersByDiscordId } from '../../../db/dao/player'
import { finishPug, getLivePugForUser } from '../../../db/dao/pug'
import { updateScoreStatus } from '../../../db/dao/score'
import { setServerFreeId } from '../../../db/dao/server'
import { BetDeadOption, resolveBets } from '../../../pug/bet/bet'
import { bot, sendLogMessage } from '../../bot'
import { CommandHandler } from '../../commands'
import { pugMode, ScoreStatus } from '../../embeds/utils/constants'
import { setPasswordsNotActiveForPug } from '../../../db/dao/password'
import { resolveBoostedDeadPug } from '../../../db/dao/pug_boosts'

export const deadPugCommand = new SlashCommandBuilder()
    .setName('deadpug')
    .setDescription(`Marks your current pug to be dead.`)

export const deadPugCommandHandler: CommandHandler = {
    ...deadPugCommand,
    handle: async (interaction: CommandInteraction) => {
        try {
            if (!interaction.isChatInputCommand()) return

            await interaction.deferReply()
            const pug = await getLivePugForUser(interaction.user.id)
            if (!pug) {
                await interaction.editReply(`Couldn't find a pug to proclaim dead, contact admins`)
                return
            }

            const isDead = bot.deadpugger.deadpug(pug.id, interaction.user.id, pug.team_blue.includes(interaction.user.id) ? 'BLUE' : 'RED')
            if (isDead.dead) {
                const modeName = (await getModeById(pugMode)).name
                await updateScoreStatus(pug.score_id, ScoreStatus.Rejected)
                await finishPug(pug.score_id, pug.id)
                await setServerFreeId(pug.server_id)
                await setPasswordsNotActiveForPug(parseInt(pug.id))

                await resolveBets(BetDeadOption.Dead, pug.id)
                await resolveBoostedDeadPug(parseInt(pug.id))

                const playerNicks = await getPlayersByDiscordId(isDead.voted!)
                const deadpugEmbed = await getDeadpugEmbed(modeName, pug.id, playerNicks)
                await interaction.editReply({ embeds: [deadpugEmbed] })
                await sendLogMessage(`@here **${modeName}** [#${pug.id}] has been declared dead\nStarted: ${pug.started}\nPlayers that voted for dead pug: ${playerNicks.map(p => `**${p.nickname}**`).join(`, `)}`)
            } else {
                await interaction.editReply(`Your deadpug vote is accepted.
                Votes required: :red_square: ${bot.deadpugger.deadpugVotes.get(pug.id + 'RED') ? bot.deadpugger.deadpugVotes.get(pug.id + 'RED') : `0`}/3 :blue_square: ${bot.deadpugger.deadpugVotes.get(pug.id + 'BLUE') ? bot.deadpugger.deadpugVotes.get(pug.id + 'BLUE') : `0`}/3`)
            }
        } catch (err) {
            console.log(err)
        }
    }
}
