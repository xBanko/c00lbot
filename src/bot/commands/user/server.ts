import { SlashCommandBuilder } from '@discordjs/builders'

import { getServerListEmbed } from '../../embeds/user/server_list_embed'
import { CommandHandler } from '../../commands'

export const serverCommand = new SlashCommandBuilder()
    .setName('server')
    .setDescription(`Shows the servers in the pug and the odds for the top 3 servers`)

serverCommand
    .addSubcommand(subcommand =>
        subcommand
            .setName('list')
            .setDescription('Lists all servers'))

export const serverCommandHandler: CommandHandler = {
    ...serverCommand,
    handle: async (interaction) => {
        try {
            if (!interaction.isChatInputCommand()) return
            if (interaction.options.getSubcommand() === 'list') {
                const listServerEmbed = await getServerListEmbed()
                await interaction.reply({ ephemeral: true, embeds: [listServerEmbed] })
            }
        } catch (err) {
            console.log(err)
        }
    }
}
