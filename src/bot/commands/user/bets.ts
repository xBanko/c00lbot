/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { GuildMember } from 'discord.js'

import { SlashCommandBuilder } from '@discordjs/builders'

import { disc_bet_role_id } from '../../../config'
import { getLastPugId } from '../../../db/dao/pug'
import { CommandHandler } from '../../commands'
import { getBetLeaderboardEmbed } from '../../embeds/user/bets/bet_leaderboard_embed'
import { getBetListEmbed } from '../../embeds/user/bets/bet_list_embed'
import { getPlayerBetEmbed } from '../../embeds/user/bets/player_bet_embed'
import { getWalletEmbed } from '../../embeds/user/bets/wallet_embed'

export const betsCommand = new SlashCommandBuilder()
    .setName('bets')
    .setDescription(`Everything about bets`)

betsCommand
    .addSubcommand(subcommand =>
        subcommand
            .setName('highlight')
            .setDescription('Gives you the role that will be highlighted')
            .addBooleanOption(option => option.setName('highlight').setDescription('Yes or No?')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('list')
            .setDescription('Check list of placed bets')
            .addIntegerOption(option => option.setName('pugid').setDescription('Pug ID defaults to last pug'))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('leaderboard')
            .setDescription('Leaderboard for bets')
            .addIntegerOption(option => option.setName('quantity').setDescription('Limit').setRequired(true).addChoices(
                { name: '1-10', value: 0 },
                { name: '11-20', value: 10 },
                { name: '21-30', value: 20 },
                { name: '31-40', value: 30 },
                { name: '41-50', value: 40 },
                { name: '51-60', value: 50 },
                { name: '61-70', value: 60 },
                { name: '71-80', value: 70 },
                { name: '81-90', value: 80 },
                { name: '91-100', value: 90 }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('player')
            .setDescription('Checks bet stats for a player')
            .addMentionableOption(option => option.setName('player').setDescription('Player to check'))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('wallet')
            .setDescription('Check how many coins you currently have'))

export const betsCommandHandler: CommandHandler = {
    ...betsCommand,
    handle: async (interaction) => {
        try {
            if (!interaction.isChatInputCommand()) return
            const hidden = interaction.options.getBoolean('hidden') ?? false

            if (interaction.options.getSubcommand() === 'highlight') {
                const player: GuildMember = <GuildMember>interaction.member
                const highlight = interaction.options.getBoolean('highlight')
                if (highlight) {
                    await player.roles.add(disc_bet_role_id)
                    await interaction.reply({ ephemeral: true, content: `Role added, you will now be highlighted when a pug starts` })
                } else {
                    await player.roles.remove(disc_bet_role_id)
                    await interaction.reply({ ephemeral: true, content: `Role removed, you will no longer be highlighted` })
                }
            }

            else if (interaction.options.getSubcommand() === 'list') {
                const pugId = interaction.options.getInteger('pugid') ?? (await getLastPugId()).id
                const betListEmbed = await getBetListEmbed(pugId)
                await interaction.reply({ ephemeral: hidden, embeds: [betListEmbed] })
            }

            else if (interaction.options.getSubcommand() === 'leaderboard') {
                const betLeaderboardEmbed = await getBetLeaderboardEmbed(interaction.options.getInteger('quantity')!)
                await interaction.reply({ ephemeral: hidden, embeds: [betLeaderboardEmbed] })
            }

            else if (interaction.options.getSubcommand() === 'player') {
                const player: GuildMember = <GuildMember>interaction.options.getMentionable('player')

                const playerBetEmbed = await getPlayerBetEmbed(player ? player.id : interaction.user.id)
                await interaction.reply({ ephemeral: hidden, embeds: [playerBetEmbed] })
            }

            else if (interaction.options.getSubcommand() === 'wallet') {
                const player: GuildMember = <GuildMember><GuildMember>interaction.member
                const walletEmbed = await getWalletEmbed(player.id)
                await interaction.reply({ ephemeral: true, embeds: [walletEmbed] })
            }
        } catch (err) {
            console.log(err)
        }
    }
}
