/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { SlashCommandBuilder } from '@discordjs/builders'
import { ActionRowBuilder, ButtonBuilder, ButtonInteraction, ButtonStyle, CommandInteraction } from 'discord.js'
import { getPreferencesForPlayers } from '../../../db/dao/server_preference'

import { getAllEnabledMaps, getMap } from '../../../db/dao/map'
import { sendMessageToUser } from '../../../bot/bot'
import { getAllMapPreferencesForPlayers, insertMapPreference } from '../../../db/dao/map_preference'


export const generateMapPreferenceCommand = async () => {
    const mapPreferenceCommand = new SlashCommandBuilder()
    .setName('mappreferences')
    .setDescription(`Allows you to set maps you'd mostly like to play, affecting the picking`)

    mapPreferenceCommand.addSubcommand(subcommand =>
        subcommand
            .setName('set')
            .setDescription('Sets your map preference')

            )
    .addSubcommand(subcommand =>
        subcommand
            .setName('get')
            .setDescription('Gets your map preference'))

    return mapPreferenceCommand
}

export const generateMapPreferenceButtonHandler = (command: SlashCommandBuilder) => {
    return {
        ...command,
        name: 'mappref',
        handle: async (interaction: ButtonInteraction) => {
            try {
                if (!interaction.isButton()) return
    
                    const mapId = interaction.customId.slice(8, -2)
                    const value = parseInt(interaction.customId.slice(-1))
                    const map = await getMap(mapId)

                    switch (value) {
                        case 3:
                            await insertMapPreference(interaction.user.id, map.name,  map.id, 10)
                            break

                        case 2:
                            await insertMapPreference(interaction.user.id, map.name,  map.id, 5)
                            break

                        case 1:
                            await insertMapPreference(interaction.user.id, map.name,  map.id, 0)
                            break

                        case 0:
                            await insertMapPreference(interaction.user.id, map.name,  map.id, -10)
                            break
                    }


                    const maps = await getAllEnabledMaps()

                    const mapNames = maps.map(m => m.name)
                    const nextMapIndex = mapNames.indexOf(map.name) + 1
                    const currentPreferences = await getAllMapPreferencesForPlayers(interaction.user.id)

                    if (nextMapIndex + 1 > maps.length) {
                        let embedValue = ''
                        for (let i = 0; i<nextMapIndex; i++) {
                            embedValue += `${currentPreferences[i].map_name}: ${getEmojiForValue(currentPreferences[i].score)}  \n`
                        }
                        const previousRatings = {
                            name: `Your current list`, 
                            value: embedValue
                        }

                        await interaction.reply({
                            embeds: [{
                                fields: [previousRatings, {
                                    name: `Done`, 
                                    value: `Your results have been saved! :saluting_face:`
                                }]
                            }],
                            ephemeral: true
                        })
                    } else {

                        const embeds = []

                        if (currentPreferences && currentPreferences.length !== 0) {
                            let embedValue = ''
                            for (let i = 0; i<nextMapIndex; i++) {
                                embedValue += `${currentPreferences[i].map_name}: ${getEmojiForValue(currentPreferences[i].score)}  \n`
                            }
                            const previousRatings = {
                                name: `Your current list`, 
                                value: embedValue
                            }

                            embeds.push(previousRatings)
                        }

                        embeds.push({
                            name: `Currently setting preference for: ${maps[nextMapIndex].name}`, 
                            value: ` \n How often would you like to play this map?`})

                        const buttons = new ActionRowBuilder<ButtonBuilder>()
                            .addComponents(
                                new ButtonBuilder()
                                    .setCustomId('mappref-' + maps[nextMapIndex].id + '_3')
                                    .setLabel('Constantly')
                                    .setStyle(ButtonStyle.Success),
                                new ButtonBuilder()
                                    .setCustomId('mappref-' + maps[nextMapIndex].id + '_2')
                                    .setLabel('Often')
                                    .setStyle(ButtonStyle.Primary),
                                new ButtonBuilder()
                                    .setCustomId('mappref-' + maps[nextMapIndex].id + '_1')
                                    .setLabel('Sometimes')
                                    .setStyle(ButtonStyle.Secondary),
                                new ButtonBuilder()
                                    .setCustomId('mappref-' + maps[nextMapIndex].id + '_0')
                                    .setLabel('As little as possible')
                                    .setStyle(ButtonStyle.Danger))
                    
                        await interaction.reply({ 
                            embeds: [{
                                fields: embeds,
                            }],
                            components: [buttons],
                            ephemeral: true
                        })
                    }
            } catch (err) {
                console.log(err)
            }
        }
    }
}

export const generateMapPreferenceCommandHandler = (command: SlashCommandBuilder) => {
    return {
        ...command,
        handle: async (interaction: CommandInteraction) => {
            try {

                if (!interaction.isChatInputCommand()) return
    
                if (interaction.options.getSubcommand() === 'set') {
                    const maps = await getAllEnabledMaps()

                    const buttons = new ActionRowBuilder<ButtonBuilder>()
                        .addComponents(
                            new ButtonBuilder()
                                .setCustomId('mappref-' + maps[0].id + '_3')
                                .setLabel('Constantly')
                                .setStyle(ButtonStyle.Success),
                            new ButtonBuilder()
                                .setCustomId('mappref-' + maps[0].id + '_2')
                                .setLabel('Often')
                                .setStyle(ButtonStyle.Primary),
                            new ButtonBuilder()
                                .setCustomId('mappref-' + maps[0].id + '_1')
                                .setLabel('Sometimes')
                                .setStyle(ButtonStyle.Secondary),
                            new ButtonBuilder()
                                .setCustomId('mappref-' + maps[0].id + '_0')
                                .setLabel('As little as possible')
                                .setStyle(ButtonStyle.Danger))
                    
                    await sendMessageToUser({
                        embeds: [{
                            fields: [{
                                name: `Currently setting preference for: ${maps[0].name}`, 
                                value: ` \n How often would you like to play this map?`}]}],
                        components: [buttons],
                    }, interaction.user.id)

                    await interaction.reply({
                        embeds: [{
                            fields: [{
                                name: `Check private message`, 
                                value: `I just slid in your DMs :face_with_peeking_eye: `}]}],
                        ephemeral: true
                    })
                }

    
                else if (interaction.options.getSubcommand() === 'get') {
                    const currentPreferences = await getAllMapPreferencesForPlayers(interaction.user.id)

                    if (currentPreferences.length === 0) {
                        await interaction.reply({
                            embeds: [{
                                fields: [{
                                    name: `No map preference set :pensive:`, 
                                    value: `Use /mapPreferences set to fix that!`
                                }]
                            }],
                            ephemeral: true
                        })

                        return
                    }

                    let embedValue = ''
                    for (let i = 0; i<currentPreferences.length; i++) {
                        embedValue += `${currentPreferences[i].map_name}: ${getEmojiForValue(currentPreferences[i].score)}  \n`
                    }
                    const previousRatings = {
                        name: `Your current list`, 
                        value: embedValue
                    }

                    await interaction.reply({
                        embeds: [{
                            fields: [previousRatings]
                        }],
                        ephemeral: true
                    })
                }
            } catch (err) {
                console.log(err)
            }
        }
    }
}

const getEmojiForValue = (value: number) => {
    if (value === 10) return ':heart:'
    if (value === 5) return  ':relieved:'
    if (value === 0) return ':face_with_diagonal_mouth:'
    return ':angry:'
}