/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { SlashCommandBuilder } from '@discordjs/builders'

import { getListEmbed } from '../../embeds/user/liast/list_embed'
import { CommandHandler } from '../../commands'

export const listCommand = new SlashCommandBuilder()
    .setName('list')
    .setDescription('Lists current users in the pug')

export const listCommandHandler: CommandHandler = {
    ...listCommand,
    handle: async (interaction) => {
        try {
            const listEmbed = await getListEmbed()
            await interaction.reply({ embeds: [listEmbed] })
        } catch (err) {
            console.log(err)
        }
    }
}
