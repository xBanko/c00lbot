/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { SlashCommandBuilder } from '@discordjs/builders'

import { getLiastEmbed } from '../../embeds/user/liast/liast_embed'
import { CommandHandler } from '../../commands'

export const liastCommand = new SlashCommandBuilder()
    .setName('liast')
    .setDescription('Lists current users in the pug and the last played pug')

export const liastCommandHandler: CommandHandler = {
    ...liastCommand,
    handle: async (interaction) => {
        try {
            const liastEmbed = await getLiastEmbed()
            await interaction.reply({ embeds: [liastEmbed] })
        } catch (err) {
            console.log(err)
        }
    }
}
