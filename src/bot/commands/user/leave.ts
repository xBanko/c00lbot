/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { SlashCommandBuilder } from '@discordjs/builders'

import { checkIfPlayerRegistered } from '../../../db/dao/player'
import { getCurrentPug, updatePugPlayers } from '../../../db/dao/pug'
import { getLeaveEmbed } from '../../embeds/user/leave_embed'
import { pugMode } from '../../embeds/utils/constants'
import { CommandHandler } from '../../commands'

export const leaveCommand = new SlashCommandBuilder()
    .setName('leave')
    .setDescription('Lets you leave the pug')

export const leaveCommandHandler: CommandHandler = {
    ...leaveCommand,
    handle: async (interaction) => {
        try {
            const userId = interaction.user.id
            if (!await checkIfPlayerRegistered(userId)) {
                interaction.reply('not registered')
                return
            }

            const livePug = await getCurrentPug(pugMode)

            if (!livePug.players.includes(userId)) {
                interaction.reply('not in the pug??')
                return
            }

            const newPlayers = livePug.players.filter(p => p !== userId)
            await updatePugPlayers(newPlayers.join(','), livePug.id!)
            const leaveEmbed = await getLeaveEmbed(userId)
            await interaction.reply({ embeds: [leaveEmbed] })
        } catch (err) {
            console.log(err)
        }
    }
}
