export const flagToEmoji = (flagString: string) => {
    let flag = ''
    if ((flagString) === 'DE') flag = ':flag_de:'
    else if ((flagString) === 'FR') flag = ':flag_fr:'
    else if ((flagString) === 'UK') flag = ':flag_uk:'
    else if ((flagString) === 'NL') flag = ':flag_nl:'

    return flag
}