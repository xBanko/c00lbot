//TODO: pugMode should be used as an input to specify what pug they want to send commands to/for or remove the const.
export const pugMode = 2
export const redDot = '<:reddot:910876690312462376>'
export const blueDot = '<:bluedot:910876674579648532>'
export const greenuparrow = '<:greenuparrow:1035692320080810065>'
export const orangebox = '<:orangebox:1035692321657868348>'
export const reddownarrow = '<:reddownarrow:1035692323864051814>'
export enum ScoreStatus {
    Pending = 'PENDING',
    Live = 'LIVE',
    Manual = 'MANUAL',
    Concluded = 'CONCLUDED',
    Accepted = 'ACCEPTED',
    Rejected = 'REJECTED',
}
export enum MapStatus {
    Enabled = 'ENABLED',
    Disabled = 'DISABLED',
}
export enum SeasonChoice {
    Current = 'CURRENT',
    All = 'ALL',
}
