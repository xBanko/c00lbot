/* eslint-disable no-case-declarations */
import { SeasonChoice } from "./constants"

export const getStringDate = (time: SeasonChoice | string) => {
    const today = new Date()
    switch (time) {
        case 'today':
            return today.toLocaleDateString('sv-SE')
        case 'yesterday':
            today.setDate(today.getDate() - 1)
            return today.toLocaleDateString('sv-SE')
        case 'three':
            today.setDate(today.getDate() - 3)
            return today.toLocaleDateString('sv-SE')
        case 'week':
            const dayOfWeek = today.getDay()
            today.setDate(today.getDate() - dayOfWeek + (dayOfWeek === 0 ? -6 : 1))
            return today.toLocaleDateString('sv-SE')
        case 'month':
            today.setMonth(today.getMonth())
            today.setDate(1)
            return today.toLocaleDateString('sv-SE')
        case 'year':
            today.setFullYear(today.getFullYear())
            today.setMonth(0)
            today.setDate(1)
            return today.toLocaleDateString('sv-SE')
        default:
            throw new Error('Invalid time parameter')
    }
}

export default getStringDate
