import { ColorResolvable, Colors } from 'discord.js'

import { blueDot, redDot } from './constants'

export function getBoldScore(pug: { score_red?: number | null, score_blue?: number | null }): string {
    if (pug.score_red != null && pug.score_blue != null && pug.score_red > pug.score_blue) {
        return `${redDot} **${pug.score_red}** - ${pug.score_blue} ${blueDot}`
    } else if (pug.score_red != null && pug.score_blue != null && pug.score_red < pug.score_blue) {
        return `${redDot} ${pug.score_red} - **${pug.score_blue}** ${blueDot}`
    }
    return `${redDot} ${pug.score_red ?? '0'} - ${pug.score_blue ?? '0'} ${blueDot}`
}


export function getScoreColor(redScore: number, blueScore: number): ColorResolvable {
    if (redScore > blueScore) {
        return Colors.DarkRed
    } else if (redScore < blueScore) {
        return Colors.DarkBlue
    }
    return Colors.DarkGrey
}
