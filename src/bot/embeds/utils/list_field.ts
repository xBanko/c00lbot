import { isPugDoubleElo } from '../../../db/dao/pug_boosts'
import { getModeById } from '../../../db/dao/mode'
import { getPlayersByDiscordId } from '../../../db/dao/player'
import { LivePug } from '../../../db/dao/pug'
import { getPlayersInPug } from './players_in_pug'

export async function getListField(livePug: LivePug): Promise<{ name: string, value: string }> {
    try {
        const mode = await getModeById(livePug.mode_id)
        const players = await getPlayersByDiscordId(livePug.players)
        const doubleElo = await isPugDoubleElo(parseInt(livePug.id!))

        const listField: { name: string, value: string } = {
            name: `${mode.name} [#${livePug.id}] (${livePug.players.length}/${mode.team_size * 2}) ${doubleElo ? '✨' : ''}`,
            value: `${getPlayersInPug(players)}`
        }

        return listField
    } catch (err) {
        console.log(err)
        throw err
    }
}
