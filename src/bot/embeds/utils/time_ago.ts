import moment from 'moment'

export const timeAgo = (input: Date | string | number) => {
    let date
    if (typeof input === 'number') {
        const currentDate = moment()
        if (input > 0) {
            date = currentDate.add(input, 'seconds')
        } else {
            date = currentDate.subtract(Math.abs(input), 'seconds')
        }
    } else if (typeof input === 'string') {
        const stringDate = moment(input)
        date = stringDate.format()
        date = moment.utc(date)
    }
    else {
        date = moment.utc(input)
    }
    if (!date.isValid()) {
        return 'Invalid date'
    }

    return date.fromNow()
}
