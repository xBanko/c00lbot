import { Player } from '../../../db/dao/player'
import { format } from './format_rating'

export function getPlayersInPug(players: Player[]): string {
    let playersInPug: string
    if (!players.length) {
        playersInPug = 'No one joined yet'
    } else {
        players.sort((a, b) => Number(b.rating) - Number(a.rating))
        const playerNames = players.map((p: Player) => `:small_orange_diamond: **${p.nickname}** (${format(p.rating)})`)

        playersInPug = playerNames.join(' ')
    }
    return playersInPug
}
