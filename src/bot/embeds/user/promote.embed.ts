/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'
import { getServerOfPug } from '../../../db/dao/server'

import { getLastPug, LivePug } from '../../../db/dao/pug'
import { pugMode } from '../utils/constants'
import { timeAgo } from '../utils/time_ago'
import { Mode } from '../../../db/dao/mode'

export async function getPromoteEmbed(livePug: LivePug, mode: Mode) {
    const lastPug = await getLastPug(pugMode)

    const playersNeeded = (mode.team_size * 2) - livePug.players.length

    const promoteEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `Only ${playersNeeded} needed for **Ranked ${mode.name}** [#${livePug.id}]`,
                value: `Type /join to have some fun on the bun! :peach:`
            },
        )
        .setColor('Random')

    if (lastPug) {
        const serverName = await getServerOfPug(lastPug.id)

        promoteEmbed.setFooter({ text: `Last pug was ${timeAgo(lastPug.started)} on ${serverName.shortname}` })
    }

    return promoteEmbed
}
