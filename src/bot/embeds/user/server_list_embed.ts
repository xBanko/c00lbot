/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getAllServers } from '../../../db/dao/server'

export async function getServerListEmbed() {
    const servers = await getAllServers()

    const serverListEmbed = new EmbedBuilder()
        .setColor('Random')

    if (servers.length === 0) {
        return serverListEmbed.addFields(
            {
                name: `No Servers`,
                value: `Not a single server added yet?`,
            }
        )
    }

    let shortnameValue = ''
    let ipValue = ''
    let statusValue = ''

    for (const s of servers) {
        shortnameValue += '[' + s.country + '] ' + s.shortname + '\n'
        ipValue += s.ip + ':' + s.port + '\n'
        statusValue += s.enabled === 1 ? ' \u200B \u200B :white_check_mark:' + '\n' : ' \u200B \u200B :x:' + '\n'
    }

    serverListEmbed.addFields(
        {
            name: `Shortname`,
            value: shortnameValue,
            inline: true,
        },
    )

    serverListEmbed.addFields(
        {
            name: `IP`,
            value: ipValue,
            inline: true,
        },
    )

    serverListEmbed.addFields(
        {
            name: `Status`,
            value: statusValue,
            inline: true,
        },
    )

    return serverListEmbed
}
