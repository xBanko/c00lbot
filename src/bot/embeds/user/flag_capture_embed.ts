/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Colors, EmbedBuilder } from 'discord.js'
import { blueDot, redDot } from '../utils/constants'

export async function getFlagCaptureEmbed(userNick: string, userTeam: 'RED' | 'BLUE', scoreRed: number, scoreBlue: number, remainingTime: number, map: string) {
    const minutes = `${Math.floor(remainingTime / 60)}`
    const seconds = `${remainingTime % 60}`

    const flagCaptureEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `${redDot} ${scoreRed} - ${scoreBlue} ${blueDot}  [${minutes.length === 1 ? `0${minutes}` : `${minutes}`}:${seconds.length === 1 ? `0${seconds}` : `${seconds}`}]`,
                value: `**${userNick}** captured the flag for team ${userTeam} (${map})`
            },
        )
        .setColor(userTeam === 'RED' ? Colors.Red : Colors.Blue)

    return flagCaptureEmbed
}
