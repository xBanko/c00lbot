/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

export async function getEloBoostEmbed(amount: number, current: number, playerName: string) {

    const deadpugEmbed = new EmbedBuilder()
        .addFields(
            { value: `**${playerName}** boosted the next pug with **${amount}** :pinching_hand: current status: *${current}/10000*`, name: 'Elo boost'}
        )
        .setColor('Random')
    return deadpugEmbed
}


export async function getEloBoostFullEmbed(amount: number, current: number, playerName: string) {

    const deadpugEmbed = new EmbedBuilder()
        .addFields(
            { value: `:rosette:  **${playerName}** boosted the next pug with **${amount}**, current status: *BOOSTED* :rosette: `, name: 'Elo boost'}
        )
        .setColor('Random')
    return deadpugEmbed
}
