import { EmbedBuilder } from 'discord.js'

import { getLiveServers } from '../../../db/dao/pug'

export async function getSpectateEmbed() {
    const servers = await getLiveServers()
    const value = servers.length > 0
        ? servers.map(s => `**[#${s.pug_id}]** <unreal://${s.ip}:${s.port}?password=${s.password.split(';').slice(-1).pop()}>`).join(`\n`)
        : `Wow! Not a single pug going on right now! Use /join to get it started!`
    const name = servers.length > 0
        ? `${servers.length} Live Pugs`
        : `No Live Pugs`

    const spectateEmbed = new EmbedBuilder()
        .addFields(
            {
                name,
                value,
            },
        )
        .setColor('Random')
    return spectateEmbed
}
