/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getMap } from '../../../db/dao/map'
import { getModeById } from '../../../db/dao/mode'
import { getPlayersByDiscordId, Player } from '../../../db/dao/player'
import { LivePugFinished } from '../../../db/dao/pug'
import { blueDot, redDot } from '../utils/constants'
import { getScoreColor } from '../utils/highlight_score'

export async function getPugConcludedEmbed(pug: LivePugFinished): Promise<EmbedBuilder> {
    const gameMode = await getModeById(pug.mode_id)
    const map = await getMap(pug.map_id!)
    const team1 = await getPlayersByDiscordId(pug.team_red)
    const team2 = await getPlayersByDiscordId(pug.team_blue)

    team1.sort((a, b) => Number(b.rating) - Number(a.rating))
    team2.sort((a, b) => Number(b.rating) - Number(a.rating))

    const team1playerNames = team1.map((p: Player) => p.nickname)
    const team2playerNames = team2.map((p: Player) => p.nickname)


    const redRating = pug.rating_change_red! > 0 ? `+${pug.rating_change_red}` : `${pug.rating_change_red}`
    const blueRating = pug.rating_change_blue! > 0 ? `+${pug.rating_change_blue}` : `${pug.rating_change_blue}`
    const pugConcludedEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `${gameMode.name} [#${pug.id}] on ${map.name} Concluded`,
                value: `${redDot}**${pug.score_red}** [${redRating}] :white_small_square: **${team1playerNames.join(' :white_small_square: ')}**\n${blueDot}**${pug.score_blue}** [${blueRating}] :white_small_square: **${team2playerNames.join(' :white_small_square: ')}**`
            },
        )
        .setFooter({ text: `${pug.finished}` })
        .setColor(getScoreColor(pug.score_red!, pug.score_blue!))

    return pugConcludedEmbed
}
