/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getPlayerLastNet, getTopBetPlayers } from '../../../../db/dao/bet'
import { greenuparrow, orangebox, reddownarrow } from '../../utils/constants'
import { getSuffixOf } from '../../utils/number_suffix'

export async function getBetLeaderboardEmbed(limit: number) {
    const players = await getTopBetPlayers(limit)
    const startNo = limit + 1

    const embed = {
        name: `Betting Leaderboard ${startNo} - ${startNo + 9}`,
        value: ''
    }
    let i = startNo

    if (players.length > 0) {
        await Promise.all(players.map(async p => {
            const lastNet = (await getPlayerLastNet(p.player_id)).net
            let trend
            if (!lastNet || lastNet === 0) trend = orangebox
            else if (lastNet > 0) trend = greenuparrow
            else trend = reddownarrow

            embed.value += `${trend} ${getSuffixOf(i)} **${p.nickname}** - ${p.coins}\n`
            i++
        }))
    } else {
        embed.value += `There is nobody this far down yet!`
    }

    const betLeaderboardEmbed = new EmbedBuilder()
        .addFields(
            embed
        )
        .setColor("Random")

    return betLeaderboardEmbed
}
