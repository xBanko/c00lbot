/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { ResolvedBet } from '../../../../pug/bet/bet'
import { getBetColor } from '../../utils/bet_color'
import { greenuparrow, reddownarrow } from '../../utils/constants'

export function getBetResultEmbed(pugId: number, bets: ResolvedBet[]): EmbedBuilder | null {
    const betListEmbed = new EmbedBuilder().setColor("Random")
    bets.sort((a, b) => Number(b.net) - Number(a.net))

    let embedNumber = 1
    embedNumber = Math.floor(bets.length / 10) + 1
    if (embedNumber > 10) embedNumber = 10

    const part = Math.round(bets.length / embedNumber)

    for (let i = 0; i < embedNumber; i++) {
        const embed = {
            name: i === 0 ? `Bet result [#${pugId}]` : '\u200B',
            value: '\n'
        }

        bets.slice(i * part, (i + 1) * part).map(bet => {
            const color = getBetColor(bet.betOn)

            embed.value += `${bet.net > 0 ? `${greenuparrow}` : `${reddownarrow}`} **${bet.name}** [${bet.balance}]${bet.net > 0 ? '' + color + '+' : '' + color}${bet.net} coins\n`
        })
        betListEmbed.addFields(embed)
    }

    return betListEmbed
}
