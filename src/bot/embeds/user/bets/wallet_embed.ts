/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getPlayerBetInfo } from '../../../../db/dao/bet'

export async function getWalletEmbed(discordId: string) {
    const playerBetInfo = await getPlayerBetInfo(discordId)
    let value = ''

    if (playerBetInfo.coins > 0) {
        value = `${playerBetInfo.coins} coins`
    } else {
        value = 'Fuck me its empty. Play a pug to get 25 coins!'
    }

    const walletEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `Wallet`,
                value,
            },
        )
        .setColor('Random')
    return walletEmbed
}
