/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getBetsForPug } from '../../../../db/dao/bet'
import { getModeById } from '../../../../db/dao/mode'
import { getBetColor } from '../../utils/bet_color'
import { pugMode } from '../../utils/constants'

export async function getBetListEmbed(pugId: number) {
    const betsForPugId = await getBetsForPug(pugId)
    const modeName = (await getModeById(pugMode)).name

    const betListEmbed = new EmbedBuilder()
        .setColor('Random')

    if (betsForPugId.length === 0) {
        const modeName = (await getModeById(pugMode)).name

        return betListEmbed.addFields(
            {
                name: `Bet List :money_with_wings:`,
                value: `Not a single bet made for **${modeName}** [#${pugId}] :orangutan:`,
            }
        )
    }

    betsForPugId.sort((a, b) => Number(b.amount) - Number(a.amount))


    let nicknameValue = ''
    let betValue = ''
    let netValue = ''
    let totalBet = 0
    let totalNet: number | null = null

    for (const b of betsForPugId) {
        const color = getBetColor(b.bet_on)

        nicknameValue += b.nickname + '\n'
        betValue += color! + b.amount + '\n'
        netValue += b.net != null ? b.net + '\n' : ''
        totalBet += b.amount
        totalNet! += b.net
    }

    betListEmbed.addFields(
        {
            name: `${betsForPugId.length} Bets For ${modeName} [#${pugId}]`,
            value: `Total **${totalBet}** :white_small_square: Average **${(totalBet / betsForPugId.length).toFixed(0)}** ${totalNet !== null ? ':white_small_square: Total net **' + totalNet + '**' : ''}`
        },
    )

    betListEmbed.addFields(
        {
            name: `Nick`,
            value: nicknameValue,
            inline: true,
        },
    )

    betListEmbed.addFields(
        {
            name: ` \u200B Bet`,
            value: betValue,
            inline: true,
        },
    )

    if (netValue !== '') {
        betListEmbed.addFields(
            {
                name: `Net`,
                value: netValue,
                inline: true,
            },
        )
    }

    return betListEmbed
}
