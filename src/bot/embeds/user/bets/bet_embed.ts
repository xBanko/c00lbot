/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getModeById } from '../../../../db/dao/mode'
import { BetOption } from '../../../../pug/bet/bet'
import { getBetColor } from '../../utils/bet_color'
import { pugMode } from '../../utils/constants'

export async function getBetEmbed(playerName: string, amount: number | undefined, option: BetOption, pugID: number) {
    const modeName = (await getModeById(pugMode)).name
    const color = getBetColor(option)

    const betEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `${playerName} Bet`,
                value: `${amount} coins on${color}for **${modeName}** [#${pugID}]`
            },
        )
        .setColor('Random')
    return betEmbed
}
