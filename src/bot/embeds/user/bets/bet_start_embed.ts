/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { BetOdds } from '../../../../pug/bet/bet'
import { blueDot, orangebox, redDot } from '../../utils/constants'

export async function getBetStartEmbed(odds: BetOdds, time: string) {

    const betStartEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `Bets Open Till __${time}__`,
                value: `**Odds** ${redDot}${odds.red.toFixed(2)} : ${odds.blue.toFixed(2)}${blueDot} ${orangebox}${odds.draw.toFixed(2)}`
            },
        )
        .setColor('Random')
    return betStartEmbed
}
