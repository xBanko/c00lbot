/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js';

import { getPlayerBetInfo, getPlayerBetRank, getPlayerBetStats, getPlayerLastBet } from '../../../../db/dao/bet'
import { getModeById } from '../../../../db/dao/mode'
import { getBetColor } from '../../utils/bet_color';
import { blueDot, orangebox, pugMode, redDot } from '../../utils/constants'
import { getSuffixOf } from '../../utils/number_suffix'
import { timeAgo } from '../../utils/time_ago'

export async function getPlayerBetEmbed(playerId: string) {
    const playerBets = await getPlayerBetStats(playerId)
    const lastBet = await getPlayerLastBet(playerId)
    const modeName = (await getModeById(pugMode)).name
    const rank = await getPlayerBetRank(playerId)
    const suffix = getSuffixOf(rank.rank)
    const betInfo = await getPlayerBetInfo(playerId)

    let value = `Nothing here! Why don't you bet you have ${betInfo.coins} coins?? :chicken:`

    if (playerBets.total > 0) {
        const color = getBetColor(lastBet.bet_on)
        const redPart = (playerBets.red_count / playerBets.total * 100).toFixed(2)
        const bluePart = (playerBets.blue_count / playerBets.total * 100).toFixed(2)
        const drawPart = (playerBets.draw_count / playerBets.total * 100).toFixed(2)
        const precision = (playerBets.win_bets / playerBets.total * 100).toFixed(2)

        value = `[**${suffix}**] ${betInfo.coins} current coins. Gain from bets: ${playerBets.sum_net}
        **${playerBets.total}** bets for a total of ${playerBets.amount_total} coins (${precision}% accuracy)\n
        **Averages** :white_small_square: Amount *${playerBets.avg_amount}* :white_small_square: Gain *${playerBets.avg_net}* :white_small_square:*
        **Betting habits** :white_small_square: ${redDot}${redPart}% ${blueDot}${bluePart}% ${orangebox}${drawPart}%
        **Last bet** :white_small_square: ${modeName} [#${lastBet.pug_id}] ${lastBet.net !== null ? lastBet.net : lastBet.amount} coins${color}(${timeAgo(lastBet.created)})`
    }

    const playerBetEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `Bets ${betInfo.nickname}`,
                value: value
            },
        )
        .setColor('Random')
    return playerBetEmbed
}
