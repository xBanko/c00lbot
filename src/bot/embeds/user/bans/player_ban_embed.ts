/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getAllBansForUser, getLastBanForUser } from '../../../../db/dao/ban'
import { getPlayerName } from '../../../../db/dao/player'
import { timeAgo } from '../../utils/time_ago'

export async function getPlayerBanEmbed(playerId: string) {
    const allBans = await getAllBansForUser(playerId)
    const lastBan = await getLastBanForUser(playerId)
    const nickname = (await getPlayerName(playerId)).nickname
    let totalDuration = 0

    let value = `Wow! No bans?? Good boy. :clap:`

    if (allBans.length > 0) {
        for (let i = 0; i < allBans.length; i++) {
            totalDuration += allBans[i].duration
        }
        value = `${nickname} has been banned ${allBans.length} times and spent a total of ${totalDuration} hours banned so far!\nLast ban: ${timeAgo(lastBan.end_time)}. Duration: ${lastBan.duration}. Reason: *${lastBan.reason}*`
    }

    const playerBanEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `${nickname} Bans`,
                value: value
            },
        )
        .setColor('Random')
    return playerBanEmbed
}
