/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getAllCurrentBans } from '../../../../db/dao/ban'
import { timeAgo } from '../../utils/time_ago'

export async function getBanListEmbed() {
    const currentBans = await getAllCurrentBans()
    let value = `Good community, **no one** is currently banned! :face_holding_back_tears:`

    if (currentBans.length > 0) {
        for (let i = 0; i < currentBans.length; i++) {
            value = `${currentBans.map(cb => `**${cb.nickname}** banned by ${cb.admin_name} for ${cb.duration} hours with the reason: *${cb.reason}* (${timeAgo(cb.end_time)})`).join(`\n`)}`
        }
    }

    const banListEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `Ban List :hammer:`,
                value,
            },
        )
        .setColor('Random')
    return banListEmbed
}
