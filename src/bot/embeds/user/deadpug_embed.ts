/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { Player } from '../../../db/dao/player'

export async function getDeadpugEmbed(modeName: string, pugId: string, players: Player[]) {

    const deadpugEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `**${modeName}** [#${pugId}] has died! :headstone:`,
                value: `Players that voted for dead pug\n${players.map(p => `**${p.nickname}**`).join(` :white_small_square: `)}`
            },
        )
        .setColor('DarkerGrey')
    return deadpugEmbed
}
