/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getModeById } from '../../../db/dao/mode'
import { getPlayerByDiscordId } from '../../../db/dao/player'
import { getCurrentPug } from '../../../db/dao/pug'
import { pugMode } from '../utils/constants'

export async function getLeaveEmbed(userId: string) {
    const livePug = await getCurrentPug(pugMode)
    const gameMode = await getModeById(livePug.mode_id)
    const user = await getPlayerByDiscordId(userId)

    const leaveEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `${gameMode.name} [${livePug.players.length}/${gameMode.team_size * 2}]`,
                value: `${user.nickname} left the pug`
            },
        )
        .setColor('Random')
    return leaveEmbed
}
