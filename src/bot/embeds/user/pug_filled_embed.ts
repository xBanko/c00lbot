import { EmbedBuilder } from 'discord.js'

import { UtMap } from '../../../db/dao/map'
import { getModeById } from '../../../db/dao/mode'
import { SuggestedTeams } from '../../../db/dao/player'
import { LivePug } from '../../../db/dao/pug'
import { blueDot, redDot } from '../utils/constants'
import { format } from '../utils/format_rating'

export async function getPugFilledEmbed(pug: LivePug, teams: SuggestedTeams, map: UtMap) {
    const mode = await getModeById(pug.mode_id)

    teams.red.players = teams.red.players.sort((a, b) => Number(b.rating) - Number(a.rating))
    teams.blue.players = teams.blue.players.sort((a, b) => Number(b.rating) - Number(a.rating))

    const redOdds = +(Math.round(teams.red.rating / (teams.red.rating + teams.blue.rating) * 1000) / 10).toFixed(1)
    const blueOdds = 100 - redOdds

    const pugFilledEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `${mode.name} [#${pug.id}] filled! (${pug.players.length}/${mode.team_size * 2})`,
                value: `**${redDot} Red Team** [${teams.red.rating}]\n:white_small_square: ${teams.red.players.map(p => '**' + p.nickname + '** (' + format(p.rating) + ') ').join(' :white_small_square:')}\n\n${blueDot} **Blue Team** [${teams.blue.rating}]\n:white_small_square: ${teams.blue.players.map(p => '**' + p.nickname + '** (' + format(p.rating) + ')').join(' :white_small_square: ')}`
            },
            {
                name: '\u200B',
                value: `:white_small_square: **Map:** ${map.name}\n:white_small_square: **Total Strength:** ${teams.red.rating + teams.blue.rating} ${redDot} ${redOdds}% : ${blueOdds}% ${blueDot}`
            },
        )
        .setColor('Random')
    return pugFilledEmbed
}
