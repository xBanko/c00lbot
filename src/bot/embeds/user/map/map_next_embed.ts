/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getAllEnabledMaps, getCurrentTop3Weights } from '../../../../db/dao/map'
import { getMapLastPlayed } from '../../../../db/dao/pug'
import { calculateOdds } from '../../../../pug/pick_map'
import { timeAgo } from '../../utils/time_ago'

export async function getNextMapEmbed() {
    const maps = await getAllEnabledMaps()

    const nextMapEmbed = new EmbedBuilder()

    if (maps.length > 0) {
        // const odds = await calculateOdds(maps)
        const topWeights = await getCurrentTop3Weights()
        // const namesArray = Array.from(odds!.keys())
        // const valuesArray = Array.from(odds!.values())

        const lastPlayedMap0 = (await getMapLastPlayed(topWeights[0].name))?.finished ?? 'never'
        const lastPlayedMap1 = (await getMapLastPlayed(topWeights[1].name))?.finished ?? 'never'
        const lastPlayedMap2 = (await getMapLastPlayed(topWeights[2].name))?.finished ?? 'never'

        nextMapEmbed
            .addFields(
                {
                    name: `${topWeights[0].name}`,
                    value: `(${Math.round(topWeights[0].current_weight)} weights) Last played ${timeAgo(lastPlayedMap0)}`
                },
                {
                    name: `${topWeights[1].name}`,
                    value: `(${Math.round(topWeights[1].current_weight)} weights) Last played ${timeAgo(lastPlayedMap1)}`
                },
                {
                    name: `${topWeights[2].name}`,
                    value: `(${Math.round(topWeights[2].current_weight)} weights) Last played ${timeAgo(lastPlayedMap2)}`
                },
            )
            .setColor('Random')
    } else {
        nextMapEmbed
            .addFields(
                {
                    name: `No maps enabled!`,
                    value: '\n\u200B'
                },
            )
    }
    return nextMapEmbed
}
