/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getAllEnabledMaps } from '../../../../db/dao/map'

export async function getMapListEmbed() {
    const maps = await getAllEnabledMaps()
    let value = `No maps enabled currently!`

    if (maps.length > 0) {
        value = `${maps.map(m => `**${m.name}**`).join(`\n`)}`
    }

    const mapListEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `Current Map List`,
                value,
            },
        )
        .setColor('Random')
    return mapListEmbed
}
