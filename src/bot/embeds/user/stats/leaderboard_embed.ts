/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getAllTopPlayers, getTopPlayers } from '../../../../db/dao/player'
import { greenuparrow, orangebox, reddownarrow } from '../../utils/constants'
import { getSuffixOf } from '../../utils/number_suffix'

export async function getLeaderboardEmbed(limit: number) {
    const players = await getTopPlayers(limit)
    const startNo = limit + 1

    const embed = {
        name: `Leaderboard ${startNo} - ${startNo + 9}`,
        value: ''
    }
    let i = startNo

    if (players.length > 0) {
        players.map(p => {
            let trend
            if (!p.rating_change || p.rating_change === 0) trend = orangebox
            else if (p.rating_change > 0) trend = greenuparrow
            else trend = reddownarrow

            embed.value += `${trend} ${getSuffixOf(i)} **${p.nickname}** - ${p.rating}\n`
            i++
        })
    } else {
        embed.value += `There is nobody this far down yet!`
    }

    const leaderboardEmbed = new EmbedBuilder()
        .addFields(
            embed
        )
        .setColor("Random")

    return leaderboardEmbed
}

export async function getLeaderboardEmbedNoLimit() {
    const players = await getAllTopPlayers()

    const leaderboardEmbed = new EmbedBuilder().setColor("Random")

    if (!players) {
        const embed = {
            name: `Leaderboard`,
            value: 'Leaderboard empty'
        }

        leaderboardEmbed.addFields(embed)
    } else {
        let embedNumber = 1
        if (players.length > 135) embedNumber = 5
        else if (players.length > 105) embedNumber = 4
        else if (players.length > 60) embedNumber = 3
        else if (players.length > 30) embedNumber = 2

        const part = Math.round(players.length / embedNumber)

        for (let i = 0; i < embedNumber; i++) {
            const embed = {
                name: i === 0 ? `Leaderboard` : '⠀⠀⠀',
                inline: true,
                value: ''
            }

            players.slice(i * part, (i + 1) * part).map(p => {
                // let trend
                // if (!p.rating_change || p.rating_change === 0) trend = orangebox
                // else if (p.rating_change > 0) trend = greenuparrow
                // else trend = reddownarrow

                embed.value += `${getSuffixOf(players.indexOf(p) + 1)} **${p.nickname}** - ${p.rating}\n`
            })

            leaderboardEmbed.addFields(embed)
        }
    }

    return leaderboardEmbed
}
