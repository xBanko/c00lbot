/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'
import { DailyStats } from '../../../../pug/stats/daily';
import { greenuparrow, orangebox, reddownarrow } from '../../utils/constants';

export async function formatSeasonalStats(stats: DailyStats) {

    if (!stats) {
        return new EmbedBuilder()
            .addFields({
                name: `Fail`,
                value: 'Stats have not been generated yet.',
                inline: true,
            })
            .setColor('Random')
    }

    const generalStatsEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `Most Played :nerd:`,
                inline: true,
                value: stats.totalPlayed && stats.totalPlayed.length >= 3 ? `[${stats.totalPlayed[0].stat}] **${stats.totalPlayed[0].nickname}**\n[${stats.totalPlayed[1].stat}] **${stats.totalPlayed[1].nickname}**\n[${stats.totalPlayed[2].stat}] **${stats.totalPlayed[2].nickname}**` : 'Still establishing top 3'
            },
            {
                name: `Cap Ratio Plus :triangular_flag_on_post:`,
                inline: true,
                value: stats.capDifferenceBest && stats.capDifferenceBest.length === 3 ? `+${stats.capDifferenceBest[0].stat} **${stats.capDifferenceBest[0].nickname}**\n+${stats.capDifferenceBest[1].stat} **${stats.capDifferenceBest[1].nickname}**\n+${stats.capDifferenceBest[2].stat} **${stats.capDifferenceBest[2].nickname}**` : 'Still establishing top 3'
            },
            {
                name: `Cap Ratio Minus :wastebasket:`,
                inline: true,
                value: stats.capDifferenceWorst && stats.capDifferenceWorst.length === 3 ? `${stats.capDifferenceWorst[0].stat} **${stats.capDifferenceWorst[0].nickname}**\n${stats.capDifferenceWorst[1].stat} **${stats.capDifferenceWorst[1].nickname}**\n${stats.capDifferenceWorst[2].stat} **${stats.capDifferenceWorst[2].nickname}**` : 'Still establishing top 3'
            },
            {
                name: `Most Wins :green_circle:`,
                inline: true,
                value: stats.topWins && stats.topWins.length === 3 ? `[${stats.topWins[0].stat}] **${stats.topWins[0].nickname}**\n[${stats.topWins[1].stat}] **${stats.topWins[1].nickname}**\n[${stats.topWins[2].stat}] **${stats.topWins[2].nickname}**` : 'Still establishing top 3'
            },
            {
                name: `Most Draws :orange_circle:`,
                inline: true,
                value: stats.topDraws && stats.topDraws.length === 3 ? `[${stats.topDraws[0].stat}] **${stats.topDraws[0].nickname}**\n[${stats.topDraws[1].stat}] **${stats.topDraws[1].nickname}**\n[${stats.topDraws[2].stat}] **${stats.topDraws[2].nickname}**` : 'Still establishing top 3'
            },
            {
                name: `Most Losses :red_circle:`,
                inline: true,
                value: stats.topLosses && stats.topLosses.length === 3 ? `[${stats.topLosses[0].stat}] **${stats.topLosses[0].nickname}**\n[${stats.topLosses[1].stat}] **${stats.topLosses[1].nickname}**\n[${stats.topLosses[2].stat}] **${stats.topLosses[2].nickname}**` : 'Still establishing top 3'
            },
            {
                name: `Highest Winrate ${greenuparrow}`,
                inline: true,
                value: stats.topWinrates && stats.topWinrates.length === 3 ? `${stats.topWinrates[0].stat}% **${stats.topWinrates[0].nickname}**\n${stats.topWinrates[1].stat}% **${stats.topWinrates[1].nickname}**\n${stats.topWinrates[2].stat}% **${stats.topWinrates[2].nickname}**` : 'Still establishing top 3'
            },
            {
                name: `Highest Drawrate ${orangebox}`,
                inline: true,
                value: stats.topDrawrates && stats.topDrawrates.length === 3 ? `${stats.topDrawrates[0].stat}% **${stats.topDrawrates[0].nickname}**\n${stats.topDrawrates[1].stat}% **${stats.topDrawrates[1].nickname}**\n${stats.topDrawrates[2].stat}% **${stats.topDrawrates[2].nickname}**` : 'Still establishing top 3'
            },
            {
                name: `Highest Lossrate ${reddownarrow}`,
                inline: true,
                value: stats.topLossRates && stats.topLossRates.length === 3 ? `${stats.topLossRates[0].stat}% **${stats.topLossRates[0].nickname}**\n${stats.topLossRates[1].stat}% **${stats.topLossRates[1].nickname}**\n${stats.topLossRates[2].stat}% **${stats.topLossRates[2].nickname}**` : 'Still establishing top 3'
            },
            {
                name: `Best Duos :kiss_woman_man:`,
                inline: true,
                value: stats.friendlyWinRates && stats.friendlyWinRates.length === 3 ? `${stats.friendlyWinRates[0].winRate}% **${stats.friendlyWinRates[0].player1}** & **${stats.friendlyWinRates[0].player2}**\n${stats.friendlyWinRates[1].winRate}% **${stats.friendlyWinRates[1].player1}** & **${stats.friendlyWinRates[1].player2}**\n${stats.friendlyWinRates[2].winRate}% **${stats.friendlyWinRates[2].player1}** & **${stats.friendlyWinRates[2].player2}**` : 'Still establishing top 3'
            },
            {
                name: `Drawish Duos :pencil:`,
                inline: true,
                value: stats.friendlyDrawRates && stats.friendlyDrawRates.length === 3 ? `${stats.friendlyDrawRates[0].drawRate}% **${stats.friendlyDrawRates[0].player1}** & **${stats.friendlyDrawRates[0].player2}**\n${stats.friendlyDrawRates[1].drawRate}% **${stats.friendlyDrawRates[1].player1}** & **${stats.friendlyDrawRates[1].player2}**\n${stats.friendlyDrawRates[2].drawRate}% **${stats.friendlyDrawRates[2].player1}** & **${stats.friendlyDrawRates[2].player2}**` : 'Still establishing top 3'
            },
            {
                name: `Worst Duos :person_facepalming:`,
                inline: true,
                value: stats.friendlyLossRates && stats.friendlyLossRates.length === 3 ? `${stats.friendlyLossRates[0].lossRate}% **${stats.friendlyLossRates[0].player1}** & **${stats.friendlyLossRates[0].player2}**\n${stats.friendlyLossRates[1].lossRate}% **${stats.friendlyLossRates[1].player1}** & **${stats.friendlyLossRates[1].player2}**\n${stats.friendlyLossRates[2].lossRate}% **${stats.friendlyLossRates[2].player1}** & **${stats.friendlyLossRates[2].player2}**` : 'Still establishing top 3'
            },
            {
                name: `Biggest Buddies :people_hugging:`,
                inline: true,
                value: stats.buddies && stats.buddies.length === 3 ? `[${stats.buddies[0].total}] ${stats.buddies[0].winRate}% **${stats.buddies[0].player1}** & **${stats.buddies[0].player2}** \n[${stats.buddies[1].total}] ${stats.buddies[1].winRate}% **${stats.buddies[1].player1}** & **${stats.buddies[1].player2}**\n[${stats.buddies[2].total}] ${stats.buddies[2].winRate}% **${stats.buddies[2].player1}** & **${stats.buddies[2].player2}**\n` : 'Still establishing top 3'
            },
            {
                name: `Biggest Rivals :japanese_goblin:`,
                inline: true,
                value: stats.rivals && stats.rivals.length === 3 ? `[${stats.rivals[0].total}] ${stats.rivals[0].player1winRate}% **${stats.rivals[0].player1}** <-> **${stats.rivals[0].player2}** ${stats.rivals[0].player2winRate}%\n[${stats.rivals[1].total}] ${stats.rivals[1].player1winRate}% **${stats.rivals[1].player1}** <-> **${stats.rivals[1].player2}** ${stats.rivals[1].player2winRate}%\n[${stats.rivals[2].total}] ${stats.rivals[2].player1winRate}% **${stats.rivals[2].player1}** <-> **${stats.rivals[2].player2}** ${stats.rivals[2].player2winRate}%` : 'Still establishing top 3'
            },
        )
        .setTitle(`${stats.seasonName} Stats`)
        .setFooter({ text: 'Updated every morning around 06:00' })
        .setColor('Random')
    return generalStatsEmbed
}
