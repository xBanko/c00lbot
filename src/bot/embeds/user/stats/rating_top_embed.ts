/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getStablePlayerRatingChange, getTopPlayerRatingChange, getWorstPlayerRatingChange } from '../../../../db/dao/player'
import { greenuparrow, orangebox, reddownarrow } from '../../utils/constants'

export async function getRatingTopEmbed() {
    const topPlayerRating = await getTopPlayerRatingChange()
    const worstPlayerRating = await getWorstPlayerRatingChange()
    const stablePlayerRating = await getStablePlayerRatingChange()

    let value = ''
    if (topPlayerRating.length === 0) {
        value += `No pugs played yet! :open_mouth:`
    } else {
        value += `**Top 3 gainers** ${greenuparrow}\n`
        value += `${topPlayerRating.map(t => `**${t.nickname}** from ${t.starting_rating} to ${t.rating} **+${t.diff}**`).join(`\n`)}\n`

        value += `\n**Top 3 losers** ${reddownarrow}\n`
        value += `${worstPlayerRating.map(w => `**${w.nickname}** from ${w.starting_rating} to ${w.rating} **${w.diff}**`).join(`\n`)}\n`

        if (stablePlayerRating.length > 0) {
            value += `\n**Most stable players** ${orangebox}\n`
            value += `${stablePlayerRating.map(s => `**${s.nickname}** has not moved from ${s.rating}`).join(`\n`)}`
        }
    }
    const ratingTopEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `Top Rating Changes`,
                value,
            },
        )
        .setColor('Random')
    return ratingTopEmbed
}
