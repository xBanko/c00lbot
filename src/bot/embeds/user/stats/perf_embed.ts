/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder, GuildMember } from 'discord.js'

import { getPlayerByDiscordId, getPlayerRank, getPlayerRatingChange } from '../../../../db/dao/player'
import { getLastPlayerPug } from '../../../../db/dao/pug'
import { Winrate } from '../../../../pug/stats/winrate'
import { getSuffixOf } from '../../utils/number_suffix'
import { timeAgo } from '../../utils/time_ago'

export async function getPerfEmbed(winrate: Winrate, discordId: string) {
    const user = await getPlayerByDiscordId(discordId)
    const pugs = await getLastPlayerPug(discordId)
    const rank = await getPlayerRank(discordId)
    const suffix = getSuffixOf(rank.rank)
    const userRating = await getPlayerRatingChange(discordId)
    const userLastRatingChange = user.rating_change! > 0 ? `+${user.rating_change}` : `${user.rating_change}`
    let diff = ``


    let value = ''
    if (winrate.total === 0) {
        value += `No pugs played yet! :open_mouth:`
    } else {
        if (userRating) {
            if (userRating.diff > 0) {
                diff = ` from ${userRating.starting_rating} **+${userRating.diff}** :chart_with_upwards_trend:`
            } else if (userRating.diff === 0) {
                diff = ` is perfectly stable with **${userRating.diff}** rating change`
            } else {
                diff = ` from ${userRating.starting_rating} **${userRating.diff}** :chart_with_downwards_trend:`
            }
        }

        value += `Current rating ${user.rating} ${diff} (${winrate.total} pugs played)\n\n`

        value += `:green_circle: W**${winrate.winRate}**%  :orange_circle: D**${winrate.drawRate}**%  :red_circle: L**${winrate.lossRate}**%\n`
        if (winrate.bestMap) value += `Best map is **${winrate.bestMap?.[0]}** W**${winrate.bestMap?.[1]}**% (${winrate.bestMap?.[2]})\n`
        if (winrate.worstMap) value += `Worst map is **${winrate.worstMap?.[0]}** L**${winrate.worstMap?.[1]}**% (${winrate.worstMap?.[2]})\n`

        value += `\nLast pug was ${pugs[0] == null ? 'not played yet' : timeAgo(pugs[0].started)} (${userLastRatingChange})\n`
    }
    const winrateEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `${user.nickname} [${suffix}]`,
                value,
            },
        )
        .setColor('Random')
    return winrateEmbed
}

export async function getCombinedPerfEmbed(winrate: Winrate, enemyWinrate: [number, number] | null, player1: GuildMember, player2: GuildMember) {
    let value = ''
    if (winrate.total === 0) {
        value += `These people have not played together yet! :dancer:`
    } else {
        value += `As teammates:\n`

        value += `:green_circle: W**${winrate.winRate}**%  :orange_circle: D**${winrate.drawRate}**%  :red_circle: L**${winrate.lossRate}**% (${winrate.total} played)\n`
        if (winrate.bestMap) value += `Best map is **${winrate.bestMap?.[0]}** W**${winrate.bestMap?.[1]}**% (${winrate.bestMap?.[2]})\n`
        if (winrate.worstMap) value += `Worst map is **${winrate.worstMap?.[0]}** L**${winrate.worstMap?.[1]}**% (${winrate.worstMap?.[2]})\n`
    }

    if (enemyWinrate !== null) {
        value += `\n`
        value += `As opponents:\n`

        value += `${player1.user.username} W**${enemyWinrate[0]}%** - W**${100 - enemyWinrate[0]}%** ${player2.user.username} (${enemyWinrate[1]} played) \n\n`
    }

    const winrateEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `${player1.user.username} & ${player2.user.username}`,
                value,
            },
        )
        .setColor('Random')
    return winrateEmbed
}
