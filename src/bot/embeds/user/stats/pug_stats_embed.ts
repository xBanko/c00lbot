/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getCountAllActivePlayers, getCountSeasonActivePlayers } from '../../../../db/dao/player'
import { getModeById } from '../../../../db/dao/mode'
import { getAllPugStats, getSeasonPugStats, getTodaysPugs } from '../../../../db/dao/pug'
import { getAllTotalCaps, getSeasonTotalCaps } from '../../../../db/dao/score'
import { pugMode, SeasonChoice } from '../../utils/constants'
import { timeAgo } from '../../utils/time_ago'
import { getBoldScore } from '../../utils/highlight_score'

export async function getPugStatsEmbed(when: SeasonChoice) {
    const mode = await getModeById(pugMode)
    const pugStatsEmbed = new EmbedBuilder().setColor("Random")

    let whenName = ''

    if (when === SeasonChoice.All) {
        whenName = 'All Time'
        const pugStats = await getAllPugStats(pugMode)
        const totalCaps = await getAllTotalCaps()
        const uniquePlayers = await getCountAllActivePlayers()
        if (pugStats) {
            const embed = {
                name: `Pug Stats ${whenName}`,
                value: ''
            }
            embed.value += `There has been a total of **${pugStats.count}** pugs with **${uniquePlayers.count}** unique players and a total of **${totalCaps.total}** caps!\nThe average amount of caps are **${pugStats.caps}** with the average difference being **${pugStats.diff}**.`
            pugStatsEmbed.addFields(embed)
        } else {
            const embed = {
                name: `Pug stats for ${whenName}`,
                value: `No pugs yet? :pirate_flag:`,
            }
            pugStatsEmbed.addFields(embed)
        }

    } else if (when === SeasonChoice.Current) {
        whenName = 'Current Season'
        const pugStats = await getSeasonPugStats(pugMode)
        const totalCaps = await getSeasonTotalCaps()
        const uniquePlayers = await getCountSeasonActivePlayers()
        if (pugStats) {
            const embed = {
                name: `Pug Stats ${whenName}`,
                value: ''
            }
            embed.value += `There has been a total of **${pugStats.count}** pugs with **${uniquePlayers.count}** unique players and a total of **${totalCaps.total}** caps!\nThe average amount of caps are **${pugStats.caps}** with the average difference being **${pugStats.diff}**.`
            pugStatsEmbed.addFields(embed)

        } else {
            const embed = {
                name: `Pug stats for ${whenName}`,
                value: `No pugs yet? :pirate_flag:`,
            }
            pugStatsEmbed.addFields(embed)
        }

    } else if (when === 'today') {
        const todaysPugs = await getTodaysPugs(pugMode)
        whenName = `Today [${todaysPugs.length}]`
        if (todaysPugs.length > 0) {
            let embedNumber = 1
            embedNumber = Math.floor(todaysPugs.length / 5) + 1
            if (embedNumber > 10) embedNumber = 10

            const part = Math.round(todaysPugs.length / embedNumber)

            for (let i = 0; i < embedNumber; i++) {
                const embed = {
                    name: i === 0 ? `Pug Stats ${whenName}` : '\u200B',
                    value: ''
                }

                todaysPugs.slice(i * part, (i + 1) * part).map(p => {
                    embed.value += `**${mode.name}** [#${p.id}] ${p.map_name}${getBoldScore(p)}${p.str_red! + p.str_blue!} (*${timeAgo(p.finished)}*)\n`
                })
                pugStatsEmbed.addFields(embed)
            }
        } else {
            const embed = {
                name: `Pug stats for ${whenName}`,
                value: `No pugs yet? :pirate_flag:`,
            }
            pugStatsEmbed.addFields(embed)
        }
    }
    return pugStatsEmbed
}
