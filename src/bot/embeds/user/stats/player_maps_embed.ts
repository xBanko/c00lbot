/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getAllPlayerMapStats, getSeasonPlayerMapStats } from '../../../../db/dao/map'
import { getPlayerName } from '../../../../db/dao/player'
import { SeasonChoice } from '../../utils/constants'

export async function getPlayerMapsEmbed(season: SeasonChoice, discordId: string) {
    const playerName = await getPlayerName(discordId)
    let maps
    let name

    if (season === SeasonChoice.Current) {
        maps = await getSeasonPlayerMapStats(discordId)
        name = 'This Season'
    } else {
        maps = await getAllPlayerMapStats(discordId)
        name = 'All Time'
    }

    const playerMapsEmbed = new EmbedBuilder()
        .setColor('Random')

    if (maps.length > 0) {
        let countName = ``
        let caps = ``
        let diff = ``

        for (const m of maps) {
            countName += `[${m.count}] **${m.name}**\n`
            caps += `*${m.caps}*\n`
            diff += `*${m.diff}*\n`
        }

        playerMapsEmbed.addFields(
            {
                name: `[#] Name`,
                value: countName,
                inline: true,
            },
        )

        playerMapsEmbed.addFields(
            {
                name: `Avg Caps`,
                value: caps,
                inline: true,
            },
        )

        playerMapsEmbed.addFields(
            {
                name: `Avg Cap Diff`,
                value: diff,
                inline: true,
            },
        )
    } else {
        playerMapsEmbed.addFields(
            {
                name: `No ${name} Stats For ${playerName} Yet`,
                value: `No pugs yet?? :pleading_face:`,
            },
        )
    }
    return playerMapsEmbed
}
