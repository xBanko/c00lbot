/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { LastPug } from '../../../../db/dao/pug'
import { getServerOfPug } from '../../../../db/dao/server'
import { getScoreColor } from '../../utils/highlight_score'
import { getLastField } from '../../utils/last_field'
import { timeAgo } from '../../utils/time_ago'

export async function getLastEmbed(lastPug: LastPug) {
    const lastField = await getLastField(lastPug)
    const server = await getServerOfPug(lastPug.id)
    let serverName = ''
    if (server) {
        serverName = server.shortname
    } else {
        serverName = 'undefined'
    }

    const lastEmbed = new EmbedBuilder()
        .addFields(
            lastField,
        )
        .setFooter({ text: `${timeAgo(lastPug.started)} on ${serverName}` })
        .setColor(getScoreColor(lastPug.score_red, lastPug.score_blue))
    return lastEmbed
}
