/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getCurrentPug } from '../../../../db/dao/pug'
import { pugMode } from '../../utils/constants'
import { getListField } from '../../utils/list_field'

export async function getListEmbed() {
    const livePug = await getCurrentPug(pugMode)
    const listField = await getListField(livePug)

    const listEmbed = new EmbedBuilder()
        .addFields(
            listField,
        )
        .setColor('Random')
    return listEmbed
}
