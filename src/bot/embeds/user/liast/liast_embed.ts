/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getCurrentPug, getLastPug } from '../../../../db/dao/pug'
import { getServerOfPug } from '../../../../db/dao/server'
import { pugMode } from '../../utils/constants'
import { getLastField } from '../../utils/last_field'
import { getListField } from '../../utils/list_field'
import { timeAgo } from '../../utils/time_ago'

export async function getLiastEmbed() {
    const livePug = await getCurrentPug(pugMode)
    const listField = await getListField(livePug)
    const lastPug = await getLastPug(pugMode)
    listField.value += '\n\u200B'

    const liastEmbed = new EmbedBuilder()
        .addFields(
            listField,
        )
        .setColor('Random')

    if (lastPug) {
        const lastField = await getLastField(lastPug)
        const server = await getServerOfPug(lastPug.id)
        let serverName = ''
        if (server) {
            serverName = server.shortname
        } else {
            serverName = 'undefined'
        }

        liastEmbed
            .addFields(
                lastField,
            )
            .setFooter({ text: `${timeAgo(lastPug.started)} on ${serverName}` })
            .setColor('Random')
    }
    return liastEmbed
}
