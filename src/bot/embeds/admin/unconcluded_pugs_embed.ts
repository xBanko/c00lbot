/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getUnconcludedPugs } from '../../../db/dao/score'

export async function getUnconcludedPugsEmbed() {
    const unconcludedPugs = await getUnconcludedPugs()
    let value = `No pugs needs to be scored at this moment`

    if (unconcludedPugs.length > 0) {
        value = `${unconcludedPugs.map(p => `[#${p.pug_id}] **${p.status}** Created: ${p.created}`).join(`\n`)}`
    }

    const unconcludedPugsEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `Unconcluded Pugs`,
                value,
            },
        )
        .setColor('Random')

    return unconcludedPugsEmbed
}
