/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

export async function getBanEmbed(player: string, reason: string, duration: number, admin: string, endTime: string, whereBanned: string) {
    const banEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `${player} was banned by ${admin} in ${whereBanned} pugs`,
                value: `Duration: ${duration} hours\nReason: ${reason}`
            },
        )
        .setFooter({ text: `${endTime}` })
        .setColor('Random')
    return banEmbed
}
