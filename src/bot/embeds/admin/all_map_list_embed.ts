/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

import { getAllMaps } from '../../../db/dao/map'

export async function getAllMapListEmbed() {
    const maps = await getAllMaps()

    const allMapListEmbed = new EmbedBuilder()
        .setColor('Random')

    if (maps.length === 0) {
        return allMapListEmbed.addFields(
            {
                name: `No Maps`,
                value: `Not a single map added yet?`,
            }
        )
    }

    let nameValue = ''
    let weightValue = ''
    let statusValue = ''

    for (const m of maps) {
        nameValue += '**' + m.name + '**' + '\n'
        weightValue += ' \u200B \u200B ' + m.default_weight + '\n'
        statusValue += m.status === 'ENABLED' ? ' \u200B \u200B :white_check_mark:' + '\n' : ' \u200B \u200B :x:' + '\n'
    }

    allMapListEmbed.addFields(
        {
            name: `Name`,
            value: nameValue,
            inline: true,
        },
    )

    allMapListEmbed.addFields(
        {
            name: `Weight`,
            value: weightValue,
            inline: true,
        },
    )

    allMapListEmbed.addFields(
        {
            name: `Status`,
            value: statusValue,
            inline: true,
        },
    )

    return allMapListEmbed
}
