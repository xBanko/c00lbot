/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { EmbedBuilder } from 'discord.js'

export async function getUnbanEmbed(player: string, reason: string, duration: number) {
    const unbanEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `${player} was unbanned`,
                value: `Duration: ${duration} hours\nReason: ${reason}`
            },
        )
        .setColor('Random')
    return unbanEmbed
}
