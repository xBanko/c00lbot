import schedule from 'node-schedule'
import { clearPasswords } from '../db/clear_passwords'
import { generateSeasonalStats } from '../pug/stats/daily'

export const startScheduler = async () => {
    // 6 AM
    schedule.scheduleJob('0 6 * * *', function () {
        generateSeasonalStats()

        clearPasswords()
    })
}