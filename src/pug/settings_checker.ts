import { getAllMapPreferencesForPlayers } from "../db/dao/map_preference"
import { getPreferencesForPlayers } from "../db/dao/server_preference"
import sqlite3 from 'better-sqlite3'
import config from 'config'
import { sendMessageToUser } from "../bot/bot"

const db = sqlite3(config.get('dbConfig.dbName'))

export const checkIfDudeHasServerPreferenceSet = async (userId: string): Promise<boolean> => {
    const serverSet = await getUserWarned(userId, 'serverpref')

    if (serverSet) return true

    const serverPref = await getPreferencesForPlayers(userId)

    if (serverPref.length > 0)  {
        await setUserWarned(userId, 'serverpref')
        return true
    }

    await sendMessageToUser(`:fire_engine: Attention: You haven't set your server preferences.
     You can do that by using command /serverpreferences set in the #ranked channel. By using this command, you will be more likely to play on servers that are better for you.
     This is a one time message.`, userId)

    await setUserWarned(userId, 'serverpref')

    return false
}

export const checkIfDudeHasMapPreferenceSet = async (userId: string): Promise<boolean> => {
    const serverSet = await getUserWarned(userId, 'mappref')

    if (serverSet) return true

    const mapPref = await getAllMapPreferencesForPlayers(userId)

    if (mapPref.length > 0) {
        await setUserWarned(userId, 'mappref')
        return true
    }

    await sendMessageToUser(`:fire_engine: Attention: You haven't set your map preferences.
    You can do that by using command /mappreferences set in the #ranked channel. By using this command, maps that you prefer have a bigger chance of getting chosen.
    This is a one time message.`, userId)

    await setUserWarned(userId, 'mappref')

    return false
}

const setUserWarned = async (userId: string, warningType: string): Promise<void> => {
    await db.prepare(`INSERT INTO warnings (player_id, warning_type) VALUES (?, ?)`).run(userId, warningType)
}

const getUserWarned = async (userId: string, warningType: string): Promise<boolean> => {
    const warned = await db.prepare(`select * from warnings where player_id = ? and warning_type = ?`).get(userId, warningType)
    
    return warned ? true : false
}