/* eslint-disable @typescript-eslint/no-non-null-assertion */
import config from 'config'

export interface DeadPugResult {
    dead: boolean,
    voted?: string,
}

export class DeadPugger {
    deadpugVotes: Map<string, number> = new Map()
    userVoted: Map<string, string> = new Map()
    private deadPugVotesRequired = <number>config.get('pug.deadpugVotesRequired')


    deadpug = (pugId: string, discordId: string, team: 'RED' | 'BLUE'): DeadPugResult => {
        if (this.userVoted.get(pugId) !== undefined && this.userVoted.get(pugId)?.includes(discordId)) return { dead: false }
        this.userVoted.set(pugId, this.userVoted.get(pugId) ? this.userVoted.get(pugId) + ',' + discordId : discordId)

        const otherTeam = team === 'RED' ? 'BLUE' : 'RED'

        const teamDeadpugVotes = this.deadpugVotes.get(pugId + team)

        if (!teamDeadpugVotes) {
            this.deadpugVotes.set(pugId + team, 1)
        } else {
            this.deadpugVotes.set(pugId + team, teamDeadpugVotes + 1)
        }

        const otherTeamVotes = this.deadpugVotes.get(pugId + otherTeam)

        if (this.deadpugVotes.get(pugId + team) &&
            this.deadpugVotes.get(pugId + team)! >= this.deadPugVotesRequired &&
            otherTeamVotes && otherTeamVotes >= this.deadPugVotesRequired) {
            this.deadpugVotes.delete(pugId + otherTeam)
            this.deadpugVotes.delete(pugId + team)
            const voted = this.userVoted.get(pugId)
            this.userVoted.delete(pugId)
            return { dead: true, voted }
        }

        return { dead: false }
    }
}
