import combinations from 'combinations'
import { Player, SuggestedTeams } from '../db/dao/player'

export function makeTeams(players: Player[], teamSize: number): SuggestedTeams {
    let red: Player[] = []
    let blue: Player[] = []

    const avgPickHalved = players.reduce((a, b: Player) => a + b.rating, 0) / 2
    const playerCombinations = combinations(players, teamSize, teamSize)

    let bestCombinations: Player[][] = []
    let currentBestCombinationScore = Number.MAX_SAFE_INTEGER

    for (const combo of playerCombinations) {
        if (combo.length !== teamSize) continue //bug in lib

        const comboPick = combo.reduce((a, b: Player) => a + b.rating, 0)
        if (Math.abs(avgPickHalved - comboPick) < Math.abs(avgPickHalved - currentBestCombinationScore)) {
            bestCombinations = [combo]
            currentBestCombinationScore = comboPick
        } else if (Math.abs(avgPickHalved - comboPick) === Math.abs(avgPickHalved - currentBestCombinationScore)) {
            bestCombinations.push(combo)
        }
    }

    const currentBestCombination = bestCombinations[Math.floor(Math.random() * bestCombinations.length)]

    if (Math.random() < 0.5) {
        red = red.concat(currentBestCombination)
        blue = blue.concat(players.filter(p => !red.includes(p)))
    } else {
        blue = blue.concat(currentBestCombination)
        red = red.concat(players.filter(p => !blue.includes(p)))
    }

    return {
        red: {
            players: red,
            rating: red.reduce((a, b: Player) => a + b.rating, 0)
        },
        blue: {
            players: blue,
            rating: blue.reduce((a, b: Player) => a + b.rating, 0)
        }
    }
}
