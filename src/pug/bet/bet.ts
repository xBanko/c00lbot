import {
    addCoinsToPlayer, checkPugBettingEligibility, doesPlayerHaveMoneyz, calculateBettingOdds, finishBet, getPlayerBetInfo,
    getPlayerCoins,
    insertBet, isFirstBet, removeCoinsFromPlayer, selectUnfinishedBetsForPug, insertOdds
} from "../../db/dao/bet"

export enum BetOption {
    Red = 'RED',
    Blue = 'BLUE',
    Draw = 'DRAW',
}

export enum BetDeadOption {
    Dead = 'DEAD',
}

export type BetResult = BetOption | BetDeadOption

export interface BetPromise {
    success: boolean
    amount?: number
    msg?: string
}

export interface BetOdds {
    red: number
    draw: number
    blue: number
}

export interface ResolvedBet {
    name: string
    betAmount: number
    net: number
    balance: number
    betOn: BetOption
}

export const setBettingOdds = async (pugId: number | string): Promise<BetOdds> => {
    const odds = await calculateBettingOdds(pugId)
    await insertOdds(pugId, 100 / odds.blue, 100 / odds.red, 100 / odds.draw)

    return {
        red: 100 / odds.red,
        blue: 100 / odds.blue,
        draw: 100 / odds.draw,
    }
}

export const resolveBets = async (whoWon: BetResult, pugId: number | string): Promise<ResolvedBet[]> => {
    const betsForPug = await selectUnfinishedBetsForPug(pugId, whoWon)

    const betsResult = []
    if (whoWon === BetDeadOption.Dead) {
        for (const bet of betsForPug) {
            await addCoinsToPlayer(bet.player_id, bet.amount)
            await finishBet(bet.id, 0)
        }
    } else {
        for (const bet of betsForPug) {
            let coinChange = 0

            if (bet.bet_on === whoWon) {
                coinChange = Math.round(bet.amount * bet.odds)

                await addCoinsToPlayer(bet.player_id, coinChange)
                await finishBet(bet.id, coinChange - bet.amount)
            } else {
                coinChange = -bet.amount
                await finishBet(bet.id, coinChange)
            }

            const playerInfo = await getPlayerBetInfo(bet.player_id)

            betsResult.push({
                name: playerInfo.nickname,
                betAmount: bet.amount,
                net: coinChange,
                balance: playerInfo.coins,
                betOn: bet.bet_on
            })
        }
    }
    return betsResult
}

export const betOnResult = async (playerId: string, pugId: number, amount: string | null, betOption: BetOption): Promise<BetPromise> => {
    if (!amount) {
        return buildErrorMessage('You need to bet an amount, silly goose')
    }

    let amountNr

    if (amount === 'all') {
        amountNr = (await getPlayerCoins(playerId)).coins
    } else {
        amountNr = Number(amount)
    }

    if (isNaN(Number(amountNr))) {
        return buildErrorMessage(`That is not correct at all`)
    }

    if (!await doesPlayerHaveMoneyz(playerId, amountNr)) {
        return buildErrorMessage(`You do not have enough coins to place such a bet`)
    }

    if (amountNr <= 0) {
        return buildErrorMessage('Bet is too low')
    }

    if (!await checkPugBettingEligibility(pugId)) {
        return buildErrorMessage('This pug is not eligible for betting (you can only bet 5 minutes after the pug filled)')
    }

    const firstBet = await isFirstBet(playerId, pugId)

    const betAmount = firstBet ? amountNr : Math.round(amountNr * 0.85)

    await insertBet(playerId, pugId, betAmount, betOption)
    await removeCoinsFromPlayer(playerId, amountNr)

    return { success: true, amount: betAmount }
}

const buildErrorMessage = (msg: string): BetPromise => {
    return {
        success: false,
        msg
    }
}
