import { generate } from 'generate-password'

const generatePassword = (): string => {
    return generate({
        length: 3,
        numbers: true,
        lowercase: true,
        uppercase: false,
    })
}

export const generateMultiplePasswords = (num: number): string[] => {
    const pwds: string[] = []
    
    while (pwds.length < num) {
        const pwd = generatePassword()
        if (!pwds.includes(pwd)) pwds.push(pwd)
    }

    return pwds
}
