import { getPreferencesForPlayers } from '../../db/dao/server_preference'
import { getNonBusyServers, getServerByShortname, Server } from '../../db/dao/server'

export const findOptimalServer = async (players: string): Promise<Server | null> => {
    const servers = await getNonBusyServers()

    // 0 servers available
    if (servers.length === 0) {
        return null
    }

    // only 1 server available
    if (servers.length === 1) {
        return servers[0]
    }

    const preferences = await getPreferencesForPlayers(players)

    const preferencesMap: Map<string, number> = new Map()

    for (const p of preferences) {
        if (!servers.find(s => s.shortname === p.server_shortname )) continue

        const currentScore = preferencesMap.get(p.server_shortname)
        if (!currentScore) preferencesMap.set(p.server_shortname, p.score)
        else preferencesMap.set(p.server_shortname, currentScore + p.score)
    }

    const sortedMap = new Map([...preferencesMap.entries()].sort((a, b) => b[1] - a[1]))
    

    if (!sortedMap.values().next().value || sortedMap.keys().next().value < 0) {
        const shortname =  servers[Math.floor(Math.random() * servers.length)].shortname

        return await getServerByShortname(shortname)
    }

    return await getServerByShortname(sortedMap.keys().next().value)
}
