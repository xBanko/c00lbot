import { Guild } from 'discord.js'

import { bot, sendEmbed } from '../bot/bot'
import { getUnbanEmbed } from '../bot/embeds/admin/unban_embed'
import { disc_banned_role_id, disc_server_id } from '../config'
import { Ban, getAllCurrentBans, updateBanStatus } from '../db/dao/ban'

// Remove the banned role from the player and update the ban status in the database
export async function unbanPlayer(guild: Guild, ban: Ban) {
    const current_date = new Date()
    const endOfBan = new Date(ban.end_time)

    if (current_date > endOfBan) {
        try {
            if (!ban.id) {
                return
            }
            const player = guild.members.cache.get(ban.discord_id)
            if (player) {
                await player.roles.remove(disc_banned_role_id)
                await updateBanStatus(ban.id, 'unbanned')
                const unbanEmbed = await getUnbanEmbed(player.user.username, ban.reason, ban.duration)
                await sendEmbed(unbanEmbed)
            }
        } catch (error) {
            console.error(`Error unbannig player with ID ${ban.discord_id}:`, error)
        }
    }
}

// Check if the player is in the cache and unban them if they are
async function processUnban(guild: Guild, ban: Ban) {
    const player = guild.members.cache.get(ban.discord_id)
    if (player) {
        await unbanPlayer(guild, ban)
    } else {
        console.log(`Player with ID ${ban.discord_id} is not on the server, waiting for them to join.`)
    }
}

// Process all bans that are ready to be unbanned
async function processUnbans(guild: Guild) {
    const bansToUnban = await getAllCurrentBans()

    if (bansToUnban) {
        const current_date = new Date()

        const unbanPromises = bansToUnban.map(async ban => {
            if (!ban.id) {
                return
            }

            const endOfBan = new Date(ban.end_time)
            if (current_date > endOfBan) {
                await processUnban(guild, ban)
            }
        })
        await Promise.all(unbanPromises)
    }
}

// Start the unban process
export const startProcessing = async () => {
    const guild = await bot.client.guilds.fetch(disc_server_id)
    setInterval(() => processUnbans(guild), 60000)
}
