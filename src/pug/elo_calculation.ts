// eslint-disable-next-line @typescript-eslint/no-var-requires
const Elo = require('@pelevesque/elo')

interface RatingChange {
    teamRed: {
        delta: number,
        newRating: number,
    },
    teamBlue: {
        delta: number,
        newRating: number,
    },
}

interface EloResult {
    a: {
        delta: number,
        rating: number
    },
    b: {
        delta: number,
        rating: number
    }
}

export const calculateElo = async (redRating: number, blueRating: number, scoreRed: number, scoreBlue: number, doubleElo = false): Promise<RatingChange> => {
    const elo = new Elo()

    let result: number
    if (scoreRed > scoreBlue) {
        result = 1
    } else if (scoreBlue > scoreRed) {
        result = 0
    } else result = 0.5

    const capDiff = Math.abs(scoreRed - scoreBlue)

    if (capDiff === 0) {
        let redDelta = 0
        let blueDelta = 0
        let redNewRating = redRating
        let blueNewRating = blueRating

        if (redRating > blueRating) {
            redDelta = -1
            blueDelta = 1
            redNewRating -= 1
            blueNewRating += 1
        } else if (blueRating > redRating) {
            redDelta = 1
            blueDelta = -1
            redNewRating += 1
            blueNewRating -= 1
        }

        return {
            teamRed: {
                delta: doubleElo ? redDelta * 2 : redDelta,
                newRating: redNewRating,
            },
            teamBlue: {
                delta: doubleElo ? blueDelta * 2 : blueDelta,
                newRating: blueNewRating,
            },
        }

    } else {
        const capDiffAddon = 10 + capDiff
        const newElos: EloResult = await elo.getOutcome(redRating, blueRating, result, capDiffAddon, 1105)

        const redDelta = +(Math.round((newElos.a.delta * 1000)/10)/100).toFixed(0)
        const blueDelta = +(Math.round((newElos.b.delta * 1000)/10)/100).toFixed(0)

        return {
            teamRed: {
                delta: doubleElo ? redDelta * 2 : redDelta,
                newRating: parseInt(Math.round(newElos.a.rating).toFixed(0), 10),
            },
            teamBlue: {
                delta: doubleElo ? blueDelta * 2 : blueDelta,
                newRating: parseInt(Math.round(newElos.b.rating).toFixed(0), 10),
            },
        }
    }
}
