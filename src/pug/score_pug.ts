import { setPasswordsNotActiveForPug } from '../db/dao/password'
import { ScoreStatus } from '../bot/embeds/utils/constants'
import { sendBetResultMsg } from '../bot/internal/bet_result'
import { concludeScoreMsg, sendScoreMsg } from '../bot/internal/score'
import { addCoinsToPlayer } from '../db/dao/bet'
import { finishPug, getPugById, updateRatings } from '../db/dao/pug'
import { getLiveScoreForIp, updateScore, updateScoreStatus } from '../db/dao/score'
import { setServerFree } from '../db/dao/server'
import { resolveBets } from './bet/bet'
import { calculateElo } from './elo_calculation'
import { resetPasswordForIp } from './server/setup_server'
import { generateLeaderboard } from './stats/leaderboard'
import { whoWon } from './who_won'
import { isPugDoubleElo } from '../db/dao/pug_boosts'

export interface FinishedScore {
    red: number,
    blue: number,
    ip: string,
}

export const scorePug = async (finishedScore: FinishedScore) => {
    const scoreToProcess = await getLiveScoreForIp(finishedScore.ip)
    if (!scoreToProcess) {
        console.log('Could not find live server/score/pug for request')
        return
    }

    const pugId = scoreToProcess.pug_id

    const scoreMsgId = await sendScoreMsg(pugId, parseInt(scoreToProcess.id), finishedScore.red, finishedScore.blue)

    if (scoreMsgId !== undefined) {
        const pug = await getPugById(scoreToProcess.pug_id)

        await updateScore(finishedScore.red, finishedScore.blue, scoreToProcess.id, ScoreStatus.Concluded)
        await finishPug(scoreMsgId, pugId.toString())

        const doubleElo = await isPugDoubleElo(parseInt(pug.id))
        const newElos = await calculateElo(pug.str_red, pug.str_blue, finishedScore.red, finishedScore.blue, doubleElo)
        await updateRatings(pug, newElos.teamRed.delta, newElos.teamBlue.delta)

        await updateScoreStatus(scoreToProcess.id, ScoreStatus.Accepted)

        await concludeScoreMsg({
            ...pug,
            rating_change_red: newElos.teamRed.delta,
            rating_change_blue: newElos.teamBlue.delta,
            finished: new Date(),
        }, {
            ...scoreToProcess,
            score_msg_id: scoreMsgId,
            score_red: finishedScore.red,
            score_blue: finishedScore.blue
        })

        await Promise.all(pug.players.split(',').map(async p => await addCoinsToPlayer(p, 25)))

        const resolvedBets = await resolveBets(whoWon(finishedScore.red, finishedScore.blue), pugId.toString())
        await sendBetResultMsg(pugId, resolvedBets)
    }

    await setServerFree(scoreToProcess.shortname)
    await setPasswordsNotActiveForPug(pugId)
    await resetPasswordForIp(scoreToProcess.shortname)

    generateLeaderboard()
}
