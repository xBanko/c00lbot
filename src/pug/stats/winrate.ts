/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { SeasonChoice } from '../../bot/embeds/utils/constants'
import { getAllMaps } from '../../db/dao/map'
import { getAllPugsWithTwoTeammates, getAllScoredPugsForPlayer, getSeasonPugsWithTwoTeammates, getSeasonScoredPugsForPlayer, WonPugsTeammates } from '../../db/dao/pug'

export interface Winrate {
    wins: number,
    draws: number,
    losses: number,
    winRate: number,
    drawRate: number,
    lossRate: number,
    total: number,
    bestMap?: [string, number, number]
    worstMap?: [string, number, number]
}

export const calculateWinrate = async (season: SeasonChoice, playerId: string, teammateId?: string): Promise<Winrate> => {
    let wins = 0
    let draws = 0
    let losses = 0
    let total = 0
    let bestMap: [string, number, number] = ['', 0, 0]
    let worstMap: [string, number, number] = ['', 0, 0]


    let pugs: WonPugsTeammates
    if (season === SeasonChoice.Current) {
        if (teammateId) {
            pugs = await getSeasonPugsWithTwoTeammates(playerId, teammateId)
        } else {
            pugs = await getSeasonScoredPugsForPlayer(playerId)
        }
    } else {
        if (teammateId) {
            pugs = await getAllPugsWithTwoTeammates(playerId, teammateId)
        } else {
            pugs = await getAllScoredPugsForPlayer(playerId)
        }
    }

    const mapSet = new Map()

    const maps = await getAllMaps()
    maps.push({ id: '0', current_weight: 0, default_weight: 0, name: 'Other'})
    mapSet.set('0', [0, 0, 0])

    for (const m of maps) {
        mapSet.set(m.id, [0, 0, 0])
    }

    for (const pug of pugs.blue) {
        let currentMapData
        if (mapSet.has(pug.map_id))
            currentMapData = mapSet.get(pug.map_id)
        else 
            currentMapData = mapSet.get('0')

        total += 1
        if (pug.score_blue > pug.score_red) {
            wins += 1
            mapSet.set(pug.map_id, [currentMapData[0] + 1, currentMapData[1], currentMapData[2]])
        }
        else if (pug.score_blue < pug.score_red) {
            losses += 1
            mapSet.set(pug.map_id, [currentMapData[0], currentMapData[1], currentMapData[2] + 1])
        }
        else {
            draws += 1
            mapSet.set(pug.map_id, [currentMapData[0], currentMapData[1] + 1, currentMapData[2]])
        }
    }

    for (const pug of pugs.red) {
        let currentMapData
        if (mapSet.has(pug.map_id))
            currentMapData = mapSet.get(pug.map_id)
        else 
            currentMapData = mapSet.get('0')

        total += 1

        if (pug.score_blue < pug.score_red) {
            wins += 1
            mapSet.set(pug.map_id, [currentMapData[0] + 1, currentMapData[1], currentMapData[2]])
        } else if (pug.score_blue > pug.score_red) {
            losses += 1
            mapSet.set(pug.map_id, [currentMapData[0], currentMapData[1], currentMapData[2] + 1])
        } else {
            draws += 1
            mapSet.set(pug.map_id, [currentMapData[0], currentMapData[1] + 1, currentMapData[2]])
        }
    }

    for (const [key, value] of mapSet.entries()) {
        const total = value[0] + value[1] + value[2]
        if (total < 3) continue

        const mapWinrate = Math.round(value[0] / total * 100)
        const lossRate = Math.round(value[2] / total * 100)

        if (mapWinrate > bestMap[1]) {
            bestMap = [key, mapWinrate, total]
        }

        if (lossRate > worstMap[1]) {
            worstMap = [key, lossRate, total]
        }
    }

    if (bestMap[0] !== '') {
        bestMap[0] = maps.find(m => m.id === bestMap[0]) ? maps.find(m => m.id === bestMap[0])!.name : 'Discontinued map'
    }

    if (worstMap[0] !== '') {
        worstMap[0] = maps.find(m => m.id === worstMap[0]) ? maps.find(m => m.id === worstMap[0])!.name : 'Discontinued map'
    }

    return {
        winRate: Math.round(wins / total * 100),
        drawRate: Math.round(draws / total * 100),
        lossRate: Math.round(losses / total * 100),
        wins,
        draws,
        losses,
        total,
        bestMap: bestMap[1] !== 0 ? bestMap : undefined,
        worstMap: worstMap[1] !== 0 ? worstMap : undefined,
    }
}
