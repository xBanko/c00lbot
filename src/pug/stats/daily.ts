import { sendStatsMessage } from '../../bot/bot'
import { formatSeasonalStats } from '../../bot/embeds/user/stats/seasonal_stats_embed'
import { SeasonChoice } from '../../bot/embeds/utils/constants'
import { getAllActivePlayers, getPlayerByDiscordId, Player } from '../../db/dao/player'
import { getEnemyWinrateSeason, getScoredPugs, LivePugScored } from '../../db/dao/pug'
import { getCurrentSeason, setCurrentSeasonStatsMsgId } from '../../db/dao/season'
import { calculateWinrate } from './winrate'

export interface DailyStats {
    seasonName: string,

    totalPlayed: PlayerStat[],
    capDifferenceBest: PlayerStat[],
    capDifferenceWorst: PlayerStat[],

    topWins: PlayerStat[],
    topDraws: PlayerStat[],
    topLosses: PlayerStat[],

    topWinrates: PlayerStat[],
    topDrawrates: PlayerStat[],
    topLossRates: PlayerStat[],

    friendlyWinRates: TeammateStats[],
    friendlyDrawRates: TeammateStats[],
    friendlyLossRates: TeammateStats[],

    friendlyWins: TeammateStats[],
    friendlyDraws: TeammateStats[],
    friendlyLosses: TeammateStats[],

    buddies: TeammateStats[],
    rivals: EnemyStats[],
}

interface PlayerStat extends Player {
    stat: number
}

interface DuoStat {
    player1: string
    player2: string
}

interface TeammateStats extends DuoStat {
    winRate: number,
    drawRate: number,
    lossRate: number,
    wins: number,
    draws: number,
    losses: number,
    total: number,
}

interface EnemyStats extends DuoStat {
    player1winRate: number,
    player2winRate: number,
    total: number
}


export const generateTopPlayed = async (allPugs: LivePugScored[], allPlayers: Player[]): Promise<PlayerStat[]> => {
    let playersWithTotal = []

    for (const p of allPlayers) {
        playersWithTotal.push({ ...p, stat: allPugs.filter((pug) => (pug.players.includes(p.discord_id))).length })
    }

    playersWithTotal = playersWithTotal.sort((a, b) => Number(b.stat) - Number(a.stat))

    return playersWithTotal
}

/**
 * returns [topWins, topDraws, topLosses, topWinRates, topDrawRates, topLossRates]
 */
export const generateTop3Puggers = async (allPlayers: Player[]): Promise<[PlayerStat[], PlayerStat[], PlayerStat[], PlayerStat[], PlayerStat[], PlayerStat[]]> => {
    let topWinRates = []
    let topDrawRates = []
    let topLossRates = []

    let topWins = []
    let topDraws = []
    let topLosses = []

    for (const p of allPlayers) {
        const playerWinrate = await calculateWinrate(SeasonChoice.Current, p.discord_id)

        if (playerWinrate.total > 4) {
            topWinRates.push({ ...p, stat: playerWinrate.winRate })
            topDrawRates.push({ ...p, stat: playerWinrate.drawRate })
            topLossRates.push({ ...p, stat: playerWinrate.lossRate })

            topWins.push({ ...p, stat: playerWinrate.wins })
            topDraws.push({ ...p, stat: playerWinrate.draws })
            topLosses.push({ ...p, stat: playerWinrate.losses })
        }
    }

    topWinRates = topWinRates.sort((a, b) => Number(b.stat) - Number(a.stat))
    topDrawRates = topDrawRates.sort((a, b) => Number(b.stat) - Number(a.stat))
    topLossRates = topLossRates.sort((a, b) => Number(b.stat) - Number(a.stat))

    topWins = topWins.sort((a, b) => Number(b.stat) - Number(a.stat))
    topDraws = topDraws.sort((a, b) => Number(b.stat) - Number(a.stat))
    topLosses = topLosses.sort((a, b) => Number(b.stat) - Number(a.stat))

    return [topWins, topDraws, topLosses, topWinRates, topDrawRates, topLossRates]
}


export const generateDuoData = async (allPlayers: Player[], allPugs: LivePugScored[]): Promise<[TeammateStats[], EnemyStats[]]> => {
    const friendly: TeammateStats[] = []
    const enemy: EnemyStats[] = []

    for (const p of allPlayers) {
        const partnersSet: Set<string> = new Set()
        const enemySet: Set<string> = new Set()


        for (const pug of allPugs) {

            if (!pug.players.includes(p.discord_id)) continue
            if (pug.team_blue.includes(p.discord_id)) {
                const partners = (<string><unknown>pug.team_blue).split(',')
                const enemies = (<string><unknown>pug.team_red).split(',')
                for (const part of partners) {
                    if (p.discord_id == part) continue

                    partnersSet.add(part)
                }

                for (const enemy of enemies) {
                    enemySet.add(enemy)
                }
            }

            else if (pug.team_red.includes(p.discord_id)) {
                const partners = (<string><unknown>pug.team_red).split(',')
                const enemies = (<string><unknown>pug.team_blue).split(',')
                for (const part of partners) {
                    if (p.discord_id == part) continue

                    partnersSet.add(part)
                }

                for (const enemy of enemies) {
                    enemySet.add(enemy)
                }
            }
        }

        for (const partner of partnersSet) {
            const partnerNick = (await getPlayerByDiscordId(partner)).nickname

            if (friendly.some(f => (f.player1 == partnerNick && f.player2 == p.nickname) ||
                (f.player2 == partnerNick && f.player1 == p.nickname))) continue

            const winrate = await calculateWinrate(SeasonChoice.Current, p.discord_id, partner)

            if (winrate.total > 4) {
                friendly.push({
                    player1: p.nickname,
                    player2: partnerNick,
                    winRate: winrate.winRate,
                    drawRate: winrate.drawRate,
                    lossRate: winrate.lossRate,
                    wins: winrate.wins,
                    losses: winrate.losses,
                    draws: winrate.draws,
                    total: winrate.total
                })
            }
        }

        for (const e of enemySet) {
            const enemyNick = (await getPlayerByDiscordId(e)).nickname

            if (enemy.some(f => (f.player1 == enemyNick && f.player2 == p.nickname) ||
                (f.player2 == enemyNick && f.player1 == p.nickname))) continue

            const enemyWinrate = await getEnemyWinrateSeason(p.discord_id, e)

            if (enemyWinrate && enemyWinrate[1] > 4) {
                enemy.push({
                    player1: p.nickname,
                    player2: enemyNick,
                    player1winRate: Math.round(enemyWinrate[0]),
                    player2winRate: 100 - Math.round(enemyWinrate[0]),
                    total: enemyWinrate[1],
                })
            }
        }
    }

    return [friendly, enemy]
}

const getTopCapDiff = async (allPlayers: Player[], allPugs: LivePugScored[]) => {
    const players: PlayerStat[] = allPlayers.map(p => ({ ...p, stat: 0 }))

    for (const p of players) {
        for (const pug of allPugs) {
            if (pug.players.includes(p.discord_id)) {
                if (pug.team_blue.includes(p.discord_id)) {
                    p.stat += pug.score_blue - pug.score_red
                } else {
                    p.stat += pug.score_red - pug.score_blue
                }
            }
        }
    }

    return players.sort((a, b) => Number(b.stat) - Number(a.stat))
}

export const generateSeasonalStats = async (): Promise<void> => {
    const t0 = performance.now()

    const season = await getCurrentSeason()
    if (!season) return

    const allPugs = await getScoredPugs()
    const allPlayers = await getAllActivePlayers()

    const topPlayed = await generateTopPlayed(allPugs, allPlayers)
    const totalPlayed = topPlayed.slice(0, 10)

    const top3WinLoss = await generateTop3Puggers(allPlayers)
    const topWins = top3WinLoss[0].slice(0, 3)
    const topDraws = top3WinLoss[1].slice(0, 3)
    const topLosses = top3WinLoss[2].slice(0, 3)
    const topWinrates = top3WinLoss[3].slice(0, 3)
    const topDrawrates = top3WinLoss[4].slice(0, 3)
    const topLossRates = top3WinLoss[5].slice(0, 3)

    const duoData = await generateDuoData(allPlayers, allPugs)

    const capDifference = await getTopCapDiff(allPlayers, allPugs)

    const friendlyWinRates = duoData[0].sort((a, b) => Number(b.winRate) - Number(a.winRate)).slice(0, 3)
    const friendlyDrawRates = duoData[0].sort((a, b) => Number(b.drawRate) - Number(a.drawRate)).slice(0, 3)
    const friendlyLossRates = duoData[0].sort((a, b) => Number(b.lossRate) - Number(a.lossRate)).slice(0, 3)

    const friendlyWins = duoData[0].sort((a, b) => Number(b.wins) - Number(a.wins)).slice(0, 3)
    const friendlyDraws = duoData[0].sort((a, b) => Number(b.draws) - Number(a.draws)).slice(0, 3)
    const friendlyLosses = duoData[0].sort((a, b) => Number(b.losses) - Number(a.losses)).slice(0, 3)

    const buddies = duoData[0].sort((a, b) => Number(b.total) - Number(a.total)).slice(0, 3)

    const rivals = duoData[1].sort((a, b) => Number(b.total) - Number(a.total)).slice(0, 3)

    const capDifferenceBest = capDifference.slice(0, 3)
    const capDifferenceWorst = capDifference.reverse().slice(0, 3)

    const dailyStats: DailyStats = {
        seasonName: season.name,

        totalPlayed,
        capDifferenceBest,
        capDifferenceWorst,

        topWins,
        topDraws,
        topLosses,

        topWinrates,
        topDrawrates,
        topLossRates,

        friendlyWinRates,
        friendlyDrawRates,
        friendlyLossRates,

        friendlyWins,
        friendlyDraws,
        friendlyLosses,

        buddies,
        rivals,
    }

    const t1 = performance.now()

    console.log(`Daily stats finished processing in  ${t1 - t0} milliseconds.`)

    const msgId = await sendStatsMessage({ embeds: [await formatSeasonalStats(dailyStats)] }, season.stats_msg_id)

    if (msgId) await setCurrentSeasonStatsMsgId(msgId, season.id)
}
