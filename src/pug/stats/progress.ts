import { ChartJSNodeCanvas } from 'chartjs-node-canvas'
import * as fs from 'fs'

import { getPlayerRating } from '../../db/dao/player'
import { getAllFinishedPugsForUser } from '../../db/dao/pug'

const randomNum = () => Math.floor(Math.random() * (235 - 52 + 1) + 52)
const randomRGB = () => `rgb(${randomNum()}, ${randomNum()}, ${randomNum()})`

export const generateProgressImageForUser = async (discordIds: string[], limit = 20): Promise<Buffer> => {
  let data = []

  if (limit && limit > 40) limit = 40

  for (const discordId of discordIds) {
    const pugsForUser = await (await getAllFinishedPugsForUser(discordId, discordIds.length > 1 ? limit : undefined))
    const ratingAndName = await getPlayerRating(discordId)
    const playerRating = ratingAndName.rating

    const ratings: number[] = [playerRating]
    let currentRating = playerRating

    for (const pug of pugsForUser) {
      let ratingChange = 0

      if (pug.team_red.includes(discordId)) ratingChange = pug.rating_change_red!
      else ratingChange = pug.rating_change_blue!

      currentRating = currentRating - ratingChange
      ratings.push(currentRating)
    }

    data.push({
      data: ratings.reverse(),
      borderColor: randomRGB(),
      label: ratingAndName.nickname,
    })
  }

  data = data.sort((a, b) => Number(b.data.length) - Number(a.data.length))

  const renderer = new ChartJSNodeCanvas({ width: 800, height: 300 })
  const image = await renderer.renderToBuffer({
    // Build your graph passing option you want
    type: 'line',
    options: {
      plugins: {
        legend: {
          title: {
            text: ' wtf',
          },
          labels: {

          }
        }
      },
      // backgroundColor: '#3e95cd',
      color: 'white',
      borderColor: 'white',
      scales: {
        x: {
          ticks: {
            display: false //this will remove only the label
          }
        },
        y: {
          ticks: {
            color: 'white',
            font: {
              size: 18,
            }
          },

          grid: {
            borderColor: 'white',
          }
        }
      }
    },
    data: {
      labels: data[0].data,
      datasets: data
    },
  })

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  fs.writeFile('./hmmm.png', image, 'binary', (err) => { })

  return image
}

  // generateProgressImageForUser(['168031471548760065', '108850415717584896', '315881722375831553', '166286180470620161'])
