import { ChartJSNodeCanvas } from 'chartjs-node-canvas'
import * as fs from 'fs'

import { getPugsByDay } from '../../db/dao/pug'

const randomNum = () => Math.floor(Math.random() * (235 - 52 + 1) + 52)
const randomRGB = () => `rgb(${randomNum()}, ${randomNum()}, ${randomNum()})`

const colr = randomRGB()

export const generatePugsCountGraph = async (): Promise<Buffer> => {
  const pugsByDay = await getPugsByDay()
  const data = []

  data.push({
    data: pugsByDay.map(p => p.cnt),
    // borderColor: randomRGB(),
    label: 'Pugs by day',
  })

  const renderer = new ChartJSNodeCanvas({ width: 500, height: 8*pugsByDay.length })
  const image = await renderer.renderToBuffer({
    // Build your graph passing option you want
    type: 'bar',
    options: {
      indexAxis: 'y',
      elements: {
        bar: {
          backgroundColor: colr,
          borderWidth: 1,
        }
      },
      plugins: {
        legend: {
        },
      },
      // backgroundColor: '#3e95cd',
      color: 'white',
      borderColor: 'white',
      scales: {
        x: {
          ticks: {
            color: 'white',
            font: {
              size: 12,
            }
          },
        },
        y: {
          ticks: {
            color: 'white',
            font: {
              size: 12,
            }
          },

          grid: {
            borderColor: 'white',
          }
        }
      }
    },
    data: {
      labels: pugsByDay.map(p => p.day),
      datasets: data
    },
  })

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  fs.writeFile('./hmmm.png', image, 'binary', (err) => { })

  return image
}

//  generatePugsCountGraph()
