/* eslint-disable @typescript-eslint/no-non-null-assertion */
import config from 'config'
import { getMapPreferencesForPlayers } from '../db/dao/map_preference'

import { getAllEnabledMaps, getLastPlayedMaps, getPlayerLastPlayedMaps, setMapCurrentWeight, UtMap } from '../db/dao/map'

const weightIncreaseConstant: number = config.get('maps.weightIncreaseConstant')
const mapPauseQuantity: number = config.get('maps.mapPauseRounds')
const defaultPlayerMinus: number = config.get('maps.defaultLastPlayedMinus')

export async function pickMap(maps: UtMap[], players: string[]): Promise<UtMap> {
    const t0 = performance.now()

    const dbMaps = maps.map(a => {return {...a}})
    const playerMinusScores: Map<string, number> = new Map()

    // for (const p of players) {
    //     const playerMaps = await getPlayerLastPlayedMaps(p)
    //     for (const m of playerMaps) {
    //         if (!playerMinusScores.get(m.name)) {
    //             playerMinusScores.set(m.name, defaultPlayerMinus)
    //             continue
    //         }

    //         playerMinusScores.set(m.name, playerMinusScores.get(m.name)! + defaultPlayerMinus)
    //     }
    // }

    const pausedMaps = await getLastPlayedMaps(mapPauseQuantity)
    const filteredMaps = maps.filter(map => !pausedMaps.map(m => m.name).includes(map.name))

    // const modifiedMaps = filteredMaps.map(filteredMap => {
    //     const minusScore = playerMinusScores.get(filteredMap.name)
    //     if (minusScore) {
    //         filteredMap.current_weight -= minusScore

    //         if (filteredMap.current_weight <= 0) filteredMap.current_weight = 0
    //     }

    //     return filteredMap
    // })

    const playerPreferenceMaps = await addPlayerPreferences(players.join(','), filteredMaps)

    const allOdds = playerPreferenceMaps.map(a => a.current_weight).reduce((a, b) => { return a + b })
    const random = Math.random() * (allOdds - 1)

    let chosenMap = null
    let sum = 0

    for (const map of playerPreferenceMaps) {
        sum += map.current_weight

        if (sum > random) {
            chosenMap = map
            break
        }
    }

    if (chosenMap === null) {
        throw ('couldnt choose a map? what?')
    }

    for (const map of dbMaps) {
        if (map.name === chosenMap.name) {
            await setMapCurrentWeight(map.default_weight, map.name)
        } else {
            if (map.current_weight < map.default_weight) { // fix for current wrong values, TODO remove later
                await setMapCurrentWeight((map.default_weight + weightIncreaseConstant), map.name)
            } else {
                await setMapCurrentWeight((map.current_weight + weightIncreaseConstant), map.name)
            }
        }
    }

    const t1 = performance.now()
    console.log(`Map chosen in  ${t1 - t0} milliseconds.`)
    console.log(`Map weights: ${JSON.stringify(playerPreferenceMaps)}`)
    console.log(`All odds: ${allOdds} Random: ${random} Chosen map: ${chosenMap.name}`)

    return chosenMap
}

const addPlayerPreferences = async (players: string, maps: UtMap[]): Promise<UtMap[]> => {
    for (const m of maps) {
        const preferences = await getMapPreferencesForPlayers(players, m.id)
        for (const p of preferences) {
            m.current_weight += p.score
            if (m.current_weight < 0)  m.current_weight = 0
        }
    }

    return maps
}

export async function calculateOdds(maps: UtMap[]): Promise<Map<string, number>> {
    const pausedMaps = await getLastPlayedMaps(mapPauseQuantity)

    const filteredMaps = maps.filter(map => !pausedMaps.map(m => m.name).includes(map.name))

    const allOdds = filteredMaps.map(a => a.current_weight).reduce((a, b) => { return a + b })

    const mapsWithOdds: Map<string, number> = new Map()

    for (const map of filteredMaps) {
        mapsWithOdds.set(map.name, map.current_weight / allOdds * 100)
    }

    const sortedOdds = new Map([...mapsWithOdds.entries()].sort((a, b) => b[1] - a[1]))

    return sortedOdds
}

// const simulate = async () => {
//     for (let i = 0; i < 5; i++) {
//         const maps = await getAllEnabledMaps()

//         await pickMap(maps, ['186887938121007105','354849553628921856','286192515730767874','732321181633609759','210630623021367297','208258066242732032','740623664344596540','124605651006521344','105027580880535552','202409323299733505'])
//     }
// }
