import { fetchDiscordUser, sendBanMessage, sendEmbed, sendLogMessage, userToMember } from '../../bot/bot'
import { getBanEmbed } from '../../bot/embeds/admin/ban_embed'
import { disc_banned_role_id } from '../../config'
import { insertBan } from '../../db/dao/ban'
import { getLatePlayersByPassword, getNoShowPlayersByPassword, getPlayerByPwd, getPugIdByPassword, Password } from '../../db/dao/password'
import config from 'config'

const whereToBan = 'all'

enum CrimeType {
    LATE = 'late',
    NOSHOW = 'noshow'
}

export const handleMatchStarted = async (gamepwd: string) => {
    const pugId = await getPugIdByPassword(gamepwd)
    await sendLogMessage(`[**#${pugId}**] Match started!`)

    const latePlayers = await getLatePlayersByPassword(gamepwd)
    const noShowPlayers = await getNoShowPlayersByPassword(gamepwd)

    await banPlayers(latePlayers.filter(p => !noShowPlayers.includes(p)), CrimeType.LATE)
    await banPlayers(noShowPlayers, CrimeType.NOSHOW)
}

export const banPlayers = async (latePlayers: Password[], crimeType: CrimeType) => {
    const reason = crimeType === CrimeType.LATE ? <string>config.get('pug.lateBanReason') : <string>config.get('pug.noshowBanReason')
    const duration = crimeType === CrimeType.LATE ? <number>config.get('pug.lateBanTime') : <number>config.get('pug.noshowBanTime')

    const bannedTill = new Date()
    
    bannedTill.setHours(bannedTill.getHours() + duration)
    const endTime = bannedTill.toString()
    
    for (const p of latePlayers) {
        try {
            const player = await getPlayerByPwd(p.password)
            const user = await fetchDiscordUser(player.discord_id)
            
            if (user) {
                if (crimeType == CrimeType.LATE) {
                    await sendLogMessage('Banning user **' + user.username + '** for being late')
                } else if (crimeType == CrimeType.NOSHOW) {
                    await sendLogMessage('Banning user **' + user.username + '** for no show')
                }
                
                const member = await userToMember(user)
                member.roles.add(disc_banned_role_id)
                await insertBan({ discord_id: player.discord_id, reason, duration, end_time: endTime, admin_id: '69', admin_name: 'c00lbot2' })
                await sendBanMessage(`.ban discord_id:${player.discord_id} reason:${reason} duration:${duration}`)
            
                const banEmbed = await getBanEmbed(player.nickname, reason, duration, 'c00lbot2', endTime, whereToBan)
                await sendEmbed(banEmbed)
            }
        } catch (e) {
            console.log(e)
        }
    }
}