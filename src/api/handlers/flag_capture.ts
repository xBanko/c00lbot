/* eslint-disable @typescript-eslint/no-non-null-assertion */

import { getPlayerByPwd, getScoreIdByPassword } from "../../db/dao/password"
import { ScoreStatus } from "../../bot/embeds/utils/constants"
import { updateScore } from "../../db/dao/score"
import { sendEmbed } from "../../bot/bot"
import { Capture, ServerPlayer } from "../router"
import { getFlagCaptureEmbed } from "../../bot/embeds/user/flag_capture_embed"

export const handleFlagCapture = async (cap: Capture) => {
    const scoreId = await getScoreIdByPassword(cap.GamePassword)
    await updateScore(cap.Teams.Red.Score, cap.Teams.Blue.Score, scoreId, ScoreStatus.Live)
    
    if (cap.InstigatorId !== undefined) {
        const player: ServerPlayer = cap.Players.find(p => p.Id === cap.InstigatorId)!
        const dbPlayer = await getPlayerByPwd(player.Password)

        if (dbPlayer) {
            console.log(`${dbPlayer.nickname} captured for team ${player.Team === 0 ? 'Red' : 'Blue'}! Current score: ${cap.Teams.Red.Score} - ${cap.Teams.Blue.Score}`)
        
            const embed = await getFlagCaptureEmbed(dbPlayer.nickname, player.Team === 0 ? 'RED' : 'BLUE', cap.Teams.Red.Score, cap.Teams.Blue.Score, cap.RemainingTime, cap.Map)
            await sendEmbed(embed)
        }
    }
}

