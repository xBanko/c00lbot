import { getLatePlayersByPassword, getPugIdByPassword, setAllPlayersLateByPwd, setPlayerLate } from '../../db/dao/password'
import { sendLogMessage } from '../../bot/bot'
import { ServerPlayer } from '../router'
import config from 'config'
import { getPlayersByDiscordId } from '../../db/dao/player'

const lateTime =  <number>config.get('pug.lateTime')
export const handleWaitingPlayers = async (players: ServerPlayer[], password: string, time: number) => {
    if (time < 20) {
        const pugId = await getPugIdByPassword(password)
        await sendLogMessage(`[**#${pugId}**] Server set up, ban timer started`)
    } else if (Math.abs(time - lateTime) < 15) {
        const nonReadyPlayers = players.filter(p => !p.Ready)

        await Promise.all(nonReadyPlayers.map(async p => {
            await setPlayerLate(p.Password)
        }))

        const pugId = await getPugIdByPassword(password)
        const latePlayers = await (await getLatePlayersByPassword(password)).map(p => p.player_id)
        if (latePlayers.length > 0) {
            const latePlayersNicks = await getPlayersByDiscordId(latePlayers.join(','))

            await sendLogMessage(`[**#${pugId}**] @here Grace period expired, these players are late: ${latePlayersNicks.map(p => p.nickname).join(', ')}`)
        }
    }
}