import { getNoShowPlayersByPassword, getPlayersByPassword, getPugIdByPassword, setPlayerNoShow } from '../../db/dao/password'
import { ServerPlayer } from '../router'
import { getPlayersByDiscordId } from '../../db/dao/player'
import { sendLogMessage } from '../../bot/bot'


export const handleWaitingPlayersEnd = async (players: ServerPlayer[], password: string, ) => {
    const allPlayers = await getPlayersByPassword(password)
    const noshowPlayers = allPlayers.filter(p => {
        const player = players.find(player => player.Password === p.password)
        if (player && player.Ready) return false

        return true
    })

    await Promise.all(noshowPlayers.map(async p => {
        await setPlayerNoShow(p.password)
    }))

    const pugId = await getPugIdByPassword(password)
    const noShows = await (await getNoShowPlayersByPassword(password)).map(p => p.player_id)
    if (noShows.length > 0) {
        const noshowerNicks = await getPlayersByDiscordId(noShows.join(','))

        await sendLogMessage(`[**#${pugId}**] @here No show period expired, these players are no shows: ${noshowerNicks.map(p => p.nickname).join(', ')}`)
    }

}