/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */
import express from 'express'
import { getServerByIp } from '../db/dao/server'
import { handleFlagCapture } from './handlers/flag_capture'
import { handleMatchEnded } from './handlers/match_ended'
import { handleMatchStarted } from './handlers/match_started'
import { handleWaitingPlayers } from './handlers/waiting_players'
import { handleWaitingPlayersEnd } from './handlers/waiting_players_end'

export const app = express()
app.use(express.json())
app.set('trust proxy', true)

export enum GameEvents  {
  WaitingPlayers = 'WAITINGPLAYERS', // WaitingPlayers 	When map is loaded and every WaitingPlayersIntervalInSecs Interval.
  WaitingPlayersEnd = 'WAITINGPLAYERSEND', // WaitingPlayersEnd 	After configuration(WaitingPlayersIntervalInSecsExpired) has reached.
  MatchStarted = 'MATCHSTARTED', // MatchStarted 	Players started playing.
  FlagCapture = 'FLAGCAPTURE', // FlagCapture 	Flag has been captured.
  MatchEnded = 'MATCHENDED' // MatchEnded 	Match ended.
}

export interface ServerPlayer {
  Name: string
  Index: number
  Id: number
  Ready: boolean
  Password: string
  Team: number 
}

export interface ScoreGame {
  Teams: {
    Red: {
      Score: number
    },
    Blue: {
      Score: number
    }
  }
  Players: ServerPlayer[]
  GamePassword: string
}

export interface Capture extends ScoreGame {
  InstigatorId: number
  RemainingTime: number
  Map: string
}

app.post('/', async function (req, res) {
  const ip = req.ip.split(':')[3]

  try {
    const server = await getServerByIp(ip)

    if (!server || !server.busy || req.body.GamePassword === 'ranked') {
      console.log(`Ignoring request: ${req.body.Name} event from IP ${ip} with game password ${req.body.GamePassword} (no matching server found)`)
      res.send()
      return
    }

    switch (req.body.Name) {
      case GameEvents.WaitingPlayers:
        console.debug(`${GameEvents.WaitingPlayers}: ${ip} (${req.body.TimeSeconds} elapsed)`)
        await handleWaitingPlayers(req.body.Players, req.body.GamePassword, req.body.TimeSeconds)
        break
      case GameEvents.WaitingPlayersEnd:
        console.debug(`${GameEvents.WaitingPlayersEnd}: ${ip}`)
        await handleWaitingPlayersEnd(req.body.Players, req.body.GamePassword)
        break
      case GameEvents.FlagCapture:
        console.debug(`${GameEvents.FlagCapture}: ${ip}`)
        await handleFlagCapture(req.body)
        break
      case GameEvents.MatchStarted:
        console.debug(`${GameEvents.MatchStarted}: ${ip}`)
        await handleMatchStarted(req.body.GamePassword)
        break
      case GameEvents.MatchEnded:
        console.debug(`${GameEvents.MatchEnded}: ${ip}`)
        await handleMatchEnded(req.body, ip)
        break
      default:
        console.debug('Unknown event received: ' + req.body.Name)
    }
  } catch (e) {
    console.log(e)
  }

  res.send()
})
