import sqlite3 from 'better-sqlite3'
import config from 'config'

import { MapStatus, ScoreStatus } from '../../bot/embeds/utils/constants'

const db = sqlite3(config.get('dbConfig.dbName'))

interface MapName {
    name: string
}

interface MapFound extends MapName {
    found: boolean
}
interface MapDeleted extends MapFound {
    deleted: boolean
}

interface MapStatusWeight extends MapName {
    default_weight: number
    status?: string
}

export interface UtMap extends MapStatusWeight {
    id: string
    current_weight: number
}

interface UtMapStats {
    id: string
    name: string
    count: number
    diff: number
    caps: number
}


//gets
export const getAllMaps = async (): Promise<UtMap[]> => {
    return db.prepare(`SELECT * from maps ORDER BY name`).all()
}

export const getAllEnabledMaps = async (): Promise<UtMap[]> => {
    return db.prepare(`SELECT * from maps WHERE status = '${MapStatus.Enabled}' ORDER BY name`).all()
}

export const getMap = async (map_id: string): Promise<UtMap> => {
    return db.prepare(`SELECT * from maps where id = ?`).get(map_id)
}

export const getCurrentTop3Weights = async (): Promise<UtMap[]> => {
    return db.prepare(`SELECT * from maps where status = '${MapStatus.Enabled}' order by current_weight desc limit 3`).all()
}

export const getAllMapStatsWithCount = async (date: string): Promise<UtMapStats[]> => {
    return db.prepare(`SELECT count(p.map_id) count, m.name, round(avg(abs(s.score_blue - s.score_red)), 2) diff, round(avg(abs(s.score_blue + s.score_red)), 2) caps
        FROM all_scores s
        JOIN all_pugs p ON p.id = s.pug_id
        JOIN maps m ON m.id = p.map_id
        WHERE NOT s.status = '${ScoreStatus.Rejected}' AND s.score_blue NOT NULL AND p.finished >= date(?)
        GROUP BY m.name
        ORDER BY count DESC`).all(date)
}

export const getAllTimeMapStatsWithCount = async (): Promise<UtMapStats[]> => {
    return db.prepare(`SELECT count(p.map_id) count, m.name, round(avg(abs(s.score_blue - s.score_red)), 2) diff, round(avg(abs(s.score_blue + s.score_red)), 2) caps
        FROM all_scores s
        JOIN all_pugs p ON p.id = s.pug_id
        JOIN maps m ON m.id = p.map_id
        WHERE NOT s.status = '${ScoreStatus.Rejected}' AND s.score_blue NOT NULL
        GROUP BY m.name
        ORDER BY count DESC`).all()
}

export const getSeasonMapStatsWithCount = async (): Promise<UtMapStats[]> => {
    return db.prepare(`SELECT count(p.map_id) count, m.name, round(avg(abs(s.score_blue - s.score_red)), 2) diff, round(avg(abs(s.score_blue + s.score_red)), 2) caps
        FROM scores s
        JOIN pugs p ON p.id = s.pug_id
        JOIN maps m ON m.id = p.map_id
        WHERE NOT s.status = '${ScoreStatus.Rejected}' AND s.score_blue NOT NULL
        GROUP BY m.name
        ORDER BY count DESC`).all()
}

export const getLastPlayedMaps = async (limit = 3): Promise<UtMap[]> => {
    return db.prepare(`SELECT m.* from pugs p
        join maps m on p.map_id = m.id
        join scores s on s.pug_id = p.id
        where s.status is not '${ScoreStatus.Rejected}' order by p.started DESC limit ?`).all(limit)
}

export const getPlayerLastPlayedMaps = async (playerId: string, limit = 3): Promise<UtMap[]> => {
    return db.prepare(`SELECT m.* from pugs p
        join maps m on p.map_id = m.id
        join scores s on s.pug_id = p.id
        where s.status is not '${ScoreStatus.Rejected}'
        and p.players like '%${playerId}%'
        order by p.started DESC limit ?`).all(limit)
}

export const getSeasonPlayerMapStats = async (playerDiscordID: string): Promise<UtMapStats[]> => {
    return db.prepare(`SELECT count(p.map_id) count, m.name, round(avg(abs(s.score_blue - s.score_red)), 2) diff, round(avg(abs(s.score_blue + s.score_red)), 2) caps
        FROM scores s
        JOIN pugs p ON p.id = s.pug_id
        JOIN maps m ON m.id = p.map_id
        WHERE NOT s.status = '${ScoreStatus.Rejected}' AND s.score_blue NOT NULL AND p.players like '%${playerDiscordID}%'
        GROUP BY m.name
        ORDER BY count DESC`).all()
}

export const getAllPlayerMapStats = async (playerDiscordID: string): Promise<UtMapStats[]> => {
    return db.prepare(`SELECT count(p.map_id) count, m.name, round(avg(abs(s.score_blue - s.score_red)), 2) diff, round(avg(abs(s.score_blue + s.score_red)), 2) caps
        FROM all_scores s
        JOIN all_pugs p ON p.id = s.pug_id
        JOIN maps m ON m.id = p.map_id
        WHERE NOT s.status = '${ScoreStatus.Rejected}' AND s.score_blue NOT NULL AND p.players like '%${playerDiscordID}%'
        GROUP BY m.name
        ORDER BY count DESC`).all()
}

//updates
export const setMapCurrentWeight = async (new_weight: number, name: string): Promise<void> => {
    await db.prepare(`UPDATE maps SET current_weight = ? where name = ? AND status = '${MapStatus.Enabled}'`).run(new_weight, name)
}

export const bumpMapWeight = async (weight: number, name: string): Promise<void> => {
    await db.prepare(`UPDATE maps SET current_weight = current_weight + ? where name = ? AND status = '${MapStatus.Enabled}'`).run(weight, name)
}

export const setMapDefaultWeight = async (new_weight: number, name: string): Promise<MapFound> => {
    const mapName = await db.prepare(`SELECT name FROM maps WHERE name = ?`).get(name)
    if (mapName === undefined) return { name, found: false }
    await db.prepare(`UPDATE maps SET current_weight = ?, default_weight = ? WHERE name = ?`).run(new_weight, new_weight, name)
    return { name, found: true }
}

export const setMapStatus = async (status: string, name: string): Promise<MapFound> => {
    const mapName = await db.prepare(`SELECT name FROM maps WHERE name = ?`).get(name)
    if (mapName === undefined) return { name, found: false }
    await db.prepare(`UPDATE maps SET status = ? WHERE name = ?`).run(status, name)
    return { name, found: true }
}

export const resetMapCurrentWeight = async (): Promise<void> => {
    await db.prepare(`UPDATE maps SET current_weight = (default_weight)`).run()
}

//inserts
export const insertMap = async (map: MapStatusWeight): Promise<void> => {
    await db.prepare(`INSERT INTO maps (name, default_weight, current_weight, status) VALUES (?, ?, ?, ?)`).run(map.name, map.default_weight, map.default_weight, map.status)
}

//deletes
export const removeMap = async (name: string): Promise<MapDeleted> => {
    try {
        const mapName = await db.prepare(`SELECT name FROM maps WHERE name = ?`).get(name)
        if (mapName === undefined) return { name, found: false, deleted: false }
        await db.prepare(`DELETE FROM maps WHERE name = ?`).run(name)
        return { name, found: true, deleted: true }
    } catch (error) {
        console.log(`Deletion of map ${name} failed`)
        return { name, found: true, deleted: false }
    }
}
