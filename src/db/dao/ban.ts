import sqlite3 from 'better-sqlite3'
import config from 'config'

const db = sqlite3(config.get('dbConfig.dbName'))

export interface Ban {
    id?: string
    discord_id: string
    reason: string
    duration: number
    end_time: string
    admin_id: string
    admin_name: string
    status?: string
}

interface ActiveBan extends Ban {
    nickname: string,
}

//gets
export const getAllCurrentBans = async (): Promise<ActiveBan[]> => {
    const bans = await db.prepare(`SELECT b.id, b.discord_id, b.reason, b.duration, b.end_time, b.admin_name, p.nickname FROM bans b JOIN players p ON p.discord_id = b.discord_id WHERE status IS NULL`).all()
    return bans
}

export const getAllCurrentBansForUser = async (playerId: string): Promise<Ban[]> => {
    const bans = await db.prepare(`SELECT * FROM bans WHERE discord_id is ? AND status IS NULL`).all(playerId)
    return bans
}

export const getAllBansForUser = async (playerId: string): Promise<Ban[]> => {
    const bans = await db.prepare(`SELECT * FROM bans WHERE discord_id is ?`).all(playerId)
    return bans
}

export const getLastBanForUser = async (playerId: string): Promise<Ban> => {
    const bans = await db.prepare(`SELECT * FROM bans WHERE discord_id is ? ORDER BY end_time desc`).get(playerId)
    return bans
}

//updates
export const updateBanStatus = async (banId: string, status: string) => {
    await db.prepare(`UPDATE bans set status = ? where id = ? `).run(status, banId)
}

export const updateEndDate = async (banId: string, endDate: string) => {
    await db.prepare(`UPDATE bans set end_time = ? where id = ? `).run(endDate, banId)
}

//inserts
export const insertBan = async (ban: Ban): Promise<void> => {
    await db.prepare(`INSERT INTO bans (discord_id, reason, duration, end_time, admin_id, admin_name) VALUES (?, ?, ?, ?, ?, ?)`).run(ban.discord_id, ban.reason, ban.duration, ban.end_time, ban.admin_id, ban.admin_name)
}
