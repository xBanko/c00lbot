import sqlite3 from 'better-sqlite3'
import config from 'config'

const db = sqlite3(config.get('dbConfig.dbName'))

export interface ServerPreference {
    server_shortname: string
    player_id: string
    score: -1 | 0 | 1
}

export const getPreferencesForPlayers = async (playerIds: string): Promise<ServerPreference[]> => {
    return await db.prepare(`SELECT * from server_preferences where player_id in (${playerIds}) order by server_shortname asc`).all()
}

export const insertServerPreference = async (playerId: string, serverShortname: string, score: number): Promise<void> => {
    await db.prepare(`INSERT into server_preferences (player_id, server_shortname, score) values (?, ?, ?)`).run(playerId, serverShortname, score)
}
