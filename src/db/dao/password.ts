import sqlite3 from 'better-sqlite3'
import config from 'config'
// import { ScoreStatus } from '../../bot/embeds/utils/constants'
import { Player } from './player'

const db = sqlite3(config.get('dbConfig.dbName'))

export interface Password extends InsertPassword {
    id: number
    active: boolean
    late: boolean
    noshow: boolean
}

export interface InsertPassword {
    pug_id: string
    player_id: string
    team: string
    password: string
}


export const insertPassword = async (pwd: InsertPassword): Promise<void> => {
    await db.prepare(`INSERT INTO passwords (pug_id, player_id, team, password) VALUES (@pug_id, @player_id, @team, @password)`).run(pwd)
}

export const getPaswordByPwd = async (pwd: string): Promise<Password> => {
    return await db.prepare(`select pwd.* from players ply join passwords pwd on pwd.player_id = ply.discord_id where pwd.password = ? and pwd.active = 1`).get(pwd)
}

export const getPlayerByPwd = async (pwd: string): Promise<Player> => {
    return await db.prepare(`select ply.* from players ply join passwords pwd on pwd.player_id = ply.discord_id where pwd.password = ? and pwd.active = 1`).get(pwd)
}

export const setAllPlayersLateByPwd = async (gamepwd: string): Promise<void> => {
    const pugId = await getPugIdByPassword(gamepwd)
    await setPlayerLateByPugId(pugId)
}

export const setPlayerLateByPugId = async (pugId: string): Promise<void> => {
    await db.prepare(`update passwords set late = 1 where pug_id = ?`).run(parseInt(pugId))
}

export const setPasswordsNotActiveForPug = async (pugId: number) => { 
    try {
        await db.prepare(`update passwords set active = 0 where pug_id = ?`).run(pugId)
    } catch (e) {
        console.log(e)
    }
}

export const setPlayerNonLate = async (pwd: string): Promise<void> => {
    try {
        const password = (await getPaswordByPwd(pwd)).id
        await db.prepare(`update passwords set late = 0 where id = ?`).run(password)
    } catch (e) {
        console.log(e)
    }
}

export const setPlayerLate = async (pwd: string): Promise<void> => {
    try {
        const password = (await getPaswordByPwd(pwd)).id
        await db.prepare(`update passwords set late = 1 where id = ?`).run(password)
    } catch (e) {
        console.log(e)
    }
}

export const setPlayerNoShow = async (pwd: string): Promise<void> => {
    try { 
        const password = (await getPaswordByPwd(pwd)).id
        await db.prepare(`update passwords set noshow = 1 where id = ?`).run(password)
    } catch (e) {
        console.log(e)
    }
}

export const getLatePlayersByPassword = async (gamepwd: string): Promise<Password[]> => {
    const pugId = await getPugIdByPassword(gamepwd)
    return await db.prepare(`select * from passwords where pug_id = ? and late = 1 and noshow = 0`).all(parseInt(pugId))
} 

export const getNoShowPlayersByPassword = async (gamepwd: string): Promise<Password[]> => {
    const pugId = await getPugIdByPassword(gamepwd)
    return await db.prepare(`select * from passwords where pug_id = ? and noshow = 1`).all(parseInt(pugId))
} 

export const getPlayersByPassword = async (gamepwd: string): Promise<Password[]> => {
    const pugId = await getPugIdByPassword(gamepwd)
    return await db.prepare(`select * from passwords where pug_id = ?`).all(parseInt(pugId))
} 

export const getLatePlayersByPugId = async (pugId: string): Promise<Password[]> => {
    return await db.prepare(`select * from passwords where pug_id = ? and late = 1`).all(parseInt(pugId))
}

export const getPugIdByPassword = async (gamepwd: string): Promise<string> => {
    const pugId = (await db.prepare(`select sco.pug_id from scores sco 
    join pugs p on sco.pug_id = p.id
    join servers serv on p.server_id = serv.id 
    where serv.password = '${gamepwd}'
    and sco.status = 'LIVE'`).get()).pug_id

    return pugId
} 

export const getScoreIdByPassword = async (gamepwd: string): Promise<string> => {
    const pugId = (await db.prepare(`select sco.id from scores sco 
    join pugs p on sco.pug_id = p.id
    join servers serv on p.server_id = serv.id 
    where serv.password = '${gamepwd}'
    and sco.status = 'LIVE'`).get()).id

    return pugId
} 