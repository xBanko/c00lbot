import sqlite3 from 'better-sqlite3'
import config from 'config'
import { BetOdds, BetOption, BetResult } from '../../pug/bet/bet'
import { ScoreStatus } from '../../bot/embeds/utils/constants'

const db = sqlite3(config.get('dbConfig.dbName'))

interface TopBetPlayer {
    player_id: string
    coins?: number
    nickname: string
}
interface PlayerBet extends TopBetPlayer {
    amount: number
    bet_on: string
    net: number
    created: string
    pug_id?: string
}

interface BetStats {
    total: number
    amount_total: number
    red_count: number
    blue_count: number
    draw_count: number
    sum_net: number
    avg_net: number
    avg_amount: number
    avg_odds: number
    win_bets: number
}

export interface Bet {
    id: number
    pug_id: number
    player_id: string
    amount: number
    bet_on: BetOption
    net: number
    created: Date
    realised: Date
    odds: number
}

interface Rank {
    rank: number
}

interface Coins {
    coins: number
}

interface PlayerBetInfo {
    nickname: string
    coins: number
}

interface Net {
    net: number
}

// gets
export const getPlayerBetInfo = async (playerId: string): Promise<PlayerBetInfo> => {
    const playerInfo = await db.prepare(`SELECT nickname, coins from players where discord_id = ?`).get(playerId)

    return playerInfo
}

export const doesPlayerHaveMoneyz = async (playerId: string, amount: number): Promise<boolean> => {
    const currentBet = await db.prepare(`SELECT coins from players where discord_id = ?`).get(playerId)

    if (currentBet.coins < amount) return false

    return true
}

export const checkPugBettingEligibility = async (pugId: number): Promise<boolean> => {
    const eligiblePug = await db.prepare(`SELECT * FROM pugs p join scores s on s.pug_id = p.id
            WHERE p.id = ? AND p.started is not null AND p.finished is null
            AND CURRENT_TIMESTAMP < DATETIME(p.started, '+5 minutes')
            AND (s.status = '${ScoreStatus.Live}' OR s.status = '${ScoreStatus.Manual}')
            AND s.score_red is null
            and s.score_blue is null`).get(pugId)

    return eligiblePug ? true : false
}

export const getTopBetPlayers = async (limit: number): Promise<TopBetPlayer[]> => {
    return await db.prepare(`SELECT DISTINCT p.nickname, p.coins, b.player_id
    FROM players p INNER JOIN bets b on p.discord_id = b.player_id
    WHERE b.bet_on NOT NULL AND realised IS NOT NULL GROUP BY p.nickname ORDER BY p.coins desc limit ${limit},10`).all()
}

export const getBetsForPug = async (pugId: number): Promise<PlayerBet[]> => {
    return await db.prepare(`SELECT b.amount, b.bet_on, b.net, p.coins, p.nickname, b.created
        FROM bets b
        JOIN players p on p.discord_id = b.player_id
        WHERE b.pug_id = ?`).all(pugId)
}

export const isFirstBet = async (userId: string, pugId: number): Promise<boolean> => {
    return await db.prepare(`SELECT * FROM bets
        WHERE pug_id = ? and player_id = ?`).get(pugId, userId) ? false : true
}

export const getPlayerBetStats = async (playerId: string): Promise<BetStats> => {
    return await db.prepare(`
    SELECT
        Count(id) total,
        Sum(amount) amount_total,
        Count(CASE WHEN bet_on = '${BetOption.Red}' THEN 1 ELSE NULL END) AS red_count,
        Count(CASE WHEN bet_on = '${BetOption.Blue}' THEN 1 ELSE NULL END) AS blue_count,
        Count(CASE WHEN bet_on = '${BetOption.Draw}' THEN 1 ELSE NULL END) AS draw_count,
        Sum(net) sum_net,
		round(avg(net)) avg_net,
		round(avg(amount)) avg_amount,
        100/avg(odds) avg_odds,
		count(CASE WHEN net > 0 THEN 1 ELSE NULL END) as win_bets
    FROM bets
    WHERE player_id = ? AND realised IS NOT NULL`).get(playerId)
}

export const getPlayerLastBet = async (playerId: string): Promise<PlayerBet> => {
    return await db.prepare(`SELECT b.amount, b.bet_on, b.net, p.nickname, b.created, b.pug_id
        FROM bets b
        JOIN players p on p.discord_id = b.player_id
        WHERE b.player_id = ?
        ORDER BY b.created DESC
        LIMIT 1`).get(playerId)
}

export const selectUnfinishedBetsForPug = async (pugId: number | string, whoWon: BetResult): Promise<Bet[]> => {
    let oddsColumn = ''

    switch (whoWon) {
        case 'RED':
            oddsColumn = 'red_odds'
            break
        case 'BLUE': 
            oddsColumn = 'blue_odds'
            break
        default:
            oddsColumn = 'draw_odds'
    }

    return await db.prepare(`select b.*, o.${oddsColumn} as odds from bets b join pug_odds o on b.pug_id = o.pug_id where b.pug_id = ? and realised is null and net is null`).all(pugId)
}

export const getPlayerBetRank = async (discord_id: string): Promise<Rank> => {
    const rank = (await db.prepare(`SELECT count(DISTINCT(b.player_id))+1 as rank from bets b join players p on b.player_id = p.discord_id where b.realised NOT NULL AND p.coins > (select coins from players where discord_id = ?)`).get(discord_id))
    return rank
}

export const getPlayerCoins = async (discord_id: string): Promise<Coins> => {
    const coins = (await db.prepare(`SELECT coins FROM players WHERE discord_id = ? `).get(discord_id))
    return coins
}

export const getPlayerLastNet = async (playerId: string): Promise<Net> => {
    return await db.prepare(`SELECT SUM(NET), * FROM BETS WHERE pug_id = (SELECT MAX(pug_id) FROM BETS WHERE player_id = ?)`).get(playerId)
}

// inserts
export const insertBet = async (playerId: string, pugId: number, amount: number, option: BetOption): Promise<void> => {
    await db.prepare(`insert into bets (pug_id, amount, bet_on, player_id) values (?,?,?,?)`).run(pugId, amount, option, playerId)
}


export const insertOdds = async (pugId: string | number, blue_odds: number, red_odds: number, draw_odds: number): Promise<void> => {
    await db.prepare(`insert into pug_odds (pug_id, blue_odds, red_odds, draw_odds) values (?,?,?,?)`).run(pugId, blue_odds, red_odds, draw_odds)
}

// updates
export const removeCoinsFromPlayer = async (playerId: string, amount: number): Promise<void> => {
    await db.prepare(`update players set coins = coins - ? where discord_id = ?`).run(Math.round(amount), playerId)
}

export const addCoinsToPlayer = async (playerId: string, amount: number): Promise<void> => {
    await db.prepare(`update players set coins = coins + ? where discord_id = ?`).run(Math.round(amount), playerId)
}

export const finishBet = async (id: number, amount: number): Promise<void> => {
    await db.prepare(`update bets set net = ?, realised = CURRENT_TIMESTAMP where id = ?`).run(Math.round(amount), id)
}

export const calculateBettingOdds = async (pugId: number | string): Promise<BetOdds> => {
    const str = await db.prepare(`select str_red, str_blue, map_id from all_pugs where id = ?`).get(pugId)
    const drawsTotal = await db.prepare(`select count(*) as cnt from all_scores s join all_pugs p on s.pug_id = p.id where p.map_id = ? and s.score_blue = s.score_red and s.status is ?`).get(str.map_id, ScoreStatus.Accepted)
    const nondrawsTotal = await db.prepare(`select count(*) as cnt from all_scores s join all_pugs p on s.pug_id = p.id where p.map_id = ? and s.score_blue != s.score_red and s.status is ?`).get(str.map_id, ScoreStatus.Accepted)

    if (drawsTotal.cnt + nondrawsTotal.cnt < 10 || drawsTotal.cnt === 0 || nondrawsTotal === 0) {
        return {
            red: 42,
            blue: 42,
            draw: 16,
        }
    }
    
    const drawPrecentage = +(Math.round(drawsTotal.cnt / (drawsTotal.cnt + nondrawsTotal.cnt) * 1000) / 10)
    const redOdds = (+(Math.round(str.str_red / (str.str_red + str.str_blue) * 1000) / 10).toFixed(1)) - drawPrecentage / 2
    const blueOdds = (100 - redOdds) - drawPrecentage
    const drawOdds = drawPrecentage

    console.log(drawPrecentage, redOdds, blueOdds, drawOdds)

    return {
        red: redOdds,
        blue: blueOdds,
        draw: drawOdds,
    }
}
