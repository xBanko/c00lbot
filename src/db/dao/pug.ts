/* eslint-disable @typescript-eslint/no-non-null-assertion */
import sqlite3 from 'better-sqlite3'
import config from 'config'

import { ScoreStatus } from '../../bot/embeds/utils/constants'
import { Server } from './server'

const db = sqlite3(config.get('dbConfig.dbName'))

export interface LivePug {
    id?: string
    players: string[]
    teams: string[]
    mode_id: number
    created: Date
    started?: Date
    finished?: Date
    score_red?: number
    score_blue?: number
    rating_change_red?: number
    rating_change_blue?: number
    str_red?: number
    str_blue?: number
    score_id: string
    map_id?: string
}

export interface LastPug {
    id: string
    team_red: string[]
    team_blue: string[]
    mode_id: number
    created: Date
    started: Date
    finished: string
    score_red: number
    score_blue: number
    name: string
    str_red: number
    str_blue: number
    map_id?: string
    status?: string
}

interface LasttPug {
    id: string
    team_red: string[]
    team_blue: string[]
    mode_id: number
    created: Date
    started: Date
    finished: string
    score_red: number
    score_blue: number
    name: string
    str_red: number
    str_blue: number
    map_id?: string
}

export interface HasId {
    id: number
}

export interface InsertedScore {
    score_red: number
    score_blue: number
    score_msg_id: string
}

export interface LivePugDto extends Omit<LivePug, 'players'> {
    players: string
    team_red: string[]
    team_blue: string[]
}

interface LastPugDto extends Omit<LastPug, 'team_red' | 'team_blue'> {
    team_red: string
    team_blue: string
}

interface LasttPugDto extends Omit<LasttPug, 'team_red' | 'team_blue'> {
    team_red: string
    team_blue: string
}

export interface LivePugFinishedDto extends Omit<LivePug, 'players'> {
    id: string
    players: string
    started: Date
    team_red: string
    team_blue: string
    finished: Date
    server_id: string
}

export interface LivePugScoredDto extends LivePugFinishedDto {
    score_red: number
    score_blue: number
    str_red: number
    str_blue: number
}

export interface LivePugFinished extends Omit<LivePugFinishedDto, 'players' | 'team_red' | 'team_blue'> {
    players: string[]
    team_red: string[]
    team_blue: string[]
    server_id: string
}

export interface LivePugScored extends LivePugFinished {
    score_red: number
    score_blue: number
    str_red: number
    str_blue: number
    score_msg_id: string
}

export interface PugScoredWithMap extends LivePugScored {
    map_name: string
}

export interface MapLastPlayed {
    finished: string
}

export interface PugStats {
    count: number
    diff: number
    caps: number
}

export interface TodaysPugs {
    id: string
    finished: string
    str_red: number
    str_blue: number
    map_name: string
    score_red: number
    score_blue: number
}

interface PugsCount {
    day: Date
    cnt: number
}

//gets
export const getCurrentPug = async (mode_id: number): Promise<LivePug> => {
    let livePug: LivePugDto = await db.prepare(`SELECT
    id, players, team_red, team_blue,
    datetime(created, 'localtime') created,
    datetime(started, 'localtime') started,
    datetime(finished, 'localtime') finished,
    str_red, str_blue, rating_change_red, rating_change_blue,
    mode_id, map_id, score_id, server_id from pugs where mode_id = ? and started is null order by created desc`).get(mode_id)

    if (!livePug) {
        livePug = await insertNewEmptyPug(mode_id)
    }

    return { ...livePug, players: livePug.players ? livePug.players.split(',') : [] }
}

export const getLastPug = async (mode_id: number): Promise<LastPug | null> => {
    const lastPug: LastPugDto | undefined = await db.prepare(`SELECT pug.id, pug.team_red, pug.team_blue, datetime(pug.created, 'localtime') created, datetime(pug.started, 'localtime') started,
    datetime(pug.finished, 'localtime') finished, pug.str_red, pug.str_blue, pug.mode_id, map.name, score.score_red, score.score_blue
    FROM all_pugs pug
    JOIN maps map ON map.id = pug.map_id
    JOIN all_scores score on score.pug_id = pug.id
    WHERE pug.mode_id = ? AND pug.started is not null and score.status is not '${ScoreStatus.Rejected}' ORDER BY pug.created DESC`).get(mode_id)

    if (!lastPug) return null

    return { ...lastPug, team_red: lastPug.team_red ? lastPug.team_red.split(',') : [], team_blue: lastPug.team_blue ? lastPug.team_blue.split(',') : [] }
}

export const getLasttPug = async (mode_id: number): Promise<LasttPug[]> => {
    const lasttPug: LasttPugDto[] = await db.prepare(`SELECT pug.id, pug.team_red, pug.team_blue, datetime(pug.created, 'localtime') created, datetime(pug.started, 'localtime') started,
    datetime(pug.finished, 'localtime') finished, pug.str_red, pug.str_blue, pug.mode_id, map.name, score.score_red, score.score_blue
    FROM all_pugs pug
    JOIN maps map ON map.id = pug.map_id
    JOIN all_scores score on score.pug_id = pug.id
    WHERE pug.mode_id = ? AND pug.started is not null and score.status is not '${ScoreStatus.Rejected}'
    ORDER BY pug.created DESC LIMIT 11`).all(mode_id)

    const lasttPugs: LasttPug[] = []
    await Promise.all(lasttPug.map(async (pug: LasttPugDto) => {
        lasttPugs.push({
            ...pug,
            team_red: pug.team_red.split(','),
            team_blue: pug.team_blue.split(','),
        })
    }))
    return lasttPugs
}

export const getSpecificLastPug = async (mode_id: number, pug_id: string): Promise<LastPug> => {
    const lastPug: LastPugDto = await db.prepare(`SELECT
    pug.id, pug.team_red, pug.team_blue, datetime(pug.created, 'localtime') created, datetime(pug.started, 'localtime') started,
    datetime(pug.finished, 'localtime') finished, pug.str_red, pug.str_blue, pug.mode_id, map.name, score.score_red, score.score_blue, score.status
    FROM all_pugs pug
    JOIN maps map ON map.id = pug.map_id
    JOIN all_scores score on score.pug_id = pug.id
    WHERE pug.mode_id = ? AND pug.id = ? AND pug.started is not null`).get(mode_id, pug_id)
    if (lastPug) {
        return { ...lastPug, team_red: lastPug.team_red ? lastPug.team_red.split(',') : [], team_blue: lastPug.team_blue ? lastPug.team_blue.split(',') : [] }
    } else {
        return lastPug
    }
}

export const getPugById = async (pug_id: number): Promise<LivePugScoredDto> => {
    return await db.prepare(`SELECT * from pugs where id = ?`).get(pug_id)
}

export const getLivePugById = async (pug_id: number): Promise<LivePugScoredDto> => {
    return await db.prepare(`SELECT * from pugs p join scores s
    on s.pug_id = p.id where p.id = ? and p.started is not null and p.rating_change_blue is null and p.finished is null and s.status in ('PENDING', 'LIVE', 'MANUAL') `).get(pug_id)
}

export const getLastPlayerPug = async (playerDiscordID: string): Promise<LivePugFinishedDto[]> => {
    const pugs: LivePugFinishedDto[] = await db.prepare(`SELECT id, players, team_red, team_blue,
    datetime(created, 'localtime') created,
    datetime(started, 'localtime') started,
    datetime(finished, 'localtime') finished,
    str_red, str_blue, rating_change_red, rating_change_blue,
    mode_id, map_id, score_id, server_id from pugs where rating_change_blue is not null and players like '%${playerDiscordID}%' order by finished desc limit 1`).all()
    return pugs
}

export const getRunningPugsForUser = async (playerDiscordID: string): Promise<LivePugFinishedDto[]> => {
    const pugs: LivePugFinishedDto[] = await db.prepare(`SELECT id, players, team_red, team_blue,
    datetime(created, 'localtime') created,
    datetime(started, 'localtime') started,
    datetime(finished, 'localtime') finished,
    str_red, str_blue, rating_change_red, rating_change_blue,
    mode_id, map_id, score_id, server_id from pugs where players like '%${playerDiscordID}%' order by finished desc`).all()
    return pugs
}

export const getAllFinishedPugsForUser = async (discord_id: string, limit?: number): Promise<LivePugFinishedDto[]> => {
    return db.prepare(`select id, players, team_red, team_blue,
    datetime(created, 'localtime') created,
    datetime(started, 'localtime') started,
    datetime(finished, 'localtime') finished,
    str_red, str_blue, rating_change_red, rating_change_blue,
    mode_id, map_id, score_id, server_id from pugs where players LIKE '%${discord_id}%' and rating_change_red is not null order by finished desc ${limit ? 'LIMIT ' + limit : ''}`).all()
}

export const getSetOfFinishedPugsForUser = async (discord_id: string): Promise<LivePugFinishedDto[]> => {
    const pugs: LivePugFinishedDto[] = await db.prepare(`SELECT id, players, team_red, team_blue,
    datetime(created, 'localtime') created,
    datetime(started, 'localtime') started,
    datetime(finished, 'localtime') finished,
    str_red, str_blue, rating_change_red, rating_change_blue,
    mode_id, map_id, score_id, server_id from pugs WHERE players LIKE '%${discord_id}%' AND rating_change_red is not null ORDER BY finished DESC LIMIT 11`).all()
    return pugs
}

export const getMapLastPlayed = async (map_name: string): Promise<MapLastPlayed> => {
    return db.prepare(`SELECT datetime(finished, 'localtime') finished FROM pugs a JOIN maps b on b.id = a.map_id WHERE b.name = ? ORDER BY a.finished DESC LIMIT 1`).get(map_name)
}

export const getAllStrongestPugs = async (): Promise<LivePugFinished[]> => {
    return db.prepare(`select (str_red + str_blue) strength, p.id, p.players, p.team_red, p.team_blue, p.str_red, p.str_blue, datetime(p.finished, 'localtime') finished, s.score_red, s.score_blue from all_pugs p join all_scores s on p.id = s.pug_id where p.finished NOT NULL AND s.status != '${ScoreStatus.Rejected}' order by strength desc limit 3`).all()
}

export const getAllWeakestPugs = async (): Promise<LivePugFinished[]> => {
    return db.prepare(`select (str_red + str_blue) strength, p.id, p.players, p.team_red, p.team_blue, p.str_red, p.str_blue, datetime(p.finished, 'localtime') finished, s.score_red, s.score_blue from all_pugs p join all_scores s on p.id = s.pug_id where p.finished NOT NULL AND s.status != '${ScoreStatus.Rejected}' order by strength limit 3`).all()
}

export const getSeasonStrongestPugs = async (): Promise<LivePugFinished[]> => {
    return db.prepare(`select (str_red + str_blue) strength, p.id, p.players, p.team_red, p.team_blue, p.str_red, p.str_blue, datetime(p.finished, 'localtime') finished, s.score_red, s.score_blue from pugs p join scores s on p.id = s.pug_id where p.finished NOT NULL AND s.status != '${ScoreStatus.Rejected}' order by strength desc limit 3`).all()
}

export const getSeasonWeakestPugs = async (): Promise<LivePugFinished[]> => {
    return db.prepare(`select (str_red + str_blue) strength, p.id, p.players, p.team_red, p.team_blue, p.str_red, p.str_blue, datetime(p.finished, 'localtime') finished, s.score_red, s.score_blue from pugs p join scores s on p.id = s.pug_id where p.finished NOT NULL AND s.status != '${ScoreStatus.Rejected}' order by strength limit 3`).all()
}

export const getLivePugForUser = async (player_discord_id: string): Promise<LivePugFinished | null> => {
    return await db.prepare(`select p.id, p.players, p.team_red, p.team_blue,
    datetime(p.created, 'localtime') created,
    datetime(p.started, 'localtime') started,
    datetime(p.finished, 'localtime') finished,
    p.str_red, p.str_blue, p.rating_change_red, p.rating_change_blue,
    p.mode_id, p.map_id, p.score_id, p.server_id from pugs p join scores s on p.id = s.pug_id
    where (s.status = '${ScoreStatus.Pending}' or s.status = '${ScoreStatus.Live}') and p.players LIKE '%${player_discord_id}%'`).get()
}

export const getLiveServers = async (): Promise<Server[]> => {
    return await db.prepare(`select serv.*, p.id pug_id from pugs p
        join scores s on p.id = s.pug_id
        join servers serv on serv.id = p.server_id
        where s.status = '${ScoreStatus.Live}' `).all()
}

export const getAllPugStats = async (mode_id: number): Promise<PugStats> => {
    return db.prepare(`SELECT count(s.pug_id) count, round(avg(abs(s.score_blue - s.score_red)), 2) as diff, round(avg(abs(s.score_blue + s.score_red)), 2) caps
        FROM all_pugs p
        JOIN all_scores s ON s.pug_id = p.id
        JOIN maps m ON m.id = p.map_id
        WHERE NOT s.status = 'REJECTED' AND s.score_blue NOT NULL AND p.mode_id = ?
        ORDER BY diff DESC`).get(mode_id)
}

export const getSeasonPugStats = async (mode_id: number): Promise<PugStats> => {
    return db.prepare(`SELECT count(s.pug_id) count, round(avg(abs(s.score_blue - s.score_red)), 2) as diff, round(avg(abs(s.score_blue + s.score_red)), 2) caps
        FROM pugs p
        JOIN scores s ON s.pug_id = p.id
        JOIN maps m ON m.id = p.map_id
        WHERE NOT s.status = 'REJECTED' AND s.score_blue NOT NULL AND p.mode_id = ?
        ORDER BY diff DESC`).get(mode_id)
}

export const getTodaysPugs = async (mode_id: number): Promise<TodaysPugs[]> => {
    return db.prepare(`SELECT pug.id, datetime(pug.finished, 'localtime') finished , pug.str_red, pug.str_blue, pug.mode_id, map.name map_name, score.score_red, score.score_blue
        FROM pugs pug
        JOIN maps map ON map.id = pug.map_id
        JOIN scores score on score.pug_id = pug.id
        WHERE pug.mode_id = ? AND pug.started is not null and score.status is not '${ScoreStatus.Rejected}' AND pug.finished >= CURRENT_DATE
        ORDER BY pug.created`).all(mode_id)
}

export const getLastPugId = async (): Promise<HasId> => {
    const lastPug = await db.prepare(`
            SELECT id
            FROM pugs
            WHERE started is not null
            ORDER BY started DESC
            LIMIT 1`).get()

    return lastPug
}

export const getPugsByDay = async (): Promise<PugsCount[]> => {
    return db.prepare(`select count(*) cnt, strftime('%d-%m-%Y', started) day from all_pugs where rating_change_blue is not null group by day order by started desc`).all()
}

//inserts
export const insertNewEmptyPug = async (mode_id: number): Promise<LivePugDto> => {
    return <LivePugDto><unknown>db.prepare(`INSERT INTO pugs (mode_id) VALUES (?) RETURNING *`).get(mode_id)
}

//updates
export const updatePugPlayers = async (players: string, pug_id: string) => {
    await db.prepare(`UPDATE pugs set players = ? where id = ? `).run(players, pug_id)
}

export const updatePugToStart = async (team_red: string, team_blue: string, str_red: number, str_blue: number, server_id: string, pug_id: string, map_id: string, score_id: string) => {
    await db.prepare(`UPDATE pugs set team_red = ?, team_blue = ?, str_red = ?, str_blue = ?, started = CURRENT_TIMESTAMP, server_id = ?, map_id = ?, score_id = ? where id = ? `)
        .run(team_red, team_blue, str_red, str_blue, server_id, map_id, score_id, pug_id)
}

export const updateRatings = async (pug: LivePugScoredDto, rating_red: number, rating_blue: number): Promise<void> => {
    await db.exec('BEGIN')

    await db.prepare(`update players set rating = rating + ?, rating_change = ?, pugs_played = pugs_played + 1 where discord_id in (${pug.team_red})`).run(rating_red, rating_red)
    await db.prepare(`update players set rating = rating + ?, rating_change = ?, pugs_played = pugs_played + 1 where discord_id in (${pug.team_blue})`).run(rating_blue, rating_blue)

    await db.prepare('update pugs set rating_change_red = ?, rating_change_blue = ? where id = ?').run(rating_red, rating_blue, pug.id)

    await db.exec('END')
}

export const finishPug = async (score_id: string, pug_id: string) => {
    await db.prepare(`UPDATE pugs set finished = CURRENT_TIMESTAMP, score_id = ? where id = ?  returning *`).get(score_id, pug_id)
}


////////////// STATS STUFF
export interface WonPugsTeammates {
    blue: PugScoredWithMap[],
    red: PugScoredWithMap[]
}

interface TeamStrengthCapDiff {
    elo6k7: number,
    elo7k: number,
    elo7k3: number,
    elo7k6: number,
    elo7k9: number,
    elo8k2: number,
}

export const getAllPugsWithTwoTeammates = async (id1: string, id2: string): Promise<WonPugsTeammates> => {
    const blue = await db.prepare(`select p.*, s.score_red, s.score_blue from all_pugs p join all_scores s on s.pug_id = p.id
        where p.team_blue LIKE ('%${id1}%${id2}%') OR p.team_blue LIKE ('%${id2}%${id1}%') AND p.rating_change_blue is not null and s.status in ('${ScoreStatus.Accepted}', '${ScoreStatus.Manual}')`).all()
    const red = await db.prepare(`select p.*, s.score_red, s.score_blue from all_pugs p join all_scores s on s.pug_id = p.id
        where p.team_red LIKE ('%${id1}%${id2}%') OR p.team_red LIKE ('%${id2}%${id1}%') AND p.rating_change_red is not null and s.status in ('${ScoreStatus.Accepted}', '${ScoreStatus.Manual}')`).all()

    return {
        blue,
        red
    }
}

export const getSeasonPugsWithTwoTeammates = async (id1: string, id2: string): Promise<WonPugsTeammates> => {
    const blue = await db.prepare(`select p.*, s.score_red, s.score_blue from pugs p join scores s on s.pug_id = p.id
        where p.team_blue LIKE ('%${id1}%${id2}%') OR p.team_blue LIKE ('%${id2}%${id1}%') AND p.rating_change_blue is not null and s.status in ('${ScoreStatus.Accepted}', '${ScoreStatus.Manual}')`).all()
    const red = await db.prepare(`select p.*, s.score_red, s.score_blue from pugs p join scores s on s.pug_id = p.id
        where p.team_red LIKE ('%${id1}%${id2}%') OR p.team_red LIKE ('%${id2}%${id1}%') AND p.rating_change_red is not null and s.status in ('${ScoreStatus.Accepted}', '${ScoreStatus.Manual}')`).all()

    return {
        blue,
        red
    }
}

export const getAllScoredPugsForPlayer = async (id1: string): Promise<WonPugsTeammates> => {
    const blue = await db.prepare(`select p.*, s.score_red, s.score_blue from all_pugs p join all_scores s on s.pug_id = p.id
        where (p.team_blue LIKE ('%${id1}%') OR p.team_blue LIKE ('%${id1}%')) AND p.rating_change_blue is not null and s.status in ('${ScoreStatus.Accepted}', '${ScoreStatus.Manual}')`).all()
    const red = await db.prepare(`select p.*, s.score_red, s.score_blue from all_pugs p join all_scores s on s.pug_id = p.id
        where (p.team_red LIKE ('%${id1}%') OR p.team_red LIKE ('${id1}%')) AND p.rating_change_red is not null and s.status in ('${ScoreStatus.Accepted}', '${ScoreStatus.Manual}')`).all()

    return {
        blue,
        red
    }
}

export const getSeasonScoredPugsForPlayer = async (id1: string): Promise<WonPugsTeammates> => {
    const blue = await db.prepare(`select p.*, s.score_red, s.score_blue from pugs p join scores s on s.pug_id = p.id
        where (p.team_blue LIKE ('%${id1}%') OR p.team_blue LIKE ('%${id1}%')) AND p.rating_change_blue is not null and s.status in ('${ScoreStatus.Accepted}', '${ScoreStatus.Manual}')`).all()
    const red = await db.prepare(`select p.*, s.score_red, s.score_blue from pugs p join scores s on s.pug_id = p.id
        where (p.team_red LIKE ('%${id1}%') OR p.team_red LIKE ('${id1}%')) AND p.rating_change_red is not null and s.status in ('${ScoreStatus.Accepted}', '${ScoreStatus.Manual}')`).all()

    return {
        blue,
        red
    }
}

export const getScoredPugsByTeamStrength = async (): Promise<TeamStrengthCapDiff> => {
    const elo6k7 = await db.prepare(`select avg(abs(s.score_blue - s.score_red)) as capdiff from pugs p join scores s on s.pug_id = p.id
        where (p.str_red + p.str_blue) < 6700 AND p.rating_change_blue is not null and s.status != '${ScoreStatus.Rejected}'`).get()['capdiff']
    const elo7k = await db.prepare(`select avg(abs(s.score_blue - s.score_red)) as capdiff from pugs p join scores s on s.pug_id = p.id
    where (p.str_red + p.str_blue) < 7000 and (p.str_red + p.str_blue) >= 6700 AND p.rating_change_blue is not null and s.status != '${ScoreStatus.Rejected}'`).get()['capdiff']
    const elo7k3 = await db.prepare(`select avg(abs(s.score_blue - s.score_red)) as capdiff from pugs p join scores s on s.pug_id = p.id
    where (p.str_red + p.str_blue) < 7300 and (p.str_red + p.str_blue) >= 7000 AND p.rating_change_blue is not null and s.status != '${ScoreStatus.Rejected}'`).get()['capdiff']
    const elo7k6 = await db.prepare(`select avg(abs(s.score_blue - s.score_red)) as capdiff from pugs p join scores s on s.pug_id = p.id
    where (p.str_red + p.str_blue) < 7600 and (p.str_red + p.str_blue) >= 7300 AND p.rating_change_blue is not null and s.status != '${ScoreStatus.Rejected}'`).get()['capdiff']
    const elo7k9 = await db.prepare(`select avg(abs(s.score_blue - s.score_red)) as capdiff from pugs p join scores s on s.pug_id = p.id
    where (p.str_red + p.str_blue) < 7900 and (p.str_red + p.str_blue) >= 7600 AND p.rating_change_blue is not null and s.status != '${ScoreStatus.Rejected}'`).get()['capdiff']
    const elo8k2 = await db.prepare(`select avg(abs(s.score_blue - s.score_red)) as capdiff from pugs p join scores s on s.pug_id = p.id
    where (p.str_red + p.str_blue) < 8200 and (p.str_red + p.str_blue) >= 7900 AND p.rating_change_blue is not null and s.status != '${ScoreStatus.Rejected}'`).get()['capdiff']

    return {
        elo6k7,
        elo7k,
        elo7k3,
        elo7k6,
        elo7k9,
        elo8k2,
    }
}

export const getScoredPugs = async (): Promise<LivePugScored[]> => {
    return db.prepare(`select p.*, s.*, m.name as map_name from pugs p join scores s on p.id = s.pug_id join maps m on p.map_id = m.id where rating_change_blue is not null and s.status != '${ScoreStatus.Rejected}'`).all()
}

export const getCapDiffs = async (): Promise<number[]> => {
    return db.prepare(`select abs(s.score_red - s.score_blue) as diff from pugs p join scores s on p.id = s.pug_id where rating_change_blue is not null and s.status == '${ScoreStatus.Accepted}'`).all().map(res => res.diff)
}

export const getEnemyWinrateAll = async (id1: string, id2: string): Promise<[number, number] | null> => {
    const blueResults = await db.prepare(`select sum(s.score_red < s.score_blue) wins, sum(s.score_red > s.score_blue) losses from all_pugs p join all_scores s on s.pug_id = p.id where team_blue like '%${id1}%' and team_red like '%${id2}%' and s.status != 'REJECTED'`).get()
    const redResults = await db.prepare(`select sum(s.score_red > s.score_blue) wins, sum(s.score_red < s.score_blue) losses from all_pugs p join all_scores s on s.pug_id = p.id where team_red like '%${id1}%' and team_blue like '%${id2}%' and s.status != 'REJECTED'`).get()

    if (blueResults['wins'] + blueResults['losses'] == 0) return null

    const totalPlayedAgainst = redResults['losses'] + blueResults['losses'] + redResults['wins'] + blueResults['wins']
    const player1Winrate = Math.round(100 * (redResults['wins'] + blueResults['wins']) / totalPlayedAgainst)

    return ([player1Winrate, totalPlayedAgainst])
}

export const getEnemyWinrateSeason = async (id1: string, id2: string): Promise<[number, number] | null> => {
    const blueResults = await db.prepare(`select sum(s.score_red < s.score_blue) wins, sum(s.score_red > s.score_blue) losses from pugs p join scores s on s.pug_id = p.id where team_blue like '%${id1}%' and team_red like '%${id2}%' and s.status != 'REJECTED'`).get()
    const redResults = await db.prepare(`select sum(s.score_red > s.score_blue) wins, sum(s.score_red < s.score_blue) losses from pugs p join scores s on s.pug_id = p.id where team_red like '%${id1}%' and team_blue like '%${id2}%' and s.status != 'REJECTED'`).get()

    if (blueResults['wins'] + blueResults['losses'] == 0) return null

    const totalPlayedAgainst = redResults['losses'] + blueResults['losses'] + redResults['wins'] + blueResults['wins']
    const player1Winrate = Math.round(100 * (redResults['wins'] + blueResults['wins']) / totalPlayedAgainst)

    return ([player1Winrate, totalPlayedAgainst])
}
