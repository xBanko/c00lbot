import sqlite3 from 'better-sqlite3'
import config from 'config'

const db = sqlite3(config.get('dbConfig.dbName'))

export interface Player {
    id?: string
    discord_id: string
    nickname: string
    rating: number
    rating_change?: number
    created?: Date
    last_played?: Date
    pugs_played?: number
    starting_rating?: number
}

interface LiveTeam {
    players: Player[]
    rating: number
}

export interface SuggestedTeams {
    red: LiveTeam
    blue: LiveTeam
}

interface RatingAndName {
    rating: number
    nickname: string
}

interface Rank {
    rank: number
}

interface PlayerName {
    nickname: string
}

interface RatingChange {
    nickname: string
    rating: number
    starting_rating: number
    diff: number
}

interface Countable {
    count: number
}

interface PlayerFound {
    discord_id?: string
    nickname?: string
    found: boolean
}

//gets
export const getPlayerById = async (id: string): Promise<Player> => {
    const Player = await db.prepare(`SELECT * from players where id = ?`).get(id)
    return Player
}

export const getPlayerByDiscordId = async (id: string): Promise<Player> => {
    const Player = await db.prepare(`SELECT * from players where discord_id = ?`).get(id)
    return Player
}

export const getPlayersByDiscordId = async (id: string[] | string): Promise<Player[]> => {
    const players = await db.prepare(`SELECT * from players where discord_id IN (${id})`).all()
    return players
}

export const getPlayersStr = async (id: string[]): Promise<number> => {
    const preparation = (id.join(','))
    const str = (await db.prepare(`SELECT SUM (rating) as rank from players where discord_id IN (${preparation})`).get()).rank
    return str
}

export const getPlayerRating = async (discord_id: string): Promise<RatingAndName> => {
    const str = (await db.prepare(`SELECT rating, nickname from players where discord_id = ?`).get(discord_id))
    return str
}

export const checkIfPlayerRegistered = async (discord_id: string): Promise<boolean> => {
    const player = await db.prepare(`SELECT * from players where discord_id = ? and rating is not null`).get(discord_id)
    return player !== undefined
}

export const getPlayerRank = async (discord_id: string): Promise<Rank> => {
    const rank = (await db.prepare(`SELECT count(*)+1 as rank from players where rating_change NOT NULL AND rating > (select rating from players where discord_id = ?)`).get(discord_id))
    return rank
}

export const getTopPlayers = async (limit: number): Promise<Player[]> => {
    return await db.prepare(`SELECT * from players where rating_change NOT NULL order by rating desc limit ${limit},10`).all()
}

export const getAllTopPlayers = async (): Promise<Player[]> => {
    return await db.prepare(`SELECT * from players where rating_change NOT NULL order by rating desc`).all()
}

export const getPlayerName = async (discord_id: string): Promise<PlayerName> => {
    const name = (await db.prepare(`SELECT nickname from players where discord_id = ?`).get(discord_id))
    return name
}

export const getAllActivePlayers = async (): Promise<Player[]> => {
    return await db.prepare(`SELECT * from players where rating_change NOT NULL`).all()
}

export const getCountAllActivePlayers = async (): Promise<Countable> => {
    return await db.prepare(`SELECT count(*) count FROM players WHERE pugs_played > 0`).get()
}

export const getCountSeasonActivePlayers = async (): Promise<Countable> => {
    return await db.prepare(`SELECT count(*) count FROM players WHERE rating_change IS NOT NULL`).get()
}

export const getPlayerRatingChange = async (discord_id: string): Promise<RatingChange> => {
    const ratingChange = (await db.prepare(`SELECT nickname, rating, starting_rating, (rating - starting_rating) as diff FROM players WHERE rating_change NOT NULL AND discord_id = ?`).get(discord_id))
    return ratingChange
}

export const getTopPlayerRatingChange = async (): Promise<RatingChange[]> => {
    return await db.prepare(`SELECT nickname, rating, starting_rating, (rating - starting_rating) as diff FROM players WHERE rating_change NOT NULL ORDER BY diff DESC LIMIT 3`).all()
}

export const getWorstPlayerRatingChange = async (): Promise<RatingChange[]> => {
    return await db.prepare(`SELECT nickname, rating, starting_rating, (rating - starting_rating) as diff FROM players WHERE rating_change NOT NULL ORDER BY diff LIMIT 3`).all()
}

export const getStablePlayerRatingChange = async (): Promise<RatingChange[]> => {
    return await db.prepare(`SELECT nickname, rating, starting_rating, (rating - starting_rating) as diff FROM players WHERE rating_change NOT NULL AND diff = 0 ORDER BY diff LIMIT 3`).all()
}

//inserts
export const insertPlayer = async (player: Player): Promise<void> => {
    await db.prepare(`INSERT INTO players (discord_id, nickname, rating, starting_rating, coins) VALUES (?, ?, ?, ?, 1000)`)
        .run(player.discord_id, player.nickname, player.rating, player.starting_rating)
}

//updates
export const setPlayer = async (discord_id: string, rating: number, nickname: string): Promise<PlayerFound> => {
    const playerName = await db.prepare(`SELECT nickname FROM players WHERE discord_id = ?`).get(discord_id)
    if (playerName === undefined) return { nickname, found: false }
    await db.prepare(`UPDATE players SET nickname = ?, rating = ?, starting_rating = ? WHERE discord_id = ?`).run(nickname, rating, rating, discord_id)
    return { nickname, found: true }
}

export const addCoins = async (discord_id: string, coins: number): Promise<PlayerFound> => {
    const playerName = await db.prepare(`SELECT nickname FROM players WHERE discord_id = ?`).get(discord_id)
    if (playerName === undefined) return { found: false }
    await db.prepare(`UPDATE players SET coins = coins + ${coins} WHERE discord_id = ?`).run(discord_id)
    return { found: true }
}
