import sqlite3 from 'better-sqlite3'
import config from 'config'

import { ScoreStatus } from '../../bot/embeds/utils/constants'

const db = sqlite3(config.get('dbConfig.dbName'))

export interface Score {
    id: string
    pug_id: number
    score_blue: number
    score_red: number
    created: Date
    created_by: string
    status: ScoreStatus
    reactions_yes: string
    reactions_no: string
    reactions_dead: string
    score_msg_id: string
}

export interface ScoreWithServer extends Score {
    shortname: string
}

interface TotalScore {
    total: number
}

//gets
export const getScoresThatNeedResults = async (): Promise<Score[]> => {
    const scores: Score[] = await db.prepare(
        `SELECT * from scores where status = ?`).all(ScoreStatus.Pending)
    return scores
}

export const getScoresToFinalize = async (): Promise<Score[]> => {
    const scores: Score[] = await db.prepare(
        `SELECT * from scores where status = ? and CURRENT_TIMESTAMP > DATETIME(created, '+${config.get('scoring.timeoutMinutes')} minutes') `).all(ScoreStatus.Concluded)
    return scores
}

export const getUnconcludedPugs = async (): Promise<Score[]> => {
    const scores: Score[] = await db.prepare(
        `SELECT * FROM scores WHERE status = '${ScoreStatus.Pending}' OR status = '${ScoreStatus.Live}'  OR (status = '${ScoreStatus.Manual}' AND created_by = 'bot')`).all()
    return scores
}

export const getAllTotalCaps = async (): Promise<TotalScore> => {
    return db.prepare(`SELECT sum(score_red + score_blue) as total FROM all_scores`).get()
}

export const getSeasonTotalCaps = async (): Promise<TotalScore> => {
    return db.prepare(`SELECT sum(score_red + score_blue) as total FROM scores`).get()
}

export const getLiveScoreForIp = async (ip: string): Promise<ScoreWithServer> => {
    return await db.prepare(`SELECT sco.*, serv.shortname from scores sco
        join pugs p on sco.pug_id = p.id
        join servers serv on p.server_id = serv.id
        where sco.status = ? and serv.ip = ?`).get(ScoreStatus.Live, ip)
}

//updates
export const updatePugScoreMsgId = async (updatePugScoreMsgId: string, scoreId: number) => {
    await db.prepare(`UPDATE scores set score_msg_id = ? where id = ? `).run(updatePugScoreMsgId, scoreId)
}

export const updateScoreStatus = async (scoreId: string, status: ScoreStatus) => {
    await db.prepare(`UPDATE scores set status = ? where id = ? `).run(status, scoreId)
}

export const updateScore = async (scoreRed: number, scoreBlue: number, scoreId: string, status: ScoreStatus) => {
    await db.prepare(`UPDATE scores set score_red = ?, score_blue = ?, status = ? where id = ? `).run(scoreRed, scoreBlue, status, scoreId)
}

//inserts
export const insertScore = async (score_red: number, score_blue: number, pug_id: number, created_by: string) => {
    const score = <number><unknown>await db.prepare(`insert into scores (pug_id, score_red, score_blue, created_by, score_msg_id ) values (?, ?, ?, ?, ?)
     returning *`).run(pug_id, score_red, score_blue, created_by, '1234').lastInsertRowid
    await db.prepare(`UPDATE pugs set score_id = ?, finished = CURRENT_TIMESTAMP where id = ?  returning *`).run(score, pug_id)
    return score
}

export const insertEmptyScore = async (pug_id: string): Promise<string> => {
    return await db.prepare(`insert into scores (pug_id, created_by, status) values (?,?,?) returning *`).run(pug_id, 'bot', ScoreStatus.Pending).lastInsertRowid.toString()
}
