import sqlite3 from 'better-sqlite3'
import config from 'config'
import { pugMode } from '../../bot/embeds/utils/constants'
import { getCurrentPug } from './pug'

const db = sqlite3(config.get('dbConfig.dbName'))

enum BoostType {
    ELO = 'elo'
}

export interface PugBoostFinished extends PugBoost {
    pug_id: number
}

export interface PugBoost {
    id: number
    boost_type: BoostType
    value: number
    pug_id?: number
}

//gets
export const getActiveEloBoost = async (): Promise<PugBoost> => {
    let boost = await db.prepare(`SELECT * from pug_boosts where pug_id is null and boost_type = 'elo'`).get()
    if (!boost || !boost.id) {
        await insertEmptEloBoost()
        boost = await db.prepare(`SELECT * from pug_boosts where pug_id is null and boost_type = 'elo'`).get()
    }

    return boost
}

export const isPugDoubleElo = async (pugId: number): Promise<boolean> => {
    const boost = await db.prepare(`SELECT * from pug_boosts where pug_id = ? and boost_type = 'elo'`).get(pugId)

    return boost && boost.id ? true : false
}

//inserts
const insertEmptEloBoost = async (): Promise<void> => {
    await db.prepare(`INSERT INTO pug_boosts (boost_type) VALUES (?)`).run('elo')
}

//updates
export const updateValueForBoost = async (value: number, id: number): Promise<void> => {
    await db.prepare(`update pug_boosts set value = value + ? where id = ?`).run(value, id)
}

export const updateValueForBoostFinish = async (id: number): Promise<void> => {
    await db.prepare(`update pug_boosts set value = 10000 where id = ?`).run(id)
}

export const updatePugForBoost = async (pugId: number, id: number): Promise<void> => {
    await db.prepare(`update pug_boosts set pug_id = ? where id = ?`).run(pugId, id)
}

export const resolveBoostedDeadPug = async (pugId: number): Promise<void> => {
    const existingBoost = await db.prepare(`SELECT * from pug_boosts where pug_id = ?`).get(pugId)
    if (existingBoost && existingBoost.id) {
        const newPug = await getCurrentPug(pugMode)
        await updatePugForBoost(parseInt(newPug.id!), existingBoost.id)
    }
}