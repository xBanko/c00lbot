import sqlite3 from 'better-sqlite3'
import config from 'config'

const db = sqlite3(config.get('dbConfig.dbName'))

interface ServerName {
    shortname: string
}

interface ServerFound extends ServerName {
    found: boolean
}

interface ServerDeleted extends ServerFound {
    deleted: boolean
}

export interface Server extends ServerName {
    id: string
    ip: string
    port: string
    modes: string
    busy: boolean
    password: string
    country: string
    pug_id?: string
    enabled?: number
}

//gets
export const getNonBusyServers = async (): Promise<Server[]> => {
    return await db.prepare(`SELECT * FROM servers WHERE busy is not 1 AND enabled is 1`).all()
}

export const getAllServers = async (): Promise<Server[]> => {
    return await db.prepare(`select * from servers`).all()
}

export const getServerOfPug = async (pug_id: number | string): Promise<Server> => {
    return await db.prepare(`select * from servers s join all_pugs p on p.server_id = s.id where p.id = ?`).get(pug_id)
}

export const getServerByShortname = async (shortname: string): Promise<Server> => {
    return db.prepare(`select * from servers where shortname = ?`).get(shortname)
}

export const getServerByIp = async (ip: string): Promise<Server> => {
    return db.prepare(`select * from servers where ip = ?`).get(ip)
}

//updates
export const setFavouriteServer = async (server_shortname: string, player_discord_id: string) => {
    await db.prepare(`UPDATE players set fav_server = ? where discord_id = ?`).run(server_shortname, player_discord_id)
}
export const setFavouriteServerCountry = async (country: string, player_discord_id: string) => {
    await db.prepare(`UPDATE players set fav_server_country = ? where discord_id = ?`).run(country, player_discord_id)
}

export const setServerFreeId = async (id: string): Promise<void> => {
    await db.prepare(`update servers set busy = 0 where id = ?`).run(parseInt(id))
}

export const setServerFree = async (shortname: string): Promise<void> => {
    console.log(`[${new Date().getTime()}] Freeing server ${shortname}`)
    await db.prepare(`update servers set busy = 0 where shortname = ?`).run(shortname)
}

export const setServerBusy = async (shortname: string, pug_id: string, map_id: string, password: string): Promise<void> => {
    await db.prepare(`update servers set busy = 1, password = ? where shortname = ?`).run(password, shortname)
    await db.prepare(`update pugs set map_id = ? where id = ?`).run(map_id, pug_id)
}

export const setServerStatus = async (shortname: string, status: number): Promise<ServerFound> => {
    const serverName = await db.prepare(`SELECT shortname FROM servers WHERE shortname = ?`).get(shortname)
    if (serverName === undefined) return { shortname, found: false }
    await db.prepare(`UPDATE servers SET enabled = ? WHERE shortname = ?`).run(status, shortname)
    return { shortname, found: true }
}

export const setServer = async (shortname: string, ip: string, port: string, modes: string, status: number, country: string): Promise<ServerFound> => {
    const serverName = await db.prepare(`SELECT shortname FROM servers WHERE shortname = ?`).get(shortname)
    if (serverName === undefined) return { shortname, found: false }
    await db.prepare(`UPDATE servers SET ip = ?, port = ?, modes = ?, enabled = ?, country = ? WHERE shortname = ?`).run(ip, port, modes, status, country, shortname)
    return { shortname, found: true }
}

//inserts
export const addServer = async (shortname: string, ip: string, port: string, modes: string, status: number, country: string): Promise<void> => {
    await db.prepare(`insert into servers (shortname, ip, port, modes, enabled, country) values (?,?,?,?,?,?)`).run(shortname, ip, port, modes, status, country)
}

//deletes
export const removeServer = async (shortname: string): Promise<ServerDeleted> => {
    try {
        const serverName = await db.prepare(`SELECT shortname FROM servers WHERE shortname = ?`).get(shortname)
        if (serverName === undefined) return { shortname, found: false, deleted: false }
        await db.prepare(`DELETE FROM servers WHERE shortname = ?`).run(shortname)
        return { shortname, found: true, deleted: true }
    } catch (error) {
        console.log(`Deletion of server ${shortname} failed`)
        return { shortname, found: true, deleted: false }
    }
}