import sqlite3 from 'better-sqlite3'
import config from 'config'

const db = sqlite3(config.get('dbConfig.dbName'))

export interface Season {
    id: string
    name: string
    start_date: Date
    end_date: Date
    archived: boolean
    stats_msg_id?: string
    leaderboard_msg_id?: string
}

export const getCurrentSeason = async (): Promise<Season> => {
    return await db.prepare(`SELECT * from seasons where archived = 0 and start_date <= CURRENT_DATE and end_date >= CURRENT_DATE`).get()
}

export const setCurrentSeasonStatsMsgId = async (statsMsgId: string, id: string): Promise<void> => {
    await db.prepare(`update seasons set stats_msg_id = ? where id = ?`).run(statsMsgId, id)
}

export const setCurrentLeaderboardMsgId = async (statsMsgId: string, id: string): Promise<void> => {
    await db.prepare(`update seasons set leaderboard_msg_id = ? where id = ?`).run(statsMsgId, id)
}
