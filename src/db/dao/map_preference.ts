import sqlite3 from 'better-sqlite3'
import config from 'config'

const db = sqlite3(config.get('dbConfig.dbName'))

export enum MapPreferenceScore {
    Constantly = 10,
    Often = 5,
    Sometimes = 0,
    AlmostNever = -10
}
export interface MapPreference {
    map_name: string
    map_id: string
    player_id: string
    score: MapPreferenceScore
}

export const getAllMapPreferencesForPlayers = async (playerIds: string): Promise<MapPreference[]> => {
    return await db.prepare(`SELECT * from map_preferences where player_id in (${playerIds}) order by map_name asc`).all()
}

export const getMapPreferencesForPlayers = async (playerIds: string, mapId: string): Promise<MapPreference[]> => {
    return await db.prepare(`SELECT * from map_preferences where player_id in (${playerIds}) and map_id like '${mapId + '%'}' order by map_name asc`).all()
}

export const insertMapPreference = async (playerId: string, mapName: string, mapId: string, score: number): Promise<void> => {
    await db.prepare(`INSERT into map_preferences (player_id, map_name, map_id, score) values (?, ?, ?, ?)`).run(playerId, mapName, mapId, score)
}
