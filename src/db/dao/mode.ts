import sqlite3 from 'better-sqlite3'
import config from 'config'

const db = sqlite3(config.get('dbConfig.dbName'))

export interface Mode {
    name: string
    team_size: number
}

//gets
export const getAllModes = async (): Promise<Mode[]> => {
    const modes = await db.prepare(`SELECT * from modes`).all()
    return modes
}

export const getModeByName = async (name: string): Promise<Mode> => {
    const mode = await db.prepare(`SELECT * from modes where name = ?`).get(name)
    return mode
}

export const getModeById = async (id: number): Promise<Mode> => {
    const mode = await db.prepare(`SELECT * from modes where id = ?`).get(id)
    return mode
}

//inserts
export const insertMode = async (mode: Mode): Promise<void> => {
    await db.prepare(`INSERT INTO modes (name, team_size) VALUES (?, ?)`).run(mode.name, mode.team_size)
}
