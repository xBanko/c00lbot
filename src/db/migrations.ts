export const migrationsToRun = `
BEGIN;

CREATE TABLE IF NOT EXISTS modes (
    id integer primary key autoincrement,
    name TEXT UNIQUE,
    team_size integer,
    created DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS pugs (
    id integer primary key autoincrement,
    players TEXT,
    team_red TEXT,
    team_blue TEXT,
    created DEFAULT CURRENT_TIMESTAMP,
    started TEXT,
    finished TEXT,
    str_red integer,
    str_blue integer,
    rating_change_red integer,
    rating_change_blue integer,
    mode_id integer REFERENCES modes(id),
    map_id integer REFERENCES maps(id),
    score_id integer,
    server_id integer
);

CREATE TABLE IF NOT EXISTS players (
    id integer primary key autoincrement,
    discord_id TEXT UNIQUE,
    nickname TEXT,
    created DEFAULT CURRENT_TIMESTAMP,
    rating integer,
    rating_change integer,
    pugs_played integer DEFAULT 0,
    fav_server text REFERENCES servers(shortname),
    fav_server_country text,
    starting_rating integer,
    coins integer
);

CREATE TABLE IF NOT EXISTS maps (
    id integer primary key autoincrement,
    name TEXT UNIQUE,
    default_weight integer,
    current_weight integer,
    status text
);

CREATE TABLE IF NOT EXISTS scores (
    id integer primary key autoincrement,
    pug_id integer REFERENCES pugs(id),
    score_red integer,
    score_blue integer,
    created DEFAULT CURRENT_TIMESTAMP,
    created_by text,
    status text default 'PENDING',
    score_msg_id text
);

CREATE TABLE IF NOT EXISTS servers (
    id integer primary key autoincrement,
    shortname text UNIQUE,
    ip TEXT,
    port TEXT,
    modes TEXT,
    busy integer,
    password TEXT,
    country TEXT,
    enabled INTEGER
);

CREATE TABLE IF NOT EXISTS bans (
    id integer primary key autoincrement,
    discord_id TEXT NOT NULL,
    reason text,
    duration integer,
    end_time text,
    admin_id text,
    admin_name text,
    status text
);

CREATE TABLE IF NOT EXISTS seasons (
    id integer primary key autoincrement,
    name text,
    start_date text,
    end_date text,
    archived integer,
    stats_msg_id text,
    leaderboard_msg_id text
);

CREATE TABLE IF NOT EXISTS bets (
  id integer primary key,
  pug_id integer,
  player_id text,
  amount integer,
  bet_on text,
  net integer,
  odds integer,
  created DEFAULT CURRENT_TIMESTAMP,
  realised text
);

CREATE TABLE IF NOT EXISTS passwords (
  id integer primary key,
  pug_id text,
  player_id text,
  team text,
  password text,
  active integer DEFAULT 1,
  late integer DEFAULT 0,
  noshow integer DEFAULT 0
);

CREATE TABLE IF NOT EXISTS all_pugs (
    id integer primary key,
    players TEXT,
    team_red TEXT,
    team_blue TEXT,
    created DEFAULT CURRENT_TIMESTAMP,
    started TEXT,
    finished TEXT,
    str_red integer,
    str_blue integer,
    rating_change_red integer,
    rating_change_blue integer,
    mode_id integer,
    map_id integer,
    score_id integer,
    server_id integer
);

CREATE TABLE IF NOT EXISTS all_scores (
    id integer primary key,
    pug_id integer,
    score_red integer,
    score_blue integer,
    created DEFAULT CURRENT_TIMESTAMP,
    created_by text,
    status text default 'PENDING',
    score_msg_id text
);

CREATE TRIGGER IF NOT EXISTS insert_all_pugs
AFTER INSERT ON pugs
BEGIN
INSERT INTO all_pugs (
    id, players, team_red, team_blue, created,
    started, finished, str_red, str_blue,
    rating_change_red, rating_change_blue,
    mode_id, map_id, score_id, server_id
  )
VALUES
  (
    NEW.id, NEW.players, NEW.team_red, NEW.team_blue,
    NEW.created, NEW.started, NEW.finished,
    NEW.str_red, NEW.str_blue, NEW.rating_change_red,
    NEW.rating_change_blue, NEW.mode_id,
    NEW.map_id, NEW.score_id, NEW.server_id
  );
END;

CREATE TRIGGER IF NOT EXISTS insert_all_scores
AFTER INSERT ON scores
BEGIN
INSERT INTO all_scores (
    id, pug_id, score_red, score_blue, created,
    created_by, status, score_msg_id
  )
VALUES
  (
    NEW.id, NEW.pug_id, NEW.score_red, NEW.score_blue,
    NEW.created, NEW.created_by, NEW.status,
    NEW.score_msg_id
  );
END;

CREATE TRIGGER IF NOT EXISTS update_all_pugs
AFTER UPDATE ON pugs
BEGIN
UPDATE all_pugs
SET
  players = NEW.players,
  team_red = NEW.team_red,
  team_blue = NEW.team_blue,
  created = NEW.created,
  started = NEW.started,
  finished = NEW.finished,
  str_red = NEW.str_red,
  str_blue = NEW.str_blue,
  rating_change_red = NEW.rating_change_red,
  rating_change_blue = NEW.rating_change_blue,
  mode_id = NEW.mode_id,
  map_id = NEW.map_id,
  score_id = NEW.score_id,
  server_id = NEW.server_id
WHERE id = new.id;
END;

CREATE TRIGGER IF NOT EXISTS update_all_scores
AFTER UPDATE ON scores
BEGIN
UPDATE all_scores
SET
  pug_id = NEW.pug_id,
  score_red = NEW.score_red,
  score_blue = NEW.score_blue,
  created = NEW.created,
  created_by = NEW.created_by,
  status = NEW.status,
  score_msg_id = NEW.score_msg_id
WHERE id = new.id;
END;

CREATE TABLE if not exists pug_boosts (
	id	INTEGER PRIMARY KEY AUTOINCREMENT,
	boost_type TEXT DEFAULT 'elo',
	value	INTEGER DEFAULT 0,
	pug_id INTEGER
);

CREATE TABLE IF NOT EXISTS server_preferences (
  id integer primary key,
  player_id TEXT,
  server_shortname TEXT,
  score INTEGER,
  UNIQUE(player_id, server_shortname) ON CONFLICT REPLACE
);

CREATE TABLE IF NOT EXISTS map_preferences (
  id integer primary key,
  player_id TEXT,
  map_id TEXT,
  map_name TEXT,
  score INTEGER,
  UNIQUE(player_id, map_id) ON CONFLICT REPLACE
);

CREATE TABLE IF NOT EXISTS pug_odds (
  pug_id integer,
  blue_odds,
  red_odds,
  draw_odds
);

CREATE TABLE IF NOT EXISTS warnings (
  player_id,
  warning_type
);

END;
`
